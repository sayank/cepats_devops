<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<div id="mainContainer" class="col-xs-12">

  <hr />
  <div class="row">
    <dl id="enrolmentInfo" class="col-sm-6">
      <dt>CE Enrolment #</dt>
      <dd id="enrolmentNumber">${enrolmentNumber}</dd>
      <dt>Address</dt>
      <dd id="address">${user.enrolment.streetAddress}, ${user.enrolment.city}, ${user.enrolment.province}, ${user.enrolment.postalCode}</dd>
    </dl>
    <dl id="warrantyInfo" class="col-sm-6">
      <dt>Warranty Start Date</dt>
      <dd><fmt:formatDate value="${user.enrolment.warrantyStartDate}" pattern="M-d-Y"/></dd>
    </dl>
    <p id="enrolmentInfoError" class="col-sm-6 text-right hidden"></p>
  </div>
  <hr />
  <div class="row">
    <nav class="btn-group col-xs-12">
      <a href="/cepats/firstyear/${enrolmentNumber}/" class="btn btn-default btn-lg col-xs-6">First Year</a>
      <a href="/cepats/secondyear/${enrolmentNumber}/" class="btn btn-default btn-lg col-xs-6">Second Year</a>
    </nav>
  </div>
  <hr />
  <div class="row buttonToolbar">
    <div class="col-sm-2">
    	<sec:authorize access="hasAuthority('WRITE')">
	      <button id="addItem" type="button" class="btn btn-primary btn-lg createNew hidden" data-toggle="tooltip" data-placement="top" title="Add a new item to this PATS file">
	        <i class="icon icon-plus" aria-hidden="true"></i> Add Item
	      </button>
      </sec:authorize>
    </div>
    <div class="col-sm-10 btn-toolbar">
       <div class="form-group" id="from-date">
            <div class="input-group date">
              <input type="text" class="form-control" placeholder="FROM:YYYY-MM-DD"/>
              <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>
        <div class="form-group" id="to-date">
            <div class="input-group date">
              <input type="text" class="form-control" placeholder="TO:YYYY-MM-DD"/>
              <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>
      <button type="button" class="btn btn-default btn-lg left" id="download-history">Download History</button>
      
      <sec:authorize access="hasAuthority('WRITE')">
	      <button id="importPATSForm" type="button" class="btn btn-default btn-lg hidden" data-toggle="tooltip" data-placement="top" title="Import form from Excel file" onclick="cepats.openImportModal();">
	        <i class="icon icon-cloud-upload" aria-hidden="true"></i> Import
	      </button>
      </sec:authorize>
      
      <button id="exportPATSForm" type="button" class="btn btn-default btn-lg export hidden" data-toggle="tooltip" data-placement="top" title="Export form to Excel file">
        <i class="icon icon-cloud-download" aria-hidden="true"></i> Export
      </button>
      <a href="/cepats/${enrolmentNumber}/downloadTemplate" id="exportPATSTemplate" class="btn btn-default btn-lg hidden" title="Download a PATS template to get started" target="_blank">
        <i class="icon icon-doc" aria-hidden="true"></i> Download Template
      </a>
      <button type="button" id="deletePATSBtn" class="btn btn-default btn-lg hidden" title="Delete PATS file">
        <i class="icon icon-trash" aria-hidden="true"></i> Delete
      </button>
    </div>
    <div class="col-xs-12" id="deletePATSResult"></div>
  </div>
  <hr />
  <div class="row">
    <div class="col-sm-6">
      <div class="search">
        <input class="form-control search" type="text" placeholder="Search by text"/>
        <i id="searchclear" class="icon icon-close" aria-hidden="true"></i>
      </div>
    </div>
		<div class="col-sm-3">
			<div class="searchByItemId">
				<input class="form-control searchByItemId" type="text" placeholder="Search by Item #"/>
				<i id="searchByItemIdClear" class="icon icon-close" aria-hidden="true"></i>
			</div>
		</div>
    <div class="col-sm-3">
      <div class="searchByRef">
        <input class="form-control searchByRef" type="text" placeholder="Search by PA Ref. #"/>
        <i id="searchByRefClear" class="icon icon-close" aria-hidden="true"></i>
      </div>
    </div>
  </div>
  <hr />
  <div class="row">
    <div class="col-sm-8">
      <div class="pagination"></div>
    </div>
    <div class="col-sm-2 group-dropdown">
      <select id="itemsPerPageBtn" class="form-control">
        <option value="na">Items per Page</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="all">All Items</option>
      </select>
    </div>
    <div class="col-sm-2">
      <div class="searchByPageNum">
        <input class="form-control searchByPageNum" type="text" placeholder="Go to page #"/>
        <i id="searchByPageNumClear" class="icon icon-close" aria-hidden="true"></i>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="tableContainer table-responsive">
        <div class="loading">Loading PATS Form...</div>
      </div>
      <div class="pagination"></div>
      <div class="clear"></div>
    </div>
  </div>
  <script type="text/javascript">cepats.init('${formType}', '${enrolmentNumber}');</script>
</div>
