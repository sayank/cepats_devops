<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html lang="en">

<c:set var="basePath" value="/cepats/" scope="application"/>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
    <spring:url value="/favicon.ico" var="favicon_url" />
    <link rel="shortcut icon" type="image/png" href="${favicon_url}"/>

    <!-- css -->
    <spring:url value="/css/jquery.dataTables.min.css" var="dataTables_css_url" />
    <spring:url value="/css/bootstrap.css" var="bootstrap_css_url" />
    <spring:url value="/css/bootstrap-theme.css" var="bootstrap_theme_css_url" />
    <spring:url value="/css/bootstrap-validator.min.css" var="bootstrap_validator" />
    <spring:url value="/css/bootstrap-editable.css" var="bootstrap_editable_styles" />
    <spring:url value="/css/bootstrap-datepicker.standalone.css" var="bootstrap_date_css_url" />
    <spring:url value="/css/cepats.css" var="cepats_css_url" />


    <link rel="stylesheet" type="text/css" href="${dataTables_css_url}"></link><!--  -->
    <link rel="stylesheet" type="text/css" href="${bootstrap_css_url}"></link><!--  -->
    <link rel="stylesheet" type="text/css" href="${bootstrap_theme_css_url}"></link><!-- -->
    <link rel="stylesheet" type="text/css" href="${bootstrap_validator}"></link><!-- -->
    <link rel="stylesheet" type="text/css" href="${bootstrap_editable_styles}"></link><!-- -->
    <link rel="stylesheet" type="text/css" href="${bootstrap_date_css_url}"></link><!--  -->
    <link rel="stylesheet" type="text/css" href="${cepats_css_url}"></link><!-- -->


    <!-- javascript -->
    <!--<spring:url value="/js/jquery-1.11.1.min.js" var="jquery_url" />-->
    <!--<spring:url value="/js/jquery-2.1.1.min.js" var="jquery_url" />-->
    <spring:url value="/js/jquery-3.1.1.min.js" var="jquery_url" />
    <spring:url value="/js/jquery-ui.min.js" var="jquery_ui_url" />
    <spring:url value="/js/moment.js" var="moment_url" />
    <spring:url value="/js/transition.js" var="transition_url" />
    <spring:url value="/js/collapse.js" var="collapse_url" />
    <spring:url value="/js/bootstrap.min.js" var="bootstrap_url" />
    <spring:url value="/js/handlebars-v1.3.0.js" var="handlebars_url" />
    <spring:url value="/js/handlebars.helpers.js" var="handlebars_helpers_url" />
    <spring:url value="/js/jquery.dataTables.min.js" var="dataTables_url" />
    <spring:url value="/js/dataTables.bootstrap.min.js" var="bootstrap_dataTables_url" />
    <spring:url value="/js/full_numbers_no_ellipses.js" var="full_numbers_url" />
    <spring:url value="/js/jquery.tablesorter.min.js" var="tableSorter_url" />
    <spring:url value="/js/jquery.storageapi.min.js" var="jquery_storage"/>
    <spring:url value="/js/jquery.bootstrapvalidator.min.js" var="jquery_validate"/>
    <spring:url value="/js/bootstrap-editable.min.js" var="bootstrap_editable_script"/>
    <spring:url value="/js/cepats.js?ver=2" var="cepats"/>
    <spring:url value="/js/filters.js?ver=2" var="filters"/>
    <spring:url value="/js/jspdf.debug.js" var="jspdf_url"/>
    <spring:url value="/js/jspdf.plugin.autotable.min.js" var="jspdf_autotable_url"/>
    <spring:url value="/js/jquery.tabletojson.js" var="tabletojson_url"/>
    <spring:url value="/js/bootstrap-datepicker.min.js" var="bootstrap_date_url" />


    <script src="${jquery_url}" type="text/javascript"><!-- --></script>
    <script src="${moment_url}" type="text/javascript"><!-- --></script>
    <script src="${transition_url}" type="text/javascript"><!-- --></script>
    <script src="${collapse_url}" type="text/javascript"><!-- --></script>
    <script src="${bootstrap_url}" type="text/javascript"><!-- --></script>
    <script src="${jquery_ui_url}" type="text/javascript"><!-- --></script>
    <script src="${handlebars_url}" type="text/javascript"><!-- --></script>
    <script src="${handlebars_helpers_url}" type="text/javascript"><!-- --></script>
    <script src="${dataTables_url}" type="text/javascript"><!-- --></script>
    <script src="${bootstrap_dataTables_url}" type="text/javascript"><!-- --></script>
    <script src="${full_numbers_url}" type="text/javascript"><!-- --></script>
    <script src="${tableSorter_url}" type="text/javascript"><!-- --></script>
    <script src="${jquery_validate}" type="text/javascript"><!-- --></script>
    <script src="${jquery_storage}" type="text/javascript"><!-- --></script>
    <script src="${bootstrap_editable_script}" type="text/javascript"><!-- --></script>
    <script src="${cepats}" type="text/javascript"><!-- --></script>
    <script src="${filters}" type="text/javascript"><!-- --></script>
    <script src="${jspdf_url}" type="text/javascript"><!-- --></script>
    <script src="${jspdf_autotable_url}" type="text/javascript"><!-- --></script>
    <script src="${tabletojson_url}" type="text/javascript"><!-- --></script>
    <script src="${bootstrap_date_url}" type="text/javascript"><!-- --></script>

    <title>CE PATS</title>
</head>

<body>
    <div id="mainContainer">

        <header id="mainHeader" class="${headerCssClass}">
            <div id="mainHeader__logo">
                <h1>
                    <a href="/" title="CE PATS Home">Tarion</a>
                </h1>
            </div>
            <div id="mainHeader__title">
                <h2>CE PATS</h2>
            </div>
            <div id="mainHeader__logout">
                <!-- <c:if test="${not empty SPRING_SECURITY_CONTEXT}">
                  <a class="btn btn-primary" href="${basePath}resources/j_spring_security_logout">Sign Out</a>
                </c:if> -->
                <a class="btn btn-primary" href="${basePath}resources/j_spring_security_logout">Sign Out</a>
            </div>
        </header>

        <nav id="mainMenu">
            <ul>
                <li><a href="${videoTutorialLink}" target="_blank">Video Tutorial</a></li>
                <li><a href="${tutorialLink}" target="_blank">Tutorial</a></li>
                <li><a href="${basePath}faq/" target="_blank">FAQ</a></li>
                <li><a href="http://www.tarion.com/Pages/Contact-Us.aspx" target="_blank">Contact Us</a></li>
            </ul>
        </nav>
    </div>
    <div id="mainContent" class="container screen">
        <tiles:insertAttribute name="body"/>
    </div>

    <div id="mainContainer">
        <a href="#" class="scrollToTop"></a>
        <footer class="Site-footer">
            <div id="Footer">
                <div class="footer-top">
                    <div class="inner-footer">
                        <div class="row">

                            <div class="col col-connect">
                                <h4 class="footer-title">Connect</h4>
                                <span class="footer-line footer-article-title">
                        <a href="http://www.tarion.com/Pages/default.aspx" target="_blank">Tarion</a>
                      </span>
                                <span class="footer-line footer-article-title">
                        <a href="http://www.tarion.com/_layouts/15/buildersearch/builder_search.aspx"
                           target="_blank">Builder Directory</a>
                      </span>
                            </div>

                            <div class="col col-contact">
                                <h4 class="footer-title">Contact Tarion</h4>
                                <span class="footer-line">Call us: Mon-Fri 8 am-5 pm (EDT)</span>
                                <a href="tel:1-877-982-7466" class="link-decoration"
                                   target="blank">1-877-696-6497 Ext. 3812</a>
                                <br/>
                                <span class="footer-line space-up-write">Write us: <a href="mailto:builderlink@tarion.com"
                                                                                      class="link-decoration"
                                                                                      target="blank">builderlink@tarion.com</a></span>
                                <span class="footer-line space-up-address">Visit us: 5160 Yonge Street,<br/>12th Floor Toronto, ON M2N 6L9</span>
                            </div>

                            <div class="col col-newsletter">
                                <h4 class="footer-title">Newsletter</h4>
                                <span class="footer-line">Keep up with the latest from Tarion</span>
                                <a class="button btn btn-default"
                                   href="http://visitor.r20.constantcontact.com/d.jsp?llr=allmqleab&p=oi&m=1104192875872&sit=mmkluqvfb&f=e36fd679-f681-42ca-b8db-2aa5d3bb5111"
                                   target="_blank">Subscribe</a>
                                <br/>
                                <div class="footer-social social">
                                    <span class="footer-line">Follow Tarion</span>
                                    <div class="social-media">
                                        <a href="https://www.facebook.com/TarionWarrantyCorp" title="Facebook"
                                           class="imgBoxa" target="_blank"></a>
                                        <a class="imgBoxb" href="https://twitter.com/tarionwarranty" title="Twitter"
                                           target="_blank"></a>
                                        <a href="https://www.linkedin.com/company/tarion-warranty-corporation"
                                           class="imgBoxc" title="Linked In" target="_blank"></a>
                                        <a href="https://www.youtube.com/user/TarionWarranty" title="You Tube"
                                           class="imgBoxd" target="_blank"></a>
                                        <a href="https://www.instagram.com/tarionwarrantycorp/" title="Instagram"
                                           class="imgBoxe" target="_blank"></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col col-blog">
                                <h4 class="footer-title">Blog</h4>
                                <div id="rss-feeds"></div>
                                <br/>
                                <span class="read-more">
                        <a id="orangelink" href="http://blog.tarion.com/" target="_blank">Read More</a>
                      </span>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="footer-btm">
                    <div class="inner-footer">
                        <div class="footer-left">
                            <ul>
                                <li><a href="http://www.tarion.com/Pages/Terms-of-Use.aspx" target="_blank">Terms of Use</a>
                                </li>
                                <li><a href="http://www.tarion.com/Pages/Privacy.aspx" target="_blank">Privacy Policy</a>
                                </li>
                                <li><a href="http://builderlink.tarion.com/bsa/" target="_blank">BuilderLink Support</a></li>
                                <li><a href="http://www.tarion.com/about/accessibility/Pages/default.aspx"
                                       target="_blank">Accessibility</a></li>
                            </ul>
                        </div>
                        <div class="footer-right">
                            <ul>
                                <li>Copyright
                                    <script type="text/javascript">document.write(new Date().getFullYear());</script>
                                    Tarion
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <div class="printable">&#160;</div>
    <div class="pdf">&#160;</div>
</body>
</html>
