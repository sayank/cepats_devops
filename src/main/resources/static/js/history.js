function historyModal(formItemId) {

    var template = cepats.getTemplateAjax("history.modal.template");
    
    var data = getHistory(formItemId);
    
    $("#attachmentsModal")
        .html(template(data))
        .modal("show")
        .on("shown.bs.modal", function(){
        	printHistory();
        });
}


function getHistory(formItemId) {
	
	var result;
	var success = true;
	
	var basePath = $.sessionStorage.get("basePath");
    $.ajax({
        type: "GET",
        async: false,
        url: basePath + "getHistory/" + formItemId,
        cache: false,
        processData: false,
        contentType: false,
        success: function(response){
            console.log(JSON.stringify(response));
            if (response != null) {
                result = response;
            }
        },
        error: function(errorThrown) {
            success = false;
        },
        failure: function(response){
            success = false;
        }
    });
    
    if(!success)
    	return null;
    
    return result;
}


function printHistory() {
	$("#historyPrintButton")
	.off('click')
	.on("click", function(){
		
		$(".printable").html($(".modal-body").html());
		window.print();
	});
}

