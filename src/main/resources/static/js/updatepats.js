/**
 * Created by msalman on 24/09/2014.
 */

$("#uploadedForm")
    .bootstrapValidator({
        live: "enabled",
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            uploadedFile: {
                validators: {
                    notEmpty: {
                        message: "File is required"
                    },
                    file: {
                        maxSize: 10*1024*1024, //10 mb
                        extension: "xlsx",
                        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        message: "Please use the PATS Excel Template provided (File size cannot exceed 10MB)."
                    }
                }
            }
        }
    })
    .off("success.form.bv")
    .on("success.form.bv", function(e){

        e.preventDefault();
        console.log("upload file form is valid sending request to server");
        cepats.showWaitModal();

        var formData = new FormData();
        formData.append("file", $("[name='uploadedFile']").prop("files")[0]);
        cepats.saveForm("uploadPATS", formData, "Sorry, we are not able to complete your PATS upload. Please try again.");
    });

function updateItemModal(id) {

    var template = cepats.getTemplateAjax("update.item.modal.template");
    var data = {
        itemData: null,
        userInfo: $.sessionStorage.get("userInfo"),
        attachments: null
    };
    if (id != null) {
        data.itemData = cepats.getFormItem(id);
        var attList = getAttachmnents(id);
        data.attachments = attList;
    }

    $("#updateItemModal")
        .html(template(data))
        .modal("show")
        .on("shown.bs.modal", function(){
            saveItem();
        });
}

function saveItem() {

    $("#saveItemForm")
        .bootstrapValidator({
            live: "enabled",
            trigger: "blur",
//            container: "popover",
//            container: "tooltip",
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                // auditRefNum: {
                //     validators: {
                //         notEmpty: {
                //             message: "The PA Ref. # is a mandatory field. Please complete this field"
                //         },
                //         stringLength: {
                //             max: 95,
                //             message: "The PA Ref. # cannot exceed 95 characters. Please correct"
                //         }
                //     }
                // },
                // deficiencyDescription: {
                //     validators: {
                //         notEmpty: {
                //             message: "The Deficiency Description is a mandatory field. Please complete this field"
                //         },
                //         stringLength: {
                //             max: 1400,
                //             message: "The Deficiency Description cannot exceed 1400 characters. Please correct"
                //         }
                //     }
                // },
                // deficiencyLocation: {
                //     validators: {
                //         notEmpty: {
                //             message: "The Deficiency Location is a mandatory field. Please complete this field"
                //         },
                //         stringLength: {
                //             max: 1400,
                //             message: "The Deficiency Location cannot exceed 1400 characters. Please correct"
                //         }
                //     }
                // },
                vendorsResponse: {
                    validators: {
                        stringLength: {
                            max: 5000,
                            message: "The Vendor's Response cannot exceed 5000 characters. Please correct."
                        }
                    }
                },
                condoCorpResponse: {
                    validators: {
                        stringLength: {
                            max: 5000,
                            message: "The Condo Corp Response cannot exceed 5000 characters. Please correct."
                        }
                    }
                },
                attachment: {
                    validators: {
                        file: {
                            maxSize: 25*1024*1024, //25 mb
                            extension: "jpeg,jpg,pdf,doc,docx",
                            type: "image/jpeg,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                            message: "You can upload only in one of the following formats: doc, docx, pdf, jpg and jpeg (File size cannot exceed 25MB)."
                        }
                    }
                }
            }
        })
        .off('success.form.bv')		//Remove any previous event handlers attached to the success event
        .on('success.form.bv', function(e){

            e.preventDefault();
            console.log("save item form is valid sending request to server");
            var $form = $(e.target);
            $("#updateItemModal").modal("hide");
            cepats.showWaitModal();
            var itemData = {};
            $form.serializeArray().map(function(item){
               itemData[item.name] = item.value;
            });
            itemData = JSON.stringify(itemData);
            console.log(itemData);
            var formData = new FormData();
            formData.append("attachment", $("[name='attachment']").prop("files")[0]);
            formData.append("itemData", itemData);
            cepats.saveForm("saveItem", formData, "Sorry, we are not able to save item. Please try again.");
        });
    return false;
}

function getAttachmnents(formItemId) {

	var result;
	var success = true;

	var basePath = $.sessionStorage.get("basePath");
    $.ajax({
        type: "GET",
        async: false,
        url: basePath + "getAttachments/" + formItemId,
        processData: false,
        contentType: false,
        cache: false,
        success: function(response){
            console.log(JSON.stringify(response));
            if (response != null) {
                result = response;
            }
        },
        error: function(errorThrown) {
            success = false;
        },
        failure: function(response){
            success = false;
        }
    });

    if(!success)
    	return null;

    return result;
}
