var filters = {
		
		filteringByBlanks : false,

		initFilters : function(){
			
			$(document.body).on('click', '.stop-propagation, .filter-add-button', function(event){
				//The event won't be propagated to the document NODE and 
				// therefore events delegated to document won't be fired
				event.stopPropagation();
			});
		
	        
	        $(document.body).on('click', '#itemsPerPage li', function(event) {
			     var $target = $(event.currentTarget);
			     if($target.attr("data-id") == '50'){
			    	 cepats.itemsPerPage = 50;
			     }else if($target.attr("data-id") == '100'){
			    	 cepats.itemsPerPage = 100;
			     }else{
			    	 cepats.itemsPerPage = cepats.filteredData.length;
			     }
			     cepats.populateFormTable();
			 });
			 
			 // Check keyword matches as user types
			$(document.body).on('keyup', '#filter-keywords', function() {
				$("#filter-results").empty();
				// Retrieve the input field text
				var searchboxvalue = $("#filter-keywords").val().toLowerCase(), count = 0;
				var results = $(cepats.formData).filter(function(index, row) {
					description = row.deficiencyLocation;
					return description.toLowerCase().indexOf(searchboxvalue) !== -1;
				});
				$.each(results, function(i, f) {
					count++;
				});
				// Update the count
				if (count == 0 || searchboxvalue == ""){
					$("#filter-results").append('No Matches Found');
				} else if (count == 1){
					$("#filter-results").append('Match Found' + ':' + '&nbsp;' + count);
				} else {
					$("#filter-results").append('Matches' + ':' + '&nbsp;' + count);
				}
			});
			 
			// Add Keyword to Filter List
			 $(document.body).on('click', '#filter-kw-add', function() { 
				var count = $( "#filter-results" ).text();
				var searchboxvalue = $("#filter-keywords").val().trim();
				$("#filter-keywords").val('');
				if(count == "No Matches Found"){
					alert("There are no results for this keyword.");
				} else if ($("#filter-kw > button").length == 3) {
					alert("Maximum keywords (3) reached.");
				} else if (searchboxvalue == "") {
					alert("Please Enter a Keyword");
				} else {
					if($.inArray(searchboxvalue, cepats.deficiencyKeywords) == -1){
					 html = '<button class="keywords-button-li" id="'+searchboxvalue+'">' + searchboxvalue + '</button>';  
					$("#filter-kw").append(html);
					var newData = searchboxvalue;
					cepats.deficiencyKeywords.push(newData);
					$("#filter-results").empty();
					} else {
						alert("Keyword already added.")
					}
				}
			});
			
			// Remove Keyword
			$(document.body).on('click', '.keywords-button-li', function(){
				$(this).remove();
				filterKeywordID = $(this).attr("id");
				cepats.deficiencyKeywords.splice(cepats.deficiencyKeywords.indexOf(filterKeywordID), 1);
				filters.cummulativeFilter();				
			});
			
			// Filter by Deficiency Description
			$(document.body).on('click', '#apply-filter', function(){
				if(cepats.deficiencyKeywords.length == 0){
					alert("Please add serch keyword(s).")
				} else {
					filters.cummulativeFilter();	
				}
			});
			
			// Filter by Priority
			$(document.body).on('click', '#filterByPriority li, #filterByPriority input', function(event){
				var $target = $(event.currentTarget);
				var $targetDataId = $target.attr("data-id");
				if($targetDataId == 'ClearFilter'){
					filters.resetPriorityFilter();
				}else if($('#filterByPriority [data-id="'+$targetDataId+'"] input:checkbox').is(':checked')){
					$(this).prop('checked', false);
					cepats.priority.splice(cepats.priority.indexOf($targetDataId), 1);
					if(cepats.priority == ""){
						filters.resetPriorityFilter();
					} else {
						filters.cummulativeFilter();
					}
				}else{
					// Check if selection is in array, if not add 
					if($.inArray($targetDataId, cepats.priority) == -1){
						if($targetDataId == "null"){
							cepats.priority.push(null);
						} else {
							cepats.priority.push($targetDataId);
						}
					}
					filters.cummulativeFilter();
				}
			});
			 
			// Filter by Vendor's Position
			$(document.body).on('click', '#filterByVBPosition li, #filterByVBPosition input', function(event){
			    var $target = $(event.currentTarget);
				var $targetDataId = $target.attr("data-id");
			    if($targetDataId == 'ClearFilter'){
			    	filters.resetVendorPositionFilter();
			    } else if($('#filterByVBPosition [data-id="'+$targetDataId+'"] input:checkbox').is(':checked')){
					$(this).prop('checked', false);
					cepats.vendorsPosition.splice(cepats.vendorsPosition.indexOf($targetDataId), 1);
					if(cepats.vendorsPosition == ""){
						filters.resetVendorPositionFilter();
					} else {
						filters.cummulativeFilter();
					}
				} else {
					// Check if selection is in array, if not add 
					if($.inArray($targetDataId, cepats.vendorsPosition) == -1){
						if($targetDataId == "null"){
							cepats.vendorsPosition.push(null);
						} else {
							cepats.vendorsPosition.push($targetDataId);
						}
					}
					filters.cummulativeFilter();
				}
			});
	       
			// Filter by Condo Corp Response
			$(document.body).on('click', '#filterByCondoCorpPosition li, #filterByCondoCorpPosition input', function(event){
			    var $target = $(event.currentTarget);
				var $targetDataId = $target.attr("data-id");
			    if($targetDataId == 'ClearFilter'){
			    	 filters.resetCondoCorpPositionFilter();
			    } else if($('#filterByCondoCorpPosition [data-id="'+$targetDataId+'"] input:checkbox').is(':checked')){
					$(this).prop('checked', false);
					cepats.condoCorpPosition.splice(cepats.condoCorpPosition.indexOf($targetDataId), 1);
					if(cepats.condoCorpPosition == ""){
						filters.resetCondoCorpPositionFilter();
					} else {
						filters.cummulativeFilter();
					}
				} else {
					// Check if selection is in array, if not add 
					if($.inArray($targetDataId, cepats.condoCorpPosition) == -1){
						if($targetDataId == "null"){
							cepats.condoCorpPosition.push(null);
						} else {
							cepats.condoCorpPosition.push($targetDataId);
						}
					}
					filters.cummulativeFilter();
				} 
			});
			
	        $(document.body).on('click', '#clearFilterButton', function(event) {
	    	   filters.clearFilter();
		    });
	       
		},
		
		filterResults: function(){
			cepats.filteredData = cepats.formData;
			if(cepats.formData != null && cepats.formData.length && cepats.searchString != null){
				cepats.filteredData = [];
				cepats.currentPage = 1;
				for(var i=0;i<cepats.formData.length;i++){
					if(cepats.formData[i].deficiencyDescription != null){
						if(cepats.formData[i].deficiencyDescription.toUpperCase().indexOf(cepats.searchString.toUpperCase()) > -1){
							cepats.filteredData.push(cepats.formData[i]);
						}
					}
					if(cepats.formData[i].deficiencyLocation != null){
						if(cepats.formData[i].deficiencyLocation.toUpperCase().indexOf(cepats.searchString.toUpperCase()) > -1){
							cepats.filteredData.push(cepats.formData[i]);
						}
					}
					if(cepats.formData[i].vendorsPosition != null){
						if(cepats.formData[i].vendorsPosition.toUpperCase().indexOf(cepats.searchString.toUpperCase()) > -1){
							cepats.filteredData.push(cepats.formData[i]);
						}
					}
					if(cepats.formData[i].vendorsResponse != null){
						if(cepats.formData[i].vendorsResponse.toUpperCase().indexOf(cepats.searchString.toUpperCase()) > -1){
							cepats.filteredData.push(cepats.formData[i]);
						}
					}
					if(cepats.formData[i].condoCorpPosition != null){
						if(cepats.formData[i].condoCorpPosition.toUpperCase().indexOf(cepats.searchString.toUpperCase()) > -1){
							cepats.filteredData.push(cepats.formData[i]);
						}
					}
					if(cepats.formData[i].condoCorpResponse != null){
						if(cepats.formData[i].condoCorpResponse.toUpperCase().indexOf(cepats.searchString.toUpperCase()) > -1){
							cepats.filteredData.push(cepats.formData[i]);
						}
					}
				}
			}
			cepats.populateFormTable();
		},
		
		filterByItemId : function(){
			if(cepats.formData == null || !cepats.formData.length){
				return;
			}
			cepats.isFiltered = true;
			cepats.filteredData = [];
			cepats.currentPage = 1;
			for(var i=0;i<cepats.formData.length;i++){
					if(cepats.formData[i].itemId == cepats.searchItemId){
						cepats.filteredData.push(cepats.formData[i]);
						break;
					}
			}

			cepats.populateFormTable(true);
		},
		
		filterByRefNum : function(){
			if(cepats.formData == null || !cepats.formData.length){
				return;
			}
			cepats.isFiltered = true;
			cepats.filteredData = [];
			cepats.currentPage = 1;
			for(var i=0;i<cepats.formData.length;i++){
					if(cepats.formData[i].auditRefNum.trim() == cepats.searchRefNum.trim()){
						cepats.filteredData.push(cepats.formData[i]);
						break;
					}
			}

			cepats.populateFormTable(true);
		},
		
		// Cummulative Filter
		cummulativeFilter : function(){
			if(cepats.formData == null || !cepats.formData.length){
				return;
			}
			cepats.isFiltered = true;
			cepats.filteredData = cepats.formData;
			cepats.currentPage = 1;
			cepats.filteredData = [];
			
			var priorityArrayLengthNone;
			var vendorArrayLengthNone;
			var condoArrayLengthNone;
			
			// If Priority Array Empty, put all values in for cumulative search
			if(cepats.priority.length == 0){
				priorityArrayLengthNone = true;
				cepats.priority.push("1", "2", "3", "4", "5", null);
			}
			// If Vendor Array Empty, put all values in for cumulative search
			if(cepats.vendorsPosition.length == 0){
				vendorArrayLengthNone = true;
				cepats.vendorsPosition.push("Not Warranted", "Under Investigation", "Repair Proposed", "Repair In Progress", "Repair Complete", null);
			}
			// If Condo Array Empty, put all values in for cumulative search
			if(cepats.condoCorpPosition.length == 0){
				condoArrayLengthNone = true;
				cepats.condoCorpPosition.push("Warranty Disputed", "Repair Method Rejected", "Under Investigation", "Repair Method Accepted", "Deficiency Resolved", null);
			}
				
			for(var i=0;i<cepats.formData.length;i++){
				if($.inArray(cepats.formData[i].priority, cepats.priority) !== -1 && $.inArray(cepats.formData[i].vendorsPosition, cepats.vendorsPosition) !== -1 && $.inArray(cepats.formData[i].condoCorpPosition, cepats.condoCorpPosition) !== -1){
					if(cepats.deficiencyKeywords.length == 0 || typeof cepats.deficiencyKeywords[0] === 'undefined'){
						cepats.filteredData.push(cepats.formData[i]);
					} else if(typeof cepats.deficiencyKeywords[1] === 'undefined'){
						if (cepats.formData[i].deficiencyLocation.toLowerCase().indexOf(cepats.deficiencyKeywords[0].toLowerCase()) >= 0) {
							cepats.filteredData.push(cepats.formData[i]);
						} 	
					} else if(typeof cepats.deficiencyKeywords[2] === 'undefined'){
						if (cepats.formData[i].deficiencyLocation.toLowerCase().indexOf(cepats.deficiencyKeywords[0].toLowerCase()) >= 0 && cepats.formData[i].deficiencyLocation.toLowerCase().indexOf(cepats.deficiencyKeywords[1].toLowerCase()) >= 0) {
							cepats.filteredData.push(cepats.formData[i]);
						} 
					} else {
						if (cepats.formData[i].deficiencyLocation.toLowerCase().indexOf(cepats.deficiencyKeywords[0].toLowerCase()) >= 0 && cepats.formData[i].deficiencyLocation.toLowerCase().indexOf(cepats.deficiencyKeywords[1].toLowerCase()) >= 0 && cepats.formData[i].deficiencyLocation.toLowerCase().indexOf(cepats.deficiencyKeywords[2].toLowerCase()) >= 0) {
							cepats.filteredData.push(cepats.formData[i]);
						}
					}
				}
			}
			
			// If Priority Array was Empty prior to filter, remove all
			if(priorityArrayLengthNone == true){
				cepats.priority = [];
			}
			// If Vendor Array was Empty prior to filter, remove all
			if(vendorArrayLengthNone == true){
				cepats.vendorsPosition = [];
			}
			// If Condo Array was Empty prior to filter, remove all
			if(condoArrayLengthNone == true){
				cepats.condoCorpPosition = [];
			}
			
			cepats.populateFormTable(true);
		},
		
		goToPageNum : function(){
			cepats.filteredData = cepats.formData;
			cepats.isFiltered = true;
			cepats.populateFormTable();
			cepats.populateFormTable();
		},
		
		resetPriorityFilter : function(){
			cepats.priority = [];
			filters.cummulativeFilter();
	    }, 
		
		resetVendorPositionFilter : function(){
			cepats.vendorsPosition = [];
			filters.cummulativeFilter();
	    }, 
		
		resetCondoCorpPositionFilter : function(){
			cepats.condoCorpPosition = [];
			filters.cummulativeFilter();
	    },
		
	    clearFilter : function(){
			cepats.priority = [];
			cepats.vendorsPosition = [];
			cepats.condoCorpPosition = [];
			$('input:checkbox').prop('checked', false);
			cepats.isFiltered = false;
			cepats.filteredData = cepats.formData;
			cepats.searchString = null;
			cepats.populateFormTable();
	    }
		
		
		

}