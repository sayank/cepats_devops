package com.tarion.cepats.builders;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tarion.cepats.common.CEPATSUtil;
import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.domains.cepats.FormItemHistoryRepository;
import com.tarion.cepats.domains.cepats.FormItemRepository;
import com.tarion.cepats.domains.cepats.entities.FormItemEntity;
import com.tarion.cepats.domains.cepats.entities.FormItemHistoryEntity;
import com.tarion.cepats.domains.cepats.entities.FormTypeEnum;
import com.tarion.cepats.domains.cepats.entities.UserTypeEnum;
import com.tarion.cepats.services.FormService;
import com.tarion.cepats.services.VersionHistoryService;
import com.tarion.cepats.web.controllers.VersionHistoryController;
import com.tarion.cepats.web.dtos.ChangeDto;
import com.tarion.cepats.web.dtos.ItemHistoryDto;
import com.tarion.cepats.web.dtos.VersionChangeDto;
import com.tarion.cepats.web.dtos.VersionChangeDtoForItemHistoryModal;

@Component
public class VersionHistoryBuilder {
	
	@Autowired
	FormItemHistoryRepository formItemHistoryRepository;
	
	@Autowired
	FormItemRepository formItemRepository;
	
	@Autowired
    private FormService formService;
	
	/**
	 * @param formItemList
	 * @param formType
	 * @param lastChangeOnly
	 * @return
	 * @throws Exception
	 */
	public List<ItemHistoryDto> createItemHistoryList(String enrolmentNumber,  List<FormItemEntity> formItemList, 
				FormTypeEnum formType, boolean lastChangeOnly) throws Exception {
		List<ItemHistoryDto> itemHistoryDtoList = new ArrayList<>();

		for(FormItemEntity formItem : formItemList){

			List<FormItemHistoryEntity> formItemHistoryListForItemId = formItemHistoryRepository.findByEnrolmentNumberAndItemIdAndFormType(enrolmentNumber, formItem.getItemId(), formType);
			//if this is not an added item, and it hasn't been modified, 
			//skip history creation
			if(!formItem.isItemAdded() && formItemHistoryListForItemId.size() == 0){
				continue;
			}
			ItemHistoryDto itemHistoryDto = createItemHistoryDto(formItem, formItemHistoryListForItemId, lastChangeOnly);
			if(itemHistoryDto != null){
				itemHistoryDtoList.add(itemHistoryDto);
			}
		}
		if(itemHistoryDtoList.size() == 0){
			return null;
		}
		return itemHistoryDtoList;
	}
	
	public List<ItemHistoryDto> createItemHistoryList(String enrolmentNumber,  List<FormItemEntity> formItemList, 
			FormTypeEnum formType, boolean lastChangeOnly, Timestamp startTime, Timestamp endTime) throws Exception {
	List<ItemHistoryDto> itemHistoryDtoList = new ArrayList<>();

	for(FormItemEntity formItem : formItemList){

		//Replaced the existing call with start and end time  with the below call. ie. without passing the start and end time.
		//This is done to make sure that the oldest modified record durign a given time frame is also printed in the weekly email notification/download history.
		List<FormItemHistoryEntity> formItemHistoryListForItemId = 
				formItemHistoryRepository.findByEnrolmentNumberAndItemIdAndFormType(enrolmentNumber, formItem.getItemId(), formType);
		//if this is not an added item, and it hasn't been modified, 
		//skip history creation
		if(!formItem.isItemAdded() && formItemHistoryListForItemId.size() == 0){
			continue;
		}
		ItemHistoryDto itemHistoryDto = createItemHistoryDto(formItem, formItemHistoryListForItemId, startTime, endTime);
		if(itemHistoryDto != null){
			itemHistoryDtoList.add(itemHistoryDto);
		}
	}
	if(itemHistoryDtoList.size() == 0){
		return null;
	}
	return itemHistoryDtoList;
}
	
	public List<VersionChangeDto> getVersionChangesForItem(long itemId, String enrolmentNumber) {
		
		ArrayList<VersionChangeDto> result = new ArrayList<VersionChangeDto>();
		
		try {
			FormItemEntity entity = formItemRepository.findOne(itemId);
			
			//If the enrolment number does not match, the user is not authorized to view the data
			if(!entity.getEnrolmentNumber().equals(enrolmentNumber)) {
				return result;
			}
			
			FormItemEntity empty = new FormItemEntity();

			//If it's the first version, there isn't anything in the history table. Returns the values that the item vas created with
			if(entity.getVersion().equals(1)) {
				VersionChangeDto dto = createVersionChangeDto(entity, empty, entity.getEnrolmentNumber());
				result.add(dto);				
			}
			else {
				List<FormItemHistoryEntity> historyItems = 
						formItemHistoryRepository.findByEnrolmentNumberAndItemIdAndFormType(entity.getEnrolmentNumber(), entity.getItemId(), entity.getFormType());
				
				//This will throw exception if the list is empty, but this should not be empty if the version is higher than 1				
				result.add(createVersionChangeDto(entity, 
						historyItems.get(0), entity.getEnrolmentNumber()));
				
				for (int i = 0; i < historyItems.size() - 1; i++) {
					result.add(createVersionChangeDto(
							historyItems.get(i), 
							historyItems.get(i + 1), entity.getEnrolmentNumber()));
				}
				
				result.add(createVersionChangeDto(						
						historyItems.get(historyItems.size() - 1),
						empty, entity.getEnrolmentNumber()));
			}
		}
		
		catch (Exception e) {
			LoggerUtil.logError(VersionHistoryBuilder.class, e.getMessage(), e);
		}
		
		return result;
	}
	
	public VersionChangeDtoForItemHistoryModal getVersionChangesForItemHistoryModal(long itemId, 
			String enrolmentNumber) {
		LoggerUtil.logEnter(VersionHistoryBuilder.class, "info", "getVersionChangesForItemHistoryModal", enrolmentNumber, itemId);
		VersionChangeDtoForItemHistoryModal result = new VersionChangeDtoForItemHistoryModal();
		
		try {
			FormItemEntity entity = formItemRepository.findOne(itemId);
			LoggerUtil.logInfo(VersionHistoryBuilder.class, "info", "FormItemEntity id=" + entity.getId() + ", itemId=" + entity.getItemId());;
			
			//If the enrolment number does not match, the user is not authorized to view the data
			if(!entity.getEnrolmentNumber().equals(enrolmentNumber)) {
				return result;
			}
			
			FormItemEntity empty = new FormItemEntity();
			result.setAuditRefNum(getProperty(entity, "auditRefNum"));
			result.setDeficiencyDescription(getProperty(entity, "deficiencyDescription"));
			result.setDeficiencyLocation(getProperty(entity, "deficiencyLocation"));
			
			//If it's the first version, there isn't anything in the history table. Returns the values that the item vas created with
			if(entity.getVersion().equals(1)) {
				VersionChangeDto dto = createVersionChangeDtoForItemHistoryModal(entity, empty);
				result.addVersionChange(dto);
				return result;
			}

			List<FormItemHistoryEntity> historyItems = 
					formItemHistoryRepository.findByEnrolmentNumberAndItemIdAndFormType(entity.getEnrolmentNumber(), entity.getItemId(), entity.getFormType());
				
			//This will throw exception if the list is empty, but this should not be empty if the version is higher than 1				
			result.addVersionChange(createVersionChangeDtoForItemHistoryModal(entity, 
						historyItems.get(0)));
				
			for (int i = 0; i < historyItems.size() - 1; i++) {
				result.addVersionChange(createVersionChangeDtoForItemHistoryModal(
							historyItems.get(i), 
							historyItems.get(i + 1)));
			}
				
			result.addVersionChange(createVersionChangeDtoForItemHistoryModal(						
						historyItems.get(historyItems.size() - 1),
						empty));

		}
		catch (Exception e) {
			LoggerUtil.logError(VersionHistoryBuilder.class, e.getMessage(), e);
		}
		
		return result;
	}
	
	public VersionChangeDtoForItemHistoryModal getVersionChangesForDateRange(long itemId,  String enrolmentNumber, 
			Timestamp startTime, Timestamp endTime) {
		LoggerUtil.logEnter(VersionHistoryBuilder.class, "info", "getVersionChangesForDateRange", enrolmentNumber, itemId);
		VersionChangeDtoForItemHistoryModal result = new VersionChangeDtoForItemHistoryModal();
		
		try {
			FormItemEntity entity = formItemRepository.findOne(itemId);
			//empty entity to use for comparison for the latest version
			FormItemEntity empty = new FormItemEntity();
			
			//when  full history is requested, startTime and endTime are nulls
			if((startTime == null && endTime == null) || entity.getUpdatedDateTime().before(startTime)){
			
				//If the enrolment number does not match, the user is not authorized to view the data
				if(!entity.getEnrolmentNumber().equals(enrolmentNumber)) {
					return result;
				}
				
	
				result.setAuditRefNum(getProperty(entity, "auditRefNum"));
				result.setDeficiencyDescription(getProperty(entity, "deficiencyDescription"));
				result.setDeficiencyLocation(getProperty(entity, "deficiencyLocation"));
				
				//If it's the first version, there are no records for this item in the history table. 
				if(entity.getVersion().equals(1)) {
					VersionChangeDto dto = createVersionChangeDtoForItemHistoryModal(entity, empty);
					result.addVersionChange(dto);
					return result;
				}
			}
			
			List<FormItemHistoryEntity> historyItems = null;
			
			//when  full history is requested (startTime and endTime are nulls),
			//all versions need to be queried
			if(startTime == null && endTime == null && entity != null){
				historyItems = formItemHistoryRepository.findByEnrolmentNumberAndItemIdAndFormType(entity.getEnrolmentNumber(), 
						entity.getItemId(), entity.getFormType());

				result.addVersionChange(createVersionChangeDtoForItemHistoryModal(entity, 
							historyItems.get(0)));
			}else if(entity != null){
				historyItems 
					= formItemHistoryRepository.findByEnrolmentNumberAndFormTypeAndItemIdAndDateRange(entity.getEnrolmentNumber(), 
							entity.getItemId().intValue(), entity.getFormType().name(),  startTime, endTime);
			}
				
			for (int i = 0; i < historyItems.size() - 1; i++) {
				result.addVersionChange(createVersionChangeDtoForItemHistoryModal(
							historyItems.get(i), 
							historyItems.get(i + 1)));
			}
				
			result.addVersionChange(createVersionChangeDtoForItemHistoryModal(						
						historyItems.get(historyItems.size() - 1),
						empty));

		}
		
		catch (Exception e) {
			LoggerUtil.logError(VersionHistoryBuilder.class, e.getMessage(), e);
		}
		
		return result;
	}

	private VersionChangeDto createVersionChangeDto(Object updated, Object previous, String enrolmentNumber) throws Exception {
		VersionChangeDto dto = new VersionChangeDto();
		
		dto.setVersion((Integer)PropertyUtils.getSimpleProperty(updated, "version"));
		
		Timestamp updatedDate = (Timestamp)PropertyUtils.getSimpleProperty(updated, "updatedDateTime");
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(updatedDate);		
		dto.setVersionCreatedDate(date);
		
		String firstName = (String)PropertyUtils.getSimpleProperty(updated, "updatedUserFirstName");
		String lastName = (String)PropertyUtils.getSimpleProperty(updated, "updatedUserLastName");
		UserTypeEnum updatedUserType  = null;
		try {
			updatedUserType = (UserTypeEnum)PropertyUtils.getProperty(updated,"userType");			
		} catch (Exception e) {
			//it never supposed to happen
		}
		if(updatedUserType != null && updatedUserType == UserTypeEnum.TARION){
			dto.setChangedBy(UserTypeEnum.CEADMIN.getLabel());
		}else{
			dto.setChangedBy(firstName + " " + lastName);
		}
		dto.setChanges(createChangeDtoList(updated, previous, enrolmentNumber));
		return dto;
	}
	
	private VersionChangeDto createVersionChangeDtoForItemHistoryModal(Object updated, Object previous) throws Exception {
		VersionChangeDto dto = new VersionChangeDto();
		
		dto.setVersion((Integer)PropertyUtils.getSimpleProperty(updated, "version"));
		
		Timestamp updatedDate = (Timestamp)PropertyUtils.getSimpleProperty(updated, "updatedDateTime");
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(updatedDate);		
		dto.setVersionCreatedDate(date);
		
		String firstName = (String)PropertyUtils.getSimpleProperty(updated, "updatedUserFirstName");
		String lastName = (String)PropertyUtils.getSimpleProperty(updated, "updatedUserLastName");
		UserTypeEnum updatedUserType  = null;
		try {
			updatedUserType = (UserTypeEnum)PropertyUtils.getProperty(updated,"userType");			
		} catch (Exception e) {
			//it never supposed to happen
		}
		if(updatedUserType != null && updatedUserType == UserTypeEnum.TARION){
			dto.setChangedBy(UserTypeEnum.CEADMIN.getLabel());
		}else{
			dto.setChangedBy(firstName + " " + lastName);
		}
		dto.setChanges(createChangeDtoListForItemHistoryModal(updated, previous));
		return dto;
	}


	/**
	 * @param formItemHistoryList
	 * @param lastChangeOnly
	 * @param itemHistoryDtoList
	 * @param formItem
	 * @param formItemHistoryListForItemId
	 * @return
	 * @throws Exception
	 */
	private ItemHistoryDto createItemHistoryDto(FormItemEntity formItem, 
			List<FormItemHistoryEntity> formItemHistoryListForItemId, boolean lastChangeOnly)
			throws Exception {
		//create change DTO for latest version (compare values in FORM_ITEM table and FORM_ITEM_HISTORY table
		FormItemHistoryEntity latestChange = null;
		if(formItemHistoryListForItemId.size() > 0){
			latestChange = formItemHistoryListForItemId.get(0);
		}
		List<ChangeDto> changeDtoList = createChangeDtoList(formItem, latestChange, latestChange.getEnrolmentNumber());
		if (changeDtoList.size() == 0) {
			return null;
		}
		ItemHistoryDto itemHistoryDto =  createitemHistoryDtoForLatestVersion(formItem, changeDtoList);
		
		//if there is only one version, or if only last modification needed
		if(formItemHistoryListForItemId.size() == 1 || lastChangeOnly){
			return itemHistoryDto;
		}
		
		addFullHistory(formItemHistoryListForItemId, itemHistoryDto);
		return itemHistoryDto;
	}
	
	private ItemHistoryDto createItemHistoryDto(FormItemEntity formItem, 
			List<FormItemHistoryEntity> formItemHistoryListForItemId, Timestamp startTime, Timestamp endTime)
			throws Exception {

		Timestamp updatedDateTime  = formItem.getUpdatedDateTime();
		//if the record in FORM_ITEM table is outside of the requested date range
		if(!updatedDateTime.before(endTime) || !updatedDateTime.after(startTime)){
			return processItemHistoryEntitiesOnly(formItemHistoryListForItemId);
			
		}
		return processBothItemAndItemHistoryEntities(formItem, formItemHistoryListForItemId, startTime, endTime);
	}

	private ItemHistoryDto processBothItemAndItemHistoryEntities(FormItemEntity formItem,
			List<FormItemHistoryEntity> formItemHistoryListForItemId, Timestamp startTime, Timestamp endTime) throws Exception {
		ItemHistoryDto itemHistoryDto;
		//create change DTO for latest version (compare values in FORM_ITEM table and FORM_ITEM_HISTORY table
		FormItemHistoryEntity latestChange = null;
		if(formItemHistoryListForItemId.size() > 0){
			latestChange = formItemHistoryListForItemId.get(0);
		}
		List<ChangeDto> changeDtoList = createChangeDtoList(formItem, latestChange, latestChange.getEnrolmentNumber());
		if (changeDtoList.size() == 0) {
			return null;
		}
		itemHistoryDto =  createitemHistoryDtoForLatestVersion(formItem, changeDtoList);
		
		//if there is only one version, or if only last modification needed
		if(formItemHistoryListForItemId.size() == 1){
			return itemHistoryDto;
		}
		//get the full history between the timestamp instead of full history.
		addFullHistoryBetweenTimeStamp(formItemHistoryListForItemId, itemHistoryDto, startTime, endTime);
		return itemHistoryDto;
	}

	private ItemHistoryDto processItemHistoryEntitiesOnly(List<FormItemHistoryEntity> formItemHistoryListForItemId)
			throws Exception {
		ItemHistoryDto itemHistoryDto = null;
		if(formItemHistoryListForItemId.size() == 0){
			return null;
		}
		FormItemHistoryEntity latestChange = formItemHistoryListForItemId.get(0);
		FormItemHistoryEntity previuosChange = null;
		if(formItemHistoryListForItemId.size() > 1){
			 previuosChange = formItemHistoryListForItemId.get(1);
		}
		
		List<ChangeDto> changeDtoList = createChangeDtoList(latestChange, previuosChange, latestChange.getEnrolmentNumber());
		if (changeDtoList.size() == 0) {
			return null;
		}
		itemHistoryDto =  createitemHistoryDtoForLatestVersion(latestChange, changeDtoList);
		if(formItemHistoryListForItemId.size() == 1){
			return itemHistoryDto;
		}
		addFullHistory(formItemHistoryListForItemId, itemHistoryDto);
		return itemHistoryDto;
	}


	/**
	 * @param formItemHistoryListForItemId
	 * @param itemHistoryDto
	 * @throws Exception
	 */
	private void addFullHistory(List<FormItemHistoryEntity> formItemHistoryListForItemId, ItemHistoryDto itemHistoryDto)
			throws Exception {
		for (int i = 0; i < formItemHistoryListForItemId.size(); i++) {
			FormItemHistoryEntity latestFormItem = formItemHistoryListForItemId.get(i);
			FormItemHistoryEntity previousFormItem = formItemHistoryListForItemId.get(i + 1);
			List<ChangeDto> changes = createChangeDtoList(latestFormItem, previousFormItem, latestFormItem.getEnrolmentNumber());
			if (changes.size() == 0) {
				if (i == formItemHistoryListForItemId.size() - 2) {
					break;
				}else {
					continue;
				}
			}
			itemHistoryDto.getChanges().addAll(changes);

			if (i == formItemHistoryListForItemId.size() - 2) {
				break;
			}
		}
	}
	
	
	/**
	 * @param formItemHistoryListForItemId
	 * @param itemHistoryDto
	 * @throws Exception
	 * Replica of addFullHistory with the addition of getting the item history along between an interval.
	 */
	private void addFullHistoryBetweenTimeStamp(List<FormItemHistoryEntity> formItemHistoryListForItemId, ItemHistoryDto itemHistoryDto, Timestamp startTime, Timestamp endTime)
			throws Exception {
		for (int i = 0; i < formItemHistoryListForItemId.size(); i++) {
			FormItemHistoryEntity latestFormItem = formItemHistoryListForItemId.get(i);
			FormItemHistoryEntity previousFormItem = formItemHistoryListForItemId.get(i + 1);
			//Put the code here to check the time of the previousFormItem falls within the start and end time then continue else break.
			if((latestFormItem.getUpdatedDateTime()).after(startTime) && latestFormItem.getUpdatedDateTime().before(endTime)) {
				List<ChangeDto> changes = createChangeDtoList(latestFormItem, previousFormItem, latestFormItem.getEnrolmentNumber());
				if (changes.size() == 0) {
					if (i == formItemHistoryListForItemId.size() - 2) {
						break;
					}else {
						continue;
					}
				}
				itemHistoryDto.getChanges().addAll(changes);
	
				if (i == formItemHistoryListForItemId.size() - 2) {
					break;
				}
			}
			else {
				break;
			}
		}
	}

	/**
	 * @param formItem
	 * @param changeDtoList
	 * @return
	 */
	private ItemHistoryDto createitemHistoryDtoForLatestVersion(FormItemEntity formItem, List<ChangeDto> changeDtoList) {
		ItemHistoryDto itemHistoryDto = new ItemHistoryDto();
		itemHistoryDto.setItemId(formItem.getItemId());
		itemHistoryDto.setChanges(changeDtoList);
		return itemHistoryDto;
	}
	
	private ItemHistoryDto createitemHistoryDtoForLatestVersion(FormItemHistoryEntity formItemHistoryEntity, List<ChangeDto> changeDtoList) {
		ItemHistoryDto itemHistoryDto = new ItemHistoryDto();
		itemHistoryDto.setItemId(formItemHistoryEntity.getItemId());
		itemHistoryDto.setChanges(changeDtoList);
		return itemHistoryDto;
	}

	/**
	 * @param latest
	 * @param previous
	 * @return
	 * @throws Exception
	 */
	private List<ChangeDto> createChangeDtoList(Object latest, Object previous, String enrolmentNumber)
			throws Exception {
		List<ChangeDto> changeDtoList = new ArrayList<>();
		try {
			ChangeDto changeDto = createChangeDto(latest, previous, "auditRefNum", "PA Ref #");
			addToList(changeDtoList, changeDto);
			changeDto = createChangeDto(latest, previous, "deficiencyDescription", "Deficiency Description");
			addToList(changeDtoList, changeDto);
			changeDto = createChangeDto(latest, previous, "deficiencyLocation", "Deficiency Location");
			addToList(changeDtoList, changeDto);
			changeDto = createChangeDto(latest, previous, "priority", "Priority");
			addToList(changeDtoList, changeDto);
			changeDto = createChangeDto(latest, previous, "vendorsPosition", "Vendor's Position");
			addToList(changeDtoList, changeDto);
			changeDto = createChangeDto(latest, previous, "vendorsResponse", "Vendor's Response");
			addToList(changeDtoList, changeDto);
			changeDto = createChangeDto(latest, previous, "condoCorpPosition", "Condo Corp Position");
			addToList(changeDtoList, changeDto);
			changeDto = createChangeDto(latest, previous, "condoCorpResponse", "Condo Corp Response");
			addToList(changeDtoList, changeDto);
			if(formService.isCondoConversionForm(enrolmentNumber)){
				changeDto = createChangeDto(latest, previous, "fundingAndDescription", "PEF");
				addToList(changeDtoList, changeDto);
			}
			
		} catch (Exception e) {
			LoggerUtil.logError(VersionHistoryService.class, e.getMessage(), e);
		}
		return changeDtoList;
	}
	
	private List<ChangeDto> createChangeDtoListForItemHistoryModal(Object latest, Object previous)
			throws Exception {
		List<ChangeDto> changeDtoList = new ArrayList<>();
		try {
			ChangeDto changeDto = createChangeDto(latest, previous, "priority", "Priority");
			addToList(changeDtoList, changeDto);
			changeDto = createChangeDto(latest, previous, "vendorsPosition", "Vendor's Position");
			addToList(changeDtoList, changeDto);
			changeDto = createChangeDto(latest, previous, "vendorsResponse", "Vendor's Response");
			addToList(changeDtoList, changeDto);
			changeDto = createChangeDto(latest, previous, "condoCorpPosition", "Condo Corp Position");
			addToList(changeDtoList, changeDto);
			changeDto = createChangeDto(latest, previous, "condoCorpResponse", "Condo Corp Response");
			addToList(changeDtoList, changeDto);
			
		} catch (Exception e) {
			LoggerUtil.logError(VersionHistoryService.class, e.getMessage(), e);
		}
		return changeDtoList;
	}

	
	/**
	 * @param changeDtoList
	 * @param changeDto
	 */
	private void addToList(List<ChangeDto> changeDtoList, ChangeDto changeDto) {
		if (changeDto != null) {
			changeDtoList.add(changeDto);
		}
	}

	/**
	 * @param formItemLatest
	 * @param formItemPrevious
	 * @param fieldName
	 * @param changeName
	 * @return
	 * @throws Exception
	 */
	private ChangeDto createChangeDto(Object formItemLatest, Object formItemPrevious,
			String fieldName, String changeName) throws Exception {

		Object fieldValueLatest = PropertyUtils.getSimpleProperty(formItemLatest, fieldName);
		Object fieldValuePrevious = null;
		if(formItemPrevious != null){
			fieldValuePrevious = PropertyUtils.getSimpleProperty(formItemPrevious, fieldName);
		}
		if(fieldValueLatest == null){
			fieldValueLatest = "";
		}
		if(fieldValuePrevious == null){
			fieldValuePrevious = "";
		}
		if (fieldValueLatest.equals(fieldValuePrevious)) {
			return null;
		}

		ChangeDto changeDto = new ChangeDto();
		changeDto.setChangeName(changeName);
		changeDto.setNewValue((String) fieldValueLatest);
		changeDto.setOldValue((String) fieldValuePrevious);
		//updated user first name and last name can be null in database
		String updatedUserFirstName = "";
		String updatedUserLastName = "";
		UserTypeEnum updatedUserType  = null;
		try {
			updatedUserType = (UserTypeEnum)PropertyUtils.getProperty(formItemLatest,"userType");			
		} catch (Exception e) {
			//it never supposed to happen
		}
		if(updatedUserType != null && updatedUserType == UserTypeEnum.TARION){
			changeDto.setRevisedBy(UserTypeEnum.CEADMIN.getLabel());
		}else{
			try {
				updatedUserFirstName = (String)PropertyUtils.getSimpleProperty(formItemLatest,"updatedUserFirstName");
				updatedUserLastName = (String)PropertyUtils.getSimpleProperty(formItemLatest,"updatedUserLastName");			
			} catch (Exception e) {
				//do nothing - updated user first name and last name can be null in database
			}
			changeDto.setRevisedBy(CEPATSUtil.getFirstNameLastName(updatedUserFirstName, updatedUserLastName));
			
		}
		changeDto.setRevisedOnDate(CEPATSUtil.convertDateToString((Date)PropertyUtils.getSimpleProperty(formItemLatest,"updatedDateTime"), null));
		changeDto.setRevisedOnTime(CEPATSUtil.convertDateToTimeString((Date)PropertyUtils.getSimpleProperty(formItemLatest,"updatedDateTime"), null));

		changeDto.setVersion((Integer)PropertyUtils.getSimpleProperty(formItemLatest,"version"));
		return changeDto;
	}
	
	private String getProperty(Object formItemLatest, Object formItemPrevious,
			String fieldName)throws Exception{
		Object fieldValueLatest = PropertyUtils.getSimpleProperty(formItemLatest, fieldName);
		Object fieldValuePrevious = null;
		if(formItemPrevious != null){
			fieldValuePrevious = PropertyUtils.getSimpleProperty(formItemPrevious, fieldName);
		}
		if(fieldValueLatest == null){
			fieldValueLatest = "";
		}
		if(fieldValuePrevious == null){
			fieldValuePrevious = "";
		}
		if (fieldValueLatest.equals(fieldValuePrevious)) {
			return null;
		}
		return (String)fieldValueLatest;
	}
	
	private String getProperty(Object formItemLatest, String fieldName)throws Exception{
		Object fieldValueLatest = PropertyUtils.getSimpleProperty(formItemLatest, fieldName);
		
		if(fieldValueLatest == null){
			return "";
		}
		
		return (String)fieldValueLatest;
	}

}
