package com.tarion.cepats.services;

import java.sql.Timestamp;

import com.tarion.cepats.domains.cepats.entities.FormTypeEnum;
import com.tarion.cepats.web.dtos.UserDto;

public interface EmailNotificationService {
	
	public void sendInitialUploads(String enrolmentNumber, FormTypeEnum formType) throws Exception;
	public void sendUploadError(String enrolmentNumber, FormTypeEnum formType, String uploadSummary, String emailAddress, UserDto user) ;
	public void sendUpdates(FormTypeEnum formType, Timestamp timePeriod);
	public void forceNotifications(int numberOfDays);

}
