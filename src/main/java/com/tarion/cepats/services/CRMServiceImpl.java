package com.tarion.cepats.services;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tarion.cepats.domains.crm.AgreementLineRepository;
import com.tarion.cepats.domains.crm.EnrolmentRepository;
import com.tarion.cepats.domains.crm.PersonRepository;
import com.tarion.cepats.domains.crm.WSAssignedRepository;
import com.tarion.cepats.domains.crm.entities.AgreementLineEntity;
import com.tarion.cepats.domains.crm.entities.EnrolmentEntity;
import com.tarion.cepats.domains.crm.entities.PersonEntity;
import com.tarion.cepats.domains.crm.entities.WSCAssignedEntity;
import com.tarion.cepats.domains.crm.entities.WSRAssignedEntity;

@Service
public class CRMServiceImpl implements CRMService {
		
	@Autowired
    EnrolmentRepository enrolmentRepository;
	@Autowired
	AgreementLineRepository agreementLineRepository;
	@Autowired
	PersonRepository personRepository;
	@Autowired
	WSAssignedRepository wsAssignedRepository;
	
	@Override
	public String getCedefCaseId(String enrolmentNumber) {
		String cedefCaseId = enrolmentRepository.findCedefCaseIdByEnrolmentNumber(enrolmentNumber);
		return cedefCaseId;
	}


    @Override
    public EnrolmentEntity getEnrolment(String enrolmentNumber) {
        return enrolmentRepository.findByEnrolmentNumber(enrolmentNumber);
    }
    
    @Override
	public List<PersonEntity> getTarionUsers(String enrolmentNumber) {
   	return personRepository.findByUserTypeAndEnrolmentNumber(enrolmentNumber, "TARION");
    }
    
    @Override
	public List<PersonEntity> getCCAUsers(String enrolmentNumber) {
    	return personRepository.findByUserTypeAndEnrolmentNumber( enrolmentNumber, "CCA");
    }
	
	public AgreementLineEntity getAgreementLineEndDate(String enrolmentNumber){
		return agreementLineRepository.findAgreementLineEndDate(enrolmentNumber);
	}
	
	@Override
	public Timestamp getAgreementLineEndDateFirstYear(String enrolmentNumber){
		return agreementLineRepository.findAgreementLineForEnrolment(enrolmentNumber, 43, 25000);
	}
	
	@Override
	public Timestamp getAgreementLineEndDateSecondYear(String enrolmentNumber){
		return agreementLineRepository.findAgreementLineForEnrolment(enrolmentNumber, 44, 25000);
	}
	
	@Override
	public WSCAssignedEntity getWSCAssignedToCEPATS(String enrolmentNumber){
		return wsAssignedRepository.getWSCAssignedToCEPATS(enrolmentNumber);
	}
	
	@Override
	public WSRAssignedEntity getWSRAssignedToCEPATS(String enrolmentNumber){
		return wsAssignedRepository.getWSRAssignedToCEPATS(enrolmentNumber);
	}

}
