/* 
 * 
 * WebServicesClientRetrieval.java
 *
 * Copyright (c) 2014 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.cepats.services;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tarion.bsa.cepats.CepatsWS;
import com.tarion.bsa.cepats.CepatsWS_Service;
import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.services.exception.CepatsServiceException;
import com.tarion.tbi.bsa.cm.DocumentRetrieval;
import com.tarion.tbi.bsa.cm.DocumentRetrievalWSBean;
import com.tarion.tbi.cm.attachments.AttachmentsService;
import com.tarion.tbi.cm.attachments.AttachmentsWSBeanService;
import com.tarion.tbi.esp.service.EmailSender;
import com.tarion.tbi.esp.service.EmailSenderWSBean;
import com.tarion.tbi.services.CEPATSService;
import com.tarion.tbi.services.CEPATSServiceImplService;

/**
 * This Service provides Web Service Clients to other services in the application
 * 
 * @author <A href="bojan.volcansek@tarion.com">Walter Hill</A>
 * @since November 03, 2014
 * @version 1.0
 */
@Service
public class WebServicesClientRetrieval {
	private static final Logger logger = LoggerFactory.getLogger(WebServicesClientRetrieval.class);
	private EmailSenderWSBean emailSenderWebService;

	@Autowired
	private PropertyService propertyService;

	private EmailSenderWSBean getEmailSenderService() throws MalformedURLException {
		EmailSenderWSBean emailSenderWSBean = null;
		if (emailSenderWebService == null) {
			String wsdlUrlString = propertyService.getEspWsdl();
			URL webServiceUrl = new URL(wsdlUrlString);
			QName serviceName = new QName("http://service.esp.tbi.tarion.com/", "EmailSender");
	
			EmailSender locator = new EmailSender(webServiceUrl, serviceName);
			emailSenderWSBean= locator.getEmailSenderWSBeanPort();
		}
		return emailSenderWSBean;
	}

	public DocumentRetrievalWSBean getDocumentRetrievalService(){
		String wsdlUrlString = propertyService.getDocumentRetrievalWsdl();
		
		QName qName = new QName("http://cm.bsa.tbi.tarion.com/", "DocumentRetrieval");
		try {
			URL url = new URL(wsdlUrlString);
			DocumentRetrieval locator = new DocumentRetrieval(url, qName);
			DocumentRetrievalWSBean ret = locator.getDocumentRetrievalWSBeanPort();
			return ret;
		} catch (MalformedURLException e) {
			throw new CepatsServiceException("Invalid URL: " + wsdlUrlString, e);
		}
	}

	public AttachmentsService getAttachmentsService() {
		String wsdlUrlString = propertyService.getAttachmentUploadWsdl();
		
		QName qName = new QName("http://attachments.cm.tbi.tarion.com/", "AttachmentsWSBeanService");
		try {
			URL url = new URL(wsdlUrlString);
			AttachmentsWSBeanService locator = new AttachmentsWSBeanService(url, qName);
			AttachmentsService ret = locator.getAttachmentsServicePort();
			return ret;
		} catch (MalformedURLException e) {
			throw new CepatsServiceException("Invalid URL: " + wsdlUrlString, e);
		}
	}
	
	public CEPATSService getCEPATSService() {
		String wsdlUrlString = propertyService.getTipCepatsServiceWsdl();
		
		QName qName = new QName("http://services.tbi.tarion.com/", "CEPATSServiceImplService");
		try {
			URL url = new URL(wsdlUrlString);
			CEPATSServiceImplService locator = new CEPATSServiceImplService(url, qName);
			CEPATSService ret = locator.getCEPATSServicePort();
			return ret;
		} catch (MalformedURLException e) {
			throw new CepatsServiceException("Invalid URL: " + wsdlUrlString, e);
		}
	}

	public CepatsWS getCepatsService() {
		String wsdlUrlString = propertyService.getBsaCepatsServiceWsdl();
		
		QName qName = new QName("http://cepats.bsa.tarion.com/", "CepatsWS");
		try {
			URL url = new URL(wsdlUrlString);
			CepatsWS_Service locator = new CepatsWS_Service(url, qName);
			CepatsWS ret = locator.getCepatsWSBeanPort();
			return ret;
		} catch (MalformedURLException e) {
			throw new CepatsServiceException("Invalid URL: " + wsdlUrlString, e);
		}
	}

	public void sendEmailUsingEsp(String subject, String body, String fromAddress, String toAddresses) throws CepatsServiceException {
		
		LoggerUtil.logInfo(WebServicesClientRetrieval.class, "sendEmailUsingEsp - ENTER {} {} {} {} ", subject, body, fromAddress, toAddresses);
		try {
			getEmailSenderService().sendEmailWithText(subject, body, fromAddress, toAddresses, null, null, false, null, null, null);
		} catch (Exception e) {
			String exceptionMessage = "Exception occured while invoking TBI Web Service EmailSender.sendEmailWithText";
			logger.error(exceptionMessage, e);
			throw new CepatsServiceException(e.getMessage(), e);
		}
		LoggerUtil.logInfo(WebServicesClientRetrieval.class, "sendEmailUsingEsp - EXIT");
	}
	
}
