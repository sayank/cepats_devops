package com.tarion.cepats.services;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.tarion.cepats.common.CEPATSConstants;
import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.domains.cepats.SSORepository;
import com.tarion.cepats.domains.cepats.entities.SsoLoginEntity;

@Service
public class SSOServiceImpl implements SSOService {
		
	@Autowired
	SSORepository ssoRepository;
	
	@Override
	public void saveUser(String token, String userId, String firstName, String lastName, 
			String sourceSystem, String enrolmentNumber, String email, String userType) {

		long startTime = LoggerUtil.logEnter(SSOServiceImpl.class, "ENTER - saveUser({}, {}, {}, {}, {}, {}, {})",
                           token, userId, firstName, lastName, sourceSystem, enrolmentNumber, userType);
		
		SsoLoginEntity user = new SsoLoginEntity();
		user.setToken(token);
		user.setUserId(userId.trim());
		user.setEnrolmentNumber(enrolmentNumber.trim());
		if(firstName == null){
			user.setUserFirstName("");
		}else{
			user.setUserFirstName(firstName.trim());			
		}
		if(lastName == null){
			user.setUserLastName("");
		}else{
			user.setUserLastName(lastName.trim());
		}		
		user.setSourceSystem(sourceSystem.trim());
		user.setCreatedDateTime(new Timestamp(System.currentTimeMillis()));
		user.setEmailAddress(email.trim());
		user.setUserType(userType.trim());
		ssoRepository.save(user);
		LoggerUtil.logExit(SSOServiceImpl.class, "EXIT - saveUser()", user, startTime);
	}

	@Override
	public SsoLoginEntity loadUserByToken(String token) {
		long startTime = LoggerUtil.logEnter(SSOServiceImpl.class, "ENTER - findUser({})", token);
		SsoLoginEntity returnEntity = ssoRepository.findUser(token); 
		LoggerUtil.logExit(SSOServiceImpl.class, "loadUserByToken", returnEntity, startTime);
		return  returnEntity;

	}

	@Override
	public void deleteToken(String token) {
		long startTime = LoggerUtil.logEnter(SSOServiceImpl.class, "ENTER - deleteToken({})", token);
		ssoRepository.deleteToken(token);		
		LoggerUtil.logExit(SSOServiceImpl.class, "deleteToken", startTime);
	}
	
	public boolean isUserLoggedIn(String token) throws Exception{

		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (auth != null && !(auth instanceof AnonymousAuthenticationToken)) {
				if(auth.isAuthenticated()){
					return true;
				}
			}
		} catch (Exception e) {
			LoggerUtil.logServiceError(SSOServiceImpl.class, "isUserLoggedIn", e);
			throw e;
		}
		
		return false;
	}
	
	public void logUser(SsoLoginEntity user) {
		List<GrantedAuthority> grantedAuths = new ArrayList<>();
		grantedAuths.add(new SimpleGrantedAuthority(CEPATSConstants.ROLE_USER));
		if(!user.getUserType().equals(CEPATSConstants.MYHOME_CCA_READ_ONLY)){
			grantedAuths.add(new SimpleGrantedAuthority(CEPATSConstants.WRITE_ACCESS));
		}
		Authentication authentication = new UsernamePasswordAuthenticationToken(user, null, grantedAuths);
		SecurityContextHolder.getContext().setAuthentication(authentication);
	}
	
	public void deleteStaleTokens() {
		ssoRepository.deleteStaleTokens();
	}

}
