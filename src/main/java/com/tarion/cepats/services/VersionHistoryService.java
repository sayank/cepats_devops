package com.tarion.cepats.services;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.tarion.cepats.builders.VersionHistoryBuilder;
import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.common.TextUtil;
import com.tarion.cepats.domains.cepats.FormItemHistoryRepository;
import com.tarion.cepats.domains.cepats.FormItemRepository;
import com.tarion.cepats.domains.cepats.entities.FormItemEntity;
import com.tarion.cepats.domains.cepats.entities.FormTypeEnum;
import com.tarion.cepats.web.dtos.ItemHistoryDto;
import com.tarion.cepats.web.dtos.VersionChangeDto;
import com.tarion.cepats.web.dtos.VersionChangeDtoForItemHistoryModal;

@Service
public class VersionHistoryService {

	@Autowired
	FormItemHistoryRepository formItemHistoryRepository;
	
	@Autowired
	FormItemRepository formItemRepository;
	
	@Autowired
	VersionHistoryBuilder versionHistoryBuilder;
	
	/**
	 * This method returns list of itemHistoryDto for enrolment. 
	 * If "lastChangeOnly" is true, only last modification is returned for each item.
	 * Otherwise the whole history of each item is returned
	 * 
	 * Returns null if there were no form modifications after initial upload
	 * 
	 * @param enrolmentNumber
	 * @param startTime 
	 * @return
	 * @throws Exception
	 */
	public List<ItemHistoryDto> getVersionHistory(String enrolmentNumber, FormTypeEnum formType, 
			Timestamp startTime, Timestamp endTime, boolean lastChangeOnly) throws Exception {
		
		long start = LoggerUtil.logEnter(VersionHistoryService.class, "getFormHistory", enrolmentNumber, formType, lastChangeOnly);	
		List<FormItemEntity> formItemList =  null;
		if(startTime == null){
			formItemList = formItemRepository.findByEnrolmentNumberAndFormType(enrolmentNumber, formType);
		}else{			
			//Pass the start time and pick items whose updateTimeStamp is equal or after the startTimestamp.
			formItemList = formItemRepository.findByEnrolmentNumberAndFormTypeAndTimestamp(enrolmentNumber, formType, startTime, endTime);
		}
		if (formItemList.size() == 0) {
			return null;
		}
		List<ItemHistoryDto> itemHistoryDtoList =  
				versionHistoryBuilder.createItemHistoryList(enrolmentNumber, formItemList, formType, lastChangeOnly, startTime, endTime);
		LoggerUtil.logExit(VersionHistoryService.class, "getFormHistory", itemHistoryDtoList, start);
		return itemHistoryDtoList;
	}
	
	public List<VersionChangeDto> getHistory(long formId, String enrolmentNumber) {
		return versionHistoryBuilder.getVersionChangesForItem(formId, enrolmentNumber);
	}
	
	
	public VersionChangeDtoForItemHistoryModal getHistoryForItemModal(long id, String enrolmentNumber) {
		return versionHistoryBuilder.getVersionChangesForItemHistoryModal(id, enrolmentNumber);
	}
	
	public VersionChangeDtoForItemHistoryModal getHistoryForDateRange( 
			long itemId, String enrolmentNumber, String startDate, String endDate) {
		Timestamp start = new Timestamp(Long.getLong(startDate));
		Timestamp end = new Timestamp(Long.getLong(endDate));
		return versionHistoryBuilder.getVersionChangesForDateRange(itemId, enrolmentNumber, start, end);
	}
	
	public String buildFormUpdateHistory(String enrolmentNumber, FormTypeEnum formType, Timestamp startTime, Timestamp endTime) throws Exception {
		List<ItemHistoryDto> itemUpdateDtoList 
			= getVersionHistory(enrolmentNumber, formType, startTime, endTime, false);
		if(itemUpdateDtoList == null){
			return null;
		}
		return TextUtil.buildHistoryHTML(itemUpdateDtoList);
	}

}
