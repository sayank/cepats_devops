package com.tarion.cepats.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tarion.cepats.domains.cepats.ContentRepository;
import com.tarion.cepats.domains.cepats.entities.ContentEntity;
import com.tarion.cepats.web.dtos.ContentDto;

@Service
public class ContentServiceImpl implements ContentService {
		
	@Autowired
    ContentRepository contentRepository;
	
	List<ContentDto> contentDtos;

	@Override
	public List<ContentDto> findByContentType(String contentType) {
		if(contentDtos != null){
			return contentDtos;
		}
		List<ContentEntity> contentEntities =  contentRepository.findByContentType(contentType);
		contentDtos = adapt(contentEntities);
		return contentDtos;
	}
	
	private List<ContentDto> adapt(List<ContentEntity> contentEntities){
		 List<ContentDto> contentDtos = new ArrayList<>();
		 for(ContentEntity entity : contentEntities){
			 ContentDto dto = new ContentDto();
			 dto.setContentNumber(entity.getContentNumber());
			 dto.setContentText1(entity.getContentText1());
			 dto.setContentText2(entity.getContentText2());
			 contentDtos.add(dto);
		 }
		 
		 return contentDtos;
	}

}
