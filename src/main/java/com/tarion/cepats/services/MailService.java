package com.tarion.cepats.services;


import com.tarion.cepats.domains.cepats.entities.FormTypeEnum;
import com.tarion.cepats.web.dtos.UserDto;

public interface MailService {
	public void sendMailInitialUploadFirstYear(String toAddress, String name, String enrolmentNumber, boolean isVendorBuilder);
	public void sendMailInitialUploadSecondYear(String toAddress, String name, String enrolmentNumber, boolean isVendorBuilder);
	public void sendUploadError(String enrolmentNumber, FormTypeEnum formType, String uploadSummary, String emailAddress, UserDto user);
	public void sendFormUpdateFirstYear(String emailBody, String emailAddress, String name, String enrolmentNumber);
	public void sendFormUpdateSecondYear(String emailBody, String emailAddress, String name, String enrolmentNumber);
}
