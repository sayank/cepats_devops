package com.tarion.cepats.services;

import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.tarion.cepats.common.CEPATSConstants;
import com.tarion.cepats.common.CEPATSUtil;
import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.common.TextUtil;
import com.tarion.cepats.domains.cepats.DeleteAttachmentsRepository;
import com.tarion.cepats.domains.cepats.FileParser;
import com.tarion.cepats.domains.cepats.FormItemAttachmentRepository;
import com.tarion.cepats.domains.cepats.FormItemHistoryRepository;
import com.tarion.cepats.domains.cepats.FormItemRepository;
import com.tarion.cepats.domains.cepats.LoadFormRepository;
import com.tarion.cepats.domains.crm.CondoConversionRepository;
import com.tarion.cepats.domains.cepats.entities.FormItemEntity;
import com.tarion.cepats.domains.cepats.entities.FormItemHistoryEntity;
import com.tarion.cepats.domains.cepats.entities.FormTypeEnum;
import com.tarion.cepats.domains.cepats.entities.UpdateTypeEnum;
import com.tarion.cepats.domains.cepats.entities.UserTypeEnum;
import com.tarion.cepats.services.jaxbs.CepatsInLineItemsResponse;
import com.tarion.cepats.services.jaxbs.CepatsItemDto;
import com.tarion.cepats.validators.FormItemValidator;
import com.tarion.cepats.validators.impl.CCAValidator;
import com.tarion.cepats.validators.impl.TarionValidator;
import com.tarion.cepats.validators.impl.VBValidator;
import com.tarion.cepats.web.dtos.FileTypeEnum;
import com.tarion.cepats.web.dtos.FormItemDto;
import com.tarion.cepats.web.dtos.UpdateResultDto;
import com.tarion.cepats.web.dtos.UserDto;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Aug 28, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 */

/**
 * Service that handles operations against form:
 *** upload: invoke parser to parse uploaded file, validate it,
 ***    if no error and update is required then save items to database. Copy existing version to history table if required
 *** save: perform inline updates, the validation & save logic is same as upload
 *** isValidAndUpdateable: determine whether given new form item is valida and updatable
 * the remaining methods are pretty straight forward
 */

@Service
public class FormService {

    private static final String ERROR_MESSAGE_EMPTY_FILE = "Your PATS is blank. Please add items (PA Ref. #, Deficiency Description and Deficiency Location) in order to upload.";
    private static final String ERROR_MESSAGE_SYSTEM = "Sorry, we are not able to complete your PATS upload. Please try again.";
    private static final String ERROR_MESSAGE_DETAIL = "Your save item was unsuccessful. Please see below for error details.";
    private static final String ERROR_MESSAGE_WARRANTY_EXPIRED = "Cannot add a new item after warranty has expired";
    private static final String ERROR_MESSAGE_BUILDER_NEW_ITEM = "As a builder, you cannot add a new item";
    private static final String ERROR_MESSAGE_BUILDER_NEW_PATS = "As a builder, you cannot upload new PATS";
    private static final String ERROR_MESSAGE_LATE_SUBMISSION_CCA_FIRSTYEAR = "You are past the eligible First Year PATS submission date.";
    private static final String ERROR_MESSAGE_LATE_SUBMISSION_CCA_SECONDYEAR = "You are past the eligible Second Year PATS submission date.";
    private static final String ERROR_MESSAGE_SAVE_ITEM_FAILED = "Iternal error happened when saving item #";

    private static final String GENERAL_TEMPLATE_FILE_NAME = "PATS.xlsx";
    private static final String CONDO_CONVERSION_TEMPLATE_FILE_NAME = "PATS-CC.xlsx";

    private List<UpdateResultDto.InvalidItem> invalidItems;
    private Validator validator;

    @Autowired
    private FormItemRepository formItemRepository;
    @Autowired
    private LoadFormRepository loadFormRepository;
    @Autowired
    private FormItemHistoryRepository formItemHistoryRepository;
    @Autowired
    private FormItemAttachmentRepository formItemAttachmentRepository;
    @Autowired
    DeleteAttachmentsRepository deleteAttachmentsrepository;
    @Autowired
    private CondoConversionRepository condoConversionRepository;
    @Autowired
    private FileParser fileParser;
    @Autowired
    EmailNotificationService emailNotificationService;
    @Autowired
	private WebServicesClientRetrieval wsClientRetrieval;

    public FormService() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        this.validator = factory.getValidator();
    }
    
    public String getFormItems(String enrolmentNumber) {

		long startTime = LoggerUtil.logEnter(FormService.class, "getFormItems", enrolmentNumber);
    	List<FormItemEntity> formItemEntityList = formItemRepository.findAll(enrolmentNumber);
    	CepatsInLineItemsResponse cepatsInLineItemsResponse = new CepatsInLineItemsResponse();
    	cepatsInLineItemsResponse.setEnrolmentNumber(enrolmentNumber);
    	cepatsInLineItemsResponse.setSource(CEPATSConstants.CEPATS_CRM_DATA_SOURCE);
   
    	for (FormItemEntity formItemEntity : formItemEntityList) {
    		CepatsItemDto cepatsItemDto = new CepatsItemDto();
    		cepatsItemDto.setItemId(formItemEntity.getItemId().toString());
    		cepatsItemDto.setPerformanceAuditRefId(formItemEntity.getAuditRefNum());
    		// here is translation from Form Type code from CEPATS to CRM 1YR / 2YR happening
    		cepatsItemDto.setFormType(formItemEntity.getFormType().getLabel());
    		cepatsItemDto.setDeficiencyDescription(formItemEntity.getDeficiencyDescription());
    		cepatsItemDto.setDeficiencyLocation(formItemEntity.getDeficiencyLocation());
    		
    		cepatsInLineItemsResponse.getCepatsItem().add(cepatsItemDto);
    	}
    	
    	String responseXml = CEPATSUtil.convertJaxbToXml(cepatsInLineItemsResponse, CepatsInLineItemsResponse.class);
    	
		LoggerUtil.logExit(FormService.class, "getFormItems", responseXml, startTime);
    	return responseXml;
    }
    
    public FormItemEntity getFormItem(long formItemId) {
    	return formItemRepository.getFormItem(formItemId);
    }

    public UpdateResultDto upload(MultipartFile dataFile, UserDto user) {

        Long startTime = LoggerUtil.logEnter(FormService.class, "upload", user.getUsername(),
                                            user.getLoggedinUserType(), user.getUpdateAsUserType(),
                                            user.getEnrolment().getEnrolmentNumber(), user.getFormType());

        UpdateResultDto updateResult = new UpdateResultDto();
        updateResult.setError(true);
        boolean formSubmitted = isFormSubmitted(user.getEnrolment().getEnrolmentNumber(),  user.getFormType());
        if(!formSubmitted) {
        	if(!isInitialUploadAllowed(updateResult, user)) {
        		return updateResult;
        	}
        	updateResult.setInitialUpload(true);
        }

        try {
        	

            String fileName = dataFile.getOriginalFilename();
            String fileExtension = FilenameUtils.getExtension(fileName).toLowerCase();
            UpdateTypeEnum updateType = null;
            
            //reset file parser
            fileParser.setParsedItems(new ArrayList<FormItemEntity>());
            
            if (FileTypeEnum.CSV.getLabel().equals(fileExtension)) {
                 // Invoke CSV parser in FileParser to parse the file & validate each item

                updateResult.setMessage(fileParser.parseCsv(dataFile.getInputStream(), user, 
                		isCondoConversionForm(user.getEnrolment().getEnrolmentNumber())));
                updateType = UpdateTypeEnum.CSV;
            } else if (FileTypeEnum.EXCELX.getLabel().equals(fileExtension)) {
                 // Invoke Excel parser in FileParser to parse the file & validate each item

                updateResult.setMessage(fileParser.parseExcel(dataFile.getInputStream(), user, !formSubmitted, 
                		isCondoConversionForm(user.getEnrolment().getEnrolmentNumber())));
                updateType = UpdateTypeEnum.EXCEL;
            }else{
            	 updateResult.setMessage("You must correct the following errors before continuing:  .XLSX or .CSV file must be uploaded.");
                 updateResult.setError(true);
                 return updateResult;
            }
            
            if (!CEPATSUtil.isEmpty(updateResult.getMessage())) {
            	emailNotificationService.sendUploadError(user.getEnrolment().getEnrolmentNumber(), user.getFormType(), updateResult.getMessage(), user.getEmailAddress(), user);

            	updateResult.setError(true);
            	return updateResult;
            }

            // if no errors then retrieve the parsed items, validate and save them
            	Long dbAllItemsStart = LoggerUtil.logEnter(FormService.class, "databaseLookup -> getAllItems");
            	List<FormItemEntity> existingItems = formItemRepository.findAll(user.getEnrolment()
            			.getEnrolmentNumber(), user.getFormType());
            	LoggerUtil.logExit(FormService.class, "debug", "databaseLookup -> getAllItems", dbAllItemsStart);

                List<FormItemEntity> parsedItems = fileParser.getParsedItems();
                List<FormItemEntity> validItems = new ArrayList<>();
                List<FormItemHistoryEntity> itemsForVersioning = new ArrayList<>();
                this.invalidItems = new ArrayList<>();

                if (parsedItems.size() == 0) {
                    LoggerUtil.logError(FormService.class, "File was not uploaded because it was empty");
                    updateResult.setMessage(ERROR_MESSAGE_EMPTY_FILE);
                } else {



                    // set last item id to 1 if brand new form or to size of the list
                    Integer lastItemId = existingItems == null ? 0 : existingItems.size();
                    int rowNumber = 1;  // skip header

                    for(FormItemEntity newItem : parsedItems) {

                        rowNumber++;
                        FormItemEntity existingItem = null;

                        // retrieve existing item from the list 
                        //(only if the form was previously submitted - item ID might be not null in cases when Excel spreadsheet is reused from another form submission)
                        if (newItem.getItemId() != null && formSubmitted) {
                            // search in existing items
                            int index = Collections.binarySearch(existingItems, newItem,
                                                                 new Comparator<FormItemEntity>() {
                                @Override
                                public int compare(FormItemEntity o1, FormItemEntity o2) {
                                    return o1.getItemId().compareTo(o2.getItemId());
                                }
                            });

                            if (index < 0) {
                                newItem.setItemId(null);
                            } else {
                                existingItem = existingItems.get(index);
                            }
                        }

                        // a new item, set new itemId & version or skip the rest and continue iterating
                        if (existingItem == null) {
                            if (canAddNewItem(rowNumber, user)) {
                                newItem.setItemId(++lastItemId);
                                newItem.setVersion(1);

                                if(updateResult.isInitialUpload()) {
                                	newItem.setItemAdded(false);
                                } else {
                                	newItem.setItemAdded(true);
                                }
                            } else {
                                continue;
                            }
                        }

                        // proceed to validate item
                        if (isValidAndUpdateable(newItem, existingItem, rowNumber, updateType, user)) {
                            validItems.add(newItem);

                            //keep item added as false unless there is an existing item.
                            if (existingItem != null) {
                                itemsForVersioning.add(copyToHistoryEntity(existingItem));
                            }
                        }
                    }
                }

                if (invalidItems.size() > 0) {
                    LoggerUtil.logError(FormService.class, "File was not uploaded due to validation errors, prepare to send an email");
                    updateResult.setMessage(ERROR_MESSAGE_DETAIL);
                    updateResult.setInvalidItems(invalidItems);

                    String uploadSummary = TextUtil.generateUploadErrorsSummary(invalidItems);
                    emailNotificationService.sendUploadError(user.getEnrolment().getEnrolmentNumber(), user.getFormType(), uploadSummary, user.getEmailAddress(), user);
                } else {
                    if(validItems.size() > 0) {
                        this.saveForm(validItems, itemsForVersioning);
                        if(updateResult.isInitialUpload()) {
                        	emailNotificationService.sendInitialUploads(validItems.get(0).getEnrolmentNumber(), validItems.get(0).getFormType());
                        }
                    }
                    if (CEPATSUtil.isEmpty(updateResult.getMessage())) {
                        updateResult.setMessage("File uploaded successfully");
                        updateResult.setError(false);
                    }
                }

        } catch (Exception ex) {
            updateResult.setMessage(ERROR_MESSAGE_SYSTEM);
            LoggerUtil.logError(FormService.class, updateResult.getMessage(), ex);
        }

        LoggerUtil.logExit(FormService.class, "upload", updateResult, startTime);
        return updateResult;
    }


    public UpdateResultDto saveItem(FormItemDto formItem, UserDto user) {

        Long startTime = LoggerUtil.logEnter(FormService.class, "saveItem", user.getUsername(),
                                             user.getLoggedinUserType(), user.getUpdateAsUserType(),
                                             user.getEnrolment().getEnrolmentNumber(), user.getFormType());

        UpdateResultDto updateResult = new UpdateResultDto();
        updateResult.setError(true);       

        try {

        	if(isNewItem(formItem) && !canAddNewItem(updateResult, user)) {
        		return updateResult;
        	}
        	
            FormItemEntity newItem = new FormItemEntity();
            BeanUtils.copyProperties(formItem, newItem);
            FormItemEntity existingItem = null;
            this.invalidItems = new ArrayList<>();

            if (newItem.getItemId() == null) {

                // new item, lookup last item id in the database
                Integer lastItemId = formItemRepository.getLastItemId(user.getFormType(),
                                                                      user.getEnrolment().getEnrolmentNumber());

                newItem.setItemId(++lastItemId);
                newItem.setVersion(1);
                newItem.setItemAdded(true);
            } else {
                // Lookup existing item
                existingItem = formItemRepository.getExistingItem(newItem.getItemId(),
                                                                  user.getEnrolment().getEnrolmentNumber(),
                                                                  user.getFormType());
            }

            boolean doUpdate = isValidAndUpdateable(newItem, existingItem, 0, UpdateTypeEnum.INLINE, user);
            if (invalidItems.size() > 0) {
                LoggerUtil.logError(FormService.class, "Form Item was not saved due to validation errors");
                updateResult.setMessage(ERROR_MESSAGE_DETAIL);
                updateResult.setInvalidItems(invalidItems);
                updateResult.setShowErrorDetails(true);
            } else {

                // save item if can be updated
                if (doUpdate) {
                    // copy existing item version to history table if item already exists
                    if (existingItem != null) {
                        formItemHistoryRepository.save(copyToHistoryEntity(existingItem));
                    }
                    formItemRepository.save(newItem);
                }

                updateResult.setMessage("Item saved successfully");
                updateResult.setError(false);
            }
        } catch (Exception ex) {
            updateResult.setMessage(ERROR_MESSAGE_SAVE_ITEM_FAILED);
            LoggerUtil.logError(FormService.class, updateResult.getMessage(), ex);
        }

        LoggerUtil.logExit(FormService.class, "saveItem", startTime);
        return updateResult;
    }

	public XSSFWorkbook export(UserDto user) throws Exception {

        Long startTime = LoggerUtil.logEnter(FormService.class, "export",
                                             user.getEnrolment().getEnrolmentNumber(), user.getFormType(),
                                             user.getLoggedinUserType());


        InputStream fis = this.getClass().getClassLoader().getResourceAsStream(getTemplateName(user.getEnrolment().getEnrolmentNumber()));
        XSSFWorkbook patsForm = new XSSFWorkbook(fis);
        XSSFSheet sheet = patsForm.getSheetAt(0);

        List<FormItemEntity> formItems = formItemRepository.findAll(user.getEnrolment().getEnrolmentNumber(), user.getFormType());

        // we need to populate dropdowns even for empty rows in case 
        // user is adding new PATS on exported excel sheet
        int lastRow = CEPATSConstants.MAX_TYPICAL_NUMBER_OF_EXCEL_ROWS; 

        DataValidationHelper validationHelper = new XSSFDataValidationHelper(sheet);

        CellRangeAddressList priorityList = new CellRangeAddressList(1, lastRow, 4, 4);
        DataValidationConstraint constraint = validationHelper
                .createExplicitListConstraint(CEPATSConstants.priorityValues);
        DataValidation dataValidation = validationHelper.createValidation(constraint, priorityList);
        dataValidation.setSuppressDropDownArrow(true);
        sheet.addValidationData(dataValidation);

        CellRangeAddressList condoPositionList = new CellRangeAddressList(1, lastRow, 7, 7);
        constraint = validationHelper.createExplicitListConstraint(CEPATSConstants.condoPositionValues);
        dataValidation = validationHelper.createValidation(constraint, condoPositionList);
        dataValidation.setSuppressDropDownArrow(true);
        sheet.addValidationData(dataValidation);

        CellRangeAddressList vendorPositionList = new CellRangeAddressList(1, lastRow, 5, 5);
        constraint = validationHelper.createExplicitListConstraint(CEPATSConstants.vendorPositionValues);
        dataValidation = validationHelper.createValidation(constraint, vendorPositionList);
        dataValidation.setSuppressDropDownArrow(true);
        sheet.addValidationData(dataValidation);

        XSSFCellStyle cellStyleLeftAligned = createCommonStyle(patsForm);
        cellStyleLeftAligned.setAlignment(HorizontalAlignment.LEFT);
        
        XSSFCellStyle cellStyleCenterAligned = createCommonStyle(patsForm);
        cellStyleCenterAligned.setAlignment(HorizontalAlignment.CENTER);
       
        int endRow = 0;

        for (int i = 0; i < formItems.size(); i++) {

            FormItemEntity formItem = formItems.get(i);
            XSSFRow row = sheet.getRow(i + 1);
            if (row == null) {
                row = sheet.createRow(i + 1);
            }

            XSSFCell cell = getCell(row, 0, cellStyleCenterAligned);
            cell.setCellValue(formItem.getItemId());
            int columnIndex = cell.getColumnIndex();
            sheet.setColumnHidden(columnIndex, true);
            
            cell = getCell(row, 1, cellStyleCenterAligned);
            cell.setCellValue(formItem.getAuditRefNum());
            cell = getCell(row, 2, cellStyleLeftAligned);
            cell.setCellValue(formItem.getDeficiencyDescription());
            cell = getCell(row, 3, cellStyleLeftAligned);
            cell.setCellValue(formItem.getDeficiencyLocation());
            cell = getCell(row, 4, cellStyleLeftAligned);
            cell.setCellValue(formItem.getPriority());
            cell = getCell(row, 5, cellStyleLeftAligned);
            cell.setCellValue(formItem.getVendorsPosition());
            cell = getCell(row, 6, cellStyleLeftAligned);
            cell.setCellValue(formItem.getVendorsResponse());
            cell = getCell(row, 7, cellStyleLeftAligned);
            cell.setCellValue(formItem.getCondoCorpPosition());
            cell = getCell(row, 8, cellStyleLeftAligned);
            cell.setCellValue(formItem.getCondoCorpResponse());
            cell = getCell(row, 9, cellStyleLeftAligned);
            cell.setCellValue(formItem.getFundingAndDescription());
            if(i == formItems.size() - 1){
            	endRow = i;
            }
        }

//        sheet.autoSizeColumn(1);
//        sheet.autoSizeColumn(2);
//        sheet.autoSizeColumn(3);
//        sheet.autoSizeColumn(4);
//        sheet.autoSizeColumn(5);
//        sheet.autoSizeColumn(6);
//        sheet.autoSizeColumn(7);
//        sheet.autoSizeColumn(8);
//        sheet.autoSizeColumn(9);
        patsForm.setPrintArea(0, 0, 9, 0, endRow + 1);
        sheet.setDisplayGridlines(true);
        sheet.setPrintGridlines(true);
 
        LoggerUtil.logExit(FormService.class, "export", startTime);
        return patsForm;
    }
	

	private String getTemplateName(String enrolmentNumber) {
		if(isCondoConversionForm(enrolmentNumber)){
			return CONDO_CONVERSION_TEMPLATE_FILE_NAME;
		}
		return GENERAL_TEMPLATE_FILE_NAME;
	}

	public XSSFWorkbook getTemplate(UserDto user) throws Exception {

        Long startTime = LoggerUtil.logEnter(FormService.class, "getTemplate");

       InputStream fis = this.getClass().getClassLoader().getResourceAsStream(getTemplateName(user.getEnrolment().getEnrolmentNumber()));
        XSSFWorkbook patsForm = new XSSFWorkbook(fis);
        XSSFSheet sheet = patsForm.getSheetAt(0);
        sheet.setColumnHidden(0, true);
        sheet.getPrintSetup().setLandscape(true);
        sheet.getPrintSetup().setFitWidth((short)1);
        sheet.setFitToPage(true);
        LoggerUtil.logExit(FormService.class, "getTemplate", startTime);
        return patsForm;
    }

	private XSSFCellStyle createCommonStyle(XSSFWorkbook patsForm) {
		XSSFCellStyle cellStyle = patsForm.createCellStyle();
        cellStyle.getFont().setFontName("Calibri");
        cellStyle.setWrapText(true);
        cellStyle.setVerticalAlignment(VerticalAlignment.TOP);
		return cellStyle;
	}

    public boolean isFormSubmitted(String enrolmentNumber, FormTypeEnum formType) {
        long startTime = LoggerUtil.logEnter(FormService.class, "countByEnrolmentAndType", enrolmentNumber, formType);
        Long count = formItemRepository.countByEnrolmentNumberAndFormType(enrolmentNumber, formType);
        LoggerUtil.logExit(FormService.class, "isFormSubmitted numberOfItems={}", count, startTime);
        return count > 0;
    }

    /**
     * Retrieve form items from the database for given enrolment & formType
     * and then transfer the data to FormItemDto
     * <p/>
     * TODO: there has to be better way to display data to view by encapsulating entity without the use of a DTO
     * this is lots of boilerplate code, violates DRY and unnecessary copying classes back & forth
     *
     * @param enrolmentNumber
     * @param formType
     * @return
     */

    public List<FormItemDto> getItemsByEnrolmentAndType(String enrolmentNumber, FormTypeEnum formType, String userType) {

       Long startTime = LoggerUtil.logEnter(FormService.class, "getItemsByEnrolmentAndType",
                                            enrolmentNumber, formType);

       List<Object[]> formItemsData = null;
       List<FormItemDto> formItems = new ArrayList<FormItemDto>();
        try {
            LoggerUtil.logEnter(FileParser.class, "debug", "databaseLookup",
                                                     enrolmentNumber, formType);
            formItemsData = loadFormRepository.loadForm(enrolmentNumber, formType.toString());
            for (Object[] data : formItemsData) {
            	FormItemDto formItem = adapt(data, userType);
            	formItems.add(formItem);
            }
      
        } catch (Exception e) {
            LoggerUtil.logError(FormService.class, e.getMessage(), e);
        }

        LoggerUtil.logExit(FormService.class, "getItemsByEnrolmentAndType", startTime);
        return formItems;
    }
    

    private FormItemDto adapt(Object[] data, String userType) throws Exception{
    	FormItemDto item = new FormItemDto();
    	item.setId(new Long((Integer)data[0]));
    	item.setItemId((Integer)data[1]);
    	item.setAuditRefNum((String)data[2]);
    	item.setEnrolmentNumber((String)data[3]);
    	item.setFormType(FormTypeEnum.valueOf((String)data[4]));
    	item.setDeficiencyDescription((String)data[5]);
    	item.setDeficiencyLocation((String)data[6]);
    	item.setPriority((String)data[7]);
    	item.setVendorsPosition((String)data[8]);
    	item.setVendorsResponse((String)data[9]);
    	item.setCondoCorpPosition((String)data[10]);
    	item.setCondoCorpResponse((String)data[11]);
    	item.setFundingAndDescription((String)data[12]);
    	item.setIsCondoConversionForm(isCondoConversionForm((String)data[3]));

    	if((Integer)data[13] == null || (Integer)data[13] == 0){
    		item.setAttachments(false);       		
    	}else{
    		item.setAttachments(true);    
    	}
    	item.setUserType(userType);    	
    	item.setItemAdded((boolean)data[14]);
    	
    	item.setLastModifiedDate(CEPATSUtil.convertTimestampToString((Timestamp)data[15], CEPATSUtil.DATE_PATTERN_DATE_ONLY));
    	
    	try {
    		if(UserTypeEnum.valueOf((String)data[18]) == UserTypeEnum.TARION) {
        		item.setLastModifiedBy(UserTypeEnum.CEADMIN.getLabel());
        	} else {
        		item.setLastModifiedBy((String)data[16] + " " + (String)data[17]);
        	}
    	} catch(IllegalArgumentException e) {
    		LoggerUtil.logError(UserTypeEnum.class, "UserTypeEnum with the value " + (String)data[18] + " not found in UserTypeEnum.java. This is case sensitive.");
    	}
    	
    	
    	return item;
    }
    
    public List<String> getCEPATSForEnrolments(List<String> enrolments) {
        List<String> cepats = new ArrayList<>();
        for(String enrolmentNumber : enrolments) {
            int result =  formItemRepository.findCEPATSForEnrolmentNumber(enrolmentNumber);
            if(result > 0) {
                cepats.add(enrolmentNumber);
            }
        }
       
        return cepats;
    }
    
    public boolean isCondoConversionForm(String enrolmentNumber){
    	String condoConversionFlag = condoConversionRepository.findCondoConversionFlag(enrolmentNumber);
    	if(condoConversionFlag.equals("Y")){
    		return true;
    	}
    	return false;
    }

    
    /**
     * @param enrolmentNumber
     * @param formType
     * @throws Exception
     */
    public void deletePATS(String enrolmentNumber, String formType) throws Exception{
    	FormTypeEnum formTypeEnum = FormTypeEnum.valueOf(formType.toUpperCase());
    	//List<Long> idList = formItemRepository.findIDByEnrolmentNumberAndFormType(enrolmentNumber, formTypeEnum);
    	//formItemAttachmentRepository.deleteByIDs(idList);

    	deleteAttachmentsrepository.deleteAttachmentsByEnrolmentNumberAndFormType(enrolmentNumber, formType.toUpperCase());
    	formItemHistoryRepository.deleteByEnrolmentNumberAndFormType(enrolmentNumber, formTypeEnum);
    	formItemRepository.deleteByEnrolmentNumberAndFormType(enrolmentNumber, formTypeEnum);
    }
    
    public void deleteLineItem(String enrolmentNumber, int itemId, String formType) throws Exception{

    	formItemRepository.deleteLineItem(enrolmentNumber, itemId, FormTypeEnum.valueOf(formType.toUpperCase()));
    }
    
    

    /**
     * Save all items in uploaded form and copy existing version to history table if applicable
     *
     */
    private void saveForm(List<FormItemEntity> validItems, List<FormItemHistoryEntity>itemsForVersioning) {

        Long startTime = LoggerUtil.logEnter(FormService.class, "saveForm", validItems, itemsForVersioning);

        // copy existing item version to history table if there was any existing items were updated
        // NOTE: version of existing items should be saved first
        // so that new values don't overwrite existing values
        if (itemsForVersioning.size() > 0) {
            formItemHistoryRepository.save((Iterable<FormItemHistoryEntity>) itemsForVersioning);
        }
        formItemRepository.save((Iterable<FormItemEntity>) validItems);

        LoggerUtil.logExit(FormService.class, "saveForm", startTime);
    }
    
   

    private FormItemHistoryEntity copyToHistoryEntity(FormItemEntity existingItem) {

        Long startTime = LoggerUtil.logEnter(FormService.class, "copyToHistoryEntity");

        String[] ignoreFieldsForHistoryItem = {"id"};
        FormItemHistoryEntity formHistoryItem = new FormItemHistoryEntity();
        BeanUtils.copyProperties(existingItem, formHistoryItem, ignoreFieldsForHistoryItem);

        LoggerUtil.logExit(FormService.class, "copyToHistoryEntity", formHistoryItem, startTime);
        return formHistoryItem;
    }

    /**
     * Apply validation rules on FormItem
     * TODO maybe separate out user type and field level validations into two methods?
     * @param newFormItem
     * @param oldFormItem
     * @param rowNumber
     * @param updateType
     */
    private boolean isValidAndUpdateable(FormItemEntity newFormItem, FormItemEntity oldFormItem,
                                         int rowNumber, UpdateTypeEnum updateType, UserDto user) throws Exception {

        Long startTime = LoggerUtil.logEnter(FormService.class, "isValidAndUpdateable", rowNumber);

        // apply user type specific rules
        FormItemValidator userTypeValidator = getUserTypeValidator(user.getUpdateAsUserType());
        boolean isValidAndDoUpdate = userTypeValidator.validate(oldFormItem, newFormItem);

        if (isValidAndDoUpdate) {
            // verify field level validation
            Long javaxValidatorStart = LoggerUtil.logEnter(FormService.class, "debug", "javaxValidation", rowNumber);
            Set<ConstraintViolation<FormItemEntity>> constraintViolations = this.validator.validate(newFormItem);
            if (constraintViolations.size() > 0) {
                for(ConstraintViolation<FormItemEntity> constraintViolation : constraintViolations) {
                    String message = constraintViolation.getMessage();
                    LoggerUtil.logError(FormService.class, "Field level validation failed: rowNumber={}, message={}", rowNumber, message);
                    invalidItems.add(new UpdateResultDto.InvalidItem(rowNumber, message));
                }
                isValidAndDoUpdate = false;
            }
        }

        if (isValidAndDoUpdate) {
            // populate system related fields and save into FormItem table
            newFormItem.setUser(user.getUsername());
            newFormItem.setUpdatedUserFirstName(user.getFirstName());
            newFormItem.setUpdatedUserLastName(user.getLastName());
            newFormItem.setUpdatedUserEmailAddress(user.getEmailAddress());
            newFormItem.setUserType(user.getLoggedinUserType());
            newFormItem.setUpdateType(updateType);
            newFormItem.setUpdatedDateTime(new Timestamp(System.currentTimeMillis()));
            newFormItem.setFormType(user.getFormType());
            newFormItem.setEnrolmentNumber(user.getEnrolment().getEnrolmentNumber());
            if (oldFormItem != null) {
                newFormItem.setId(oldFormItem.getId());
                newFormItem.setVersion(oldFormItem.getVersion());
            }
        }

        LoggerUtil.logExit(FormService.class, "isValidAndUpdateable", isValidAndDoUpdate, startTime);
        return isValidAndDoUpdate;
    }

    /**
     * Check whether user can add a new item - this is used only when user
     * uploads form via a file
     * @return
     */
    private boolean canAddNewItem(int rowNumber, UserDto user) throws Exception {


        // is the builder attempting to add a new item?
        if (user.getLoggedinUserType().equals(UserTypeEnum.VB)) {
            invalidItems.add(new UpdateResultDto.InvalidItem(rowNumber, ERROR_MESSAGE_BUILDER_NEW_ITEM));
            return false;
        }

        // has warranty expired?
        if (!CEPATSUtil.isUnderWarranty(user.getFormType(), user) && !CEPATSUtil.isTarionUser(user.getLoggedinUserType())) {
            invalidItems.add(new UpdateResultDto.InvalidItem(rowNumber, ERROR_MESSAGE_WARRANTY_EXPIRED));
            return false;
        }

        return true;
    }
    

    /**
     * @param formItem
     * @return
     */
    private boolean isNewItem(FormItemDto formItem) {
		if(formItem.getItemId() == null){
			return true;
		}
		return false;
	}

    
    /**
     * Check whether user can add a new item (online editing)
     * @param updateResult
     * @throws Exception
     */
    private boolean canAddNewItem(UpdateResultDto updateResult, UserDto user) throws Exception {
    	
    	if(CEPATSUtil.isTarionUser(user.getLoggedinUserType())){
    		return true;
    	}
    	
    	if(CEPATSUtil.isVBUser(user.getLoggedinUserType())) {
    		updateResult.setMessage(ERROR_MESSAGE_BUILDER_NEW_ITEM);
    		updateResult.setError(true);
			return false;
		}

        // has warranty expired?
        if (!CEPATSUtil.isUnderWarranty(user.getFormType(), user)) {
        	updateResult.setMessage(ERROR_MESSAGE_WARRANTY_EXPIRED);
        	updateResult.setError(true);
            return false;
        }
 
        return true;
    }

	/**
	 * @param updateResult
	 * @param hasWarranty 
	 * @return
	 */
	private boolean isInitialUploadAllowed(UpdateResultDto updateResult, UserDto user) {
		
		if(CEPATSUtil.isVBUser(user.getLoggedinUserType())){
			updateResult.setMessage(ERROR_MESSAGE_BUILDER_NEW_PATS);
			return false;
		}
		
		if(CEPATSUtil.isUnderWarranty(user.getFormType(), user) || CEPATSUtil.isTarionUser(user.getLoggedinUserType())) {
			return true;
		}
		
		//If initial upload is late, 
		//return error message to the user
		if (CEPATSUtil.isFirstYearForm(user.getFormType())){
			updateResult.setMessage(ERROR_MESSAGE_LATE_SUBMISSION_CCA_FIRSTYEAR);
			updateResult.setError(true);
			return false;
		}
		
		if (CEPATSUtil.isSecondYearForm(user.getFormType())){
			updateResult.setMessage(ERROR_MESSAGE_LATE_SUBMISSION_CCA_SECONDYEAR);
			updateResult.setError(true);
			return false;
		}

		return false;
	}	
	

    private FormItemValidator getUserTypeValidator(UserTypeEnum userType) {

        FormItemValidator validator = null;

        switch (userType) {
            case CCA:
                validator = new CCAValidator();
                break;
            case VB:
                validator = new VBValidator();
                break;
            case TARION:
                validator = new TarionValidator();
                break;
            default:
                break;
        }
        return validator;
    }
       
    private XSSFCell getCell(XSSFRow row, int index, XSSFCellStyle cellStyle) {

        XSSFCell cell = row.getCell(index);
        if (cell == null) {
            cell = row.createCell(index);
        }
        cell.setCellStyle(cellStyle);

        return cell;
    }

    
}
