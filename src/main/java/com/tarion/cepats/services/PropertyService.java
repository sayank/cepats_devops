/**
 *
 */
package com.tarion.cepats.services;

import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.domains.cepats.ConfigRepository;
import com.tarion.cepats.domains.cepats.entities.ConfigEntity;
import com.tarion.common.configspringboot.TarionEnvConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Properties;

/**
 * Service to Access the Properties File and Properties table.
 *
 * @author <A href="mailto:joni.paananen@tarion.com">Joni Paananen</A>
 *
 * @since Jan 20, 2015
 */
@Service
public class PropertyService {

    private Properties props;

    @Autowired
    private TarionEnvConfigService envConfigService;

    @Autowired
    private ConfigRepository configRepository;

    private void loadDbPropeties() {
        LoggerUtil.logInfo(PropertyService.class, "ENTER loadDbProperties");
        props = envConfigService.loadEnvProperties("cepats");

        List<ConfigEntity> configEntityList = configRepository.findAll();

        for (ConfigEntity configEntity : configEntityList) {
            props.setProperty(configEntity.getName(), configEntity.getValue());
        }

        LoggerUtil.logInfo(PropertyService.class, "EXIT loadDbProperties: " + props);
    }

    public String getBuilderVideoTutorialLink() {
        return getValue("cepats.builder.video.tutorial.link");
    }

    public String getBuilderTutorialLink() {
        return getValue("cepats.builder.tutorial.link");
    }

    public String getCondoCorpVideoTutorialLink() {
        return getValue("cepats.condoCorp.video.tutorial.link");
    }

    public String getCondoCorpTutorialLink() {
        return getValue("cepats.condoCorp.tutorial.link");
    }


    public String getDefaultEmail() {
        return getValue("cepats.default.email.address");
    }


    public String getJmsDestinationNode() {
        return getEnvironmentDependentValue("cepats.jms_destination_node");
    }


    public String getInquiresPhoneNumber() {
        return getValue("cepats.inquiries.phone.number");
    }

    // Following five values contain replacement tokens
    public String getEspWsdl() {
        return getEnvironmentDependentValue("cepats.ws.client.tbi_email_sender.wsdl.url");
    }

    public String getDocumentRetrievalWsdl() {
        return getEnvironmentDependentValue("cepats.ws.client.tbi_cm_document_retrieval.wsdl.url");
    }

    public String getAttachmentUploadWsdl() {
        return getEnvironmentDependentValue("cepats.ws.client.tbi_cm_attachment_upload.wsdl.url");
    }

    public String getTipCepatsServiceWsdl() {
        return getEnvironmentDependentValue("cepats.ws.client.tbi_cepats_service.wsdl.url");
    }

    public String getBsaCepatsServiceWsdl() {
        return getEnvironmentDependentValue("cepats.ws.client.bsa_cepats.wsdl.url");
    }

    // These properties file were in cepats.properties before, which was not dependent on environment
    public String getCrmResponseQueueMessageName() {
        return "TWC_SOL_IMPORT_RSP.V1";
    }
    public String getCrmResponseQueueRequestingNode() {
        return "TWC_CEPATS_SOL_RSP";
    }
    public String getCrmResponseQueueJMSProvider() {
        return "IPlanet";
    }
    public String getCrmResponseQueueMessageType() {
        return "async";
    }
    public String getCrmResponseQueueVersion() {
        return "V1";
    }
    public String getCrmResponseQueueJMSMessageType() {
        return "Text";
    }
    public String getCrmResponseQueuePassword() {
        return "password";
    }

    /**
     * Given a key, return the value in the database.
     *
     * @param key the key
     * @return The value, or Null if not found
     */
    public String getValue(String key) {
        LoggerUtil.logInfo(PropertyService.class, String.format("Getting property value for %s", key));

        if (props == null) {
            loadDbPropeties();
        }

        String propertyValue = props.getProperty(key);
        LoggerUtil.logInfo(PropertyService.class, String.format("value -> %s", propertyValue));

        return propertyValue;
    }

    private String getEnvironmentDependentValue(String name) {
        return getValue(name, getValue("env.domain"));
    }

    /**
     * Method to get value of property key with list of values to format the string.
     * The placeholders are in the form of {0}, {1} etc
     *
     * @param key    the key
     * @param values The values to substitute into the string
     * @return The property, null if not found
     */
    private String getValue(String key, String... values) {
        LoggerUtil.logInfo(PropertyService.class, String.format("Getting property value for %s", key));
        if (props == null) {
            loadDbPropeties();
        }
        String tempString = props.getProperty(key);
        return replaceNumberedTokens(tempString, values);
    }

    /**
     * Replaces numbered tokens in the template string with given values. The tokens
     * format is {0}, {1} and so on
     */
    private String replaceNumberedTokens(String template, String... values) {
        String ret = template;
        if (ret != null && values != null) {
            for (int i = 0; i < values.length; i++) {
                ret = ret.replaceAll("\\{" + i + "\\}", values[i]);
            }
        }
        LoggerUtil.logInfo(PropertyService.class, String.format("value -> %s", ret));
        return ret;
    }

}