/* 
 * 
 * EnrolmentsBean.java
 *
 * Copyright (c) 2012 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.cepats.services.attachments;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tarion.cepats.common.CEPATSConstants;
import com.tarion.cepats.common.CEPATSUtil;
import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.domains.cepats.FormItemAttachmentRepository;
import com.tarion.cepats.domains.cepats.entities.FormItemAttachmentEntity;
import com.tarion.cepats.domains.cepats.entities.FormItemEntity;
import com.tarion.cepats.domains.cepats.entities.UserTypeEnum;
import com.tarion.cepats.services.CRMService;
import com.tarion.cepats.services.PropertyService;
import com.tarion.cepats.services.WebServicesClientRetrieval;
import com.tarion.cepats.web.dtos.UpdateResultDto;
import com.tarion.cepats.web.dtos.UserDto;
import com.tarion.tbi.cm.attachments.Attachments;
import com.tarion.tbi.cm.attachments.AttachmentsService;
import com.tarion.tbi.cm.attachments.CmAttribute;
import com.tarion.tbi.cm.attachments.ItemTypes;
import com.tarion.tbi.cm.attachments.ReferenceInfo;
import com.tarion.tbi.cm.attachments.SubmissionResponse;

/**
 * The Class UploadAttachmentsBeanRemote is used for integration with Content
 * Manager, via TBI Attachments Web Service.
 * 
 * @author <A href="alex.basaric@tarion.com">Alex Basaric</A>
 * @since 20-Jun-2013
 * @version 1.0
 */
@Service
public class AttachmentUploadService {

	private static final int BSA_UPLOAD_XREF_ERR_MSG_LENGTH = 1024;

	private static final Long CEPATS_TB_UPLOAD_SOURCE_ID = 2L;

	@Autowired
	private FormItemAttachmentRepository formItemAttachmentRepository;
	@Autowired
	PropertyService propertyService;
	@Autowired
	private WebServicesClientRetrieval wsClientRetrieval;
	@Autowired
	private CRMService crmService;

	private static final String JPQL_GET_BSA_UPLOAD_TYPES_BY_APPLICATION_TYPE = " SELECT e from BsaUploadDocTypeXref e WHERE e.bsaApplicationType = ?1 " + " order by e.docType ";

	private static final String JPQL_GET_BSA_UPLOAD_TYPES_BY_APPLICATION_TYPE_FOR_CE = " SELECT e from BsaUploadDocTypeXref e WHERE e.bsaApplicationType = ?1 " + " order by e.docType ";

	private FormItemAttachmentEntity insertEmptyFormItemAttachmentTable() {
		FormItemAttachmentEntity xref = new FormItemAttachmentEntity();
		xref.setSubmitCrmWorklist(false);
		FormItemAttachmentEntity savedEntity = saveFormItemAttachmentTable(xref);
		return savedEntity;
	}
	// @Override
	// public List<BsaUploadDocTypeXref> findCepatsUploadDocTypeXref() {
	// Query query =
	// bsaEm.createQuery(JPQL_GET_BSA_UPLOAD_TYPES_BY_APPLICATION_TYPE_FOR_CE);
	// query.setParameter(1, BsaApplicationType.CE);
	//
	// List<BsaUploadDocTypeXref> ret = query.getResultList();
	// return ret;
	// }
	//
	public void insertFormItemAttachmentTable(String trackingNumber, String fileName, Attachments attachments, SubmissionResponse submissionResponse, String userName, UserTypeEnum userType, FormItemEntity formItem, boolean submitCrmWorklist) {
		FormItemAttachmentEntity xref = new FormItemAttachmentEntity();
		xref = populateFormItemAttachmentEntityValues(xref, trackingNumber, fileName, attachments, submissionResponse, userName, userType, formItem, submitCrmWorklist);
		saveFormItemAttachmentTable(xref);
	}
	
	private void updateFormItemAttachmentTable(FormItemAttachmentEntity xref, String trackingNumber, String fileName, Attachments attachments, SubmissionResponse submissionResponse, String userName, UserTypeEnum userType, FormItemEntity formItem, boolean submitCrmWorklist) {
		//Column size in database (which holds file name)  is 100 char. File name needs to be truncated if it's longer than 100 char
		String truncatedFileName = truncateFileName(fileName);
		xref = populateFormItemAttachmentEntityValues(xref, trackingNumber, truncatedFileName, attachments, submissionResponse, userName, userType, formItem, submitCrmWorklist);
		saveFormItemAttachmentTable(xref);
	}
	
	private FormItemAttachmentEntity populateFormItemAttachmentEntityValues(FormItemAttachmentEntity xref, String trackingNumber, String fileName, Attachments attachments, SubmissionResponse submissionResponse, String userName, UserTypeEnum userType, FormItemEntity formItem, boolean submitCrmWorklist) {
		AttachmentUploadStatus status = AttachmentUploadStatus.SUCCESS;
		StringBuilder errMsg = new StringBuilder();
		for (ReferenceInfo refInfo : submissionResponse.getReferenceInfoList().getReferenceInfo()) {
			if (refInfo.getStatus().equalsIgnoreCase("FAILURE")) {
				status = AttachmentUploadStatus.FAILURE;
				errMsg.append(errMsg.length() > 0 ? ", " : "");
				errMsg.append(refInfo.getErrorMsg());
			}
		}

		xref.setAttachmentUploadStatus(status);

		List<ReferenceInfo> refInfos = submissionResponse.getReferenceInfoList().getReferenceInfo();

		String itemId = refInfos.get(0).getCmItemId();
		if (refInfos.size() > 1) {
			itemId = itemId + " and " + (refInfos.size() - 1) + " more";
		}

		xref.setFormItem(formItem);
		xref.setCmItemId(itemId);
		xref.setUploadedUser(userName);
		xref.setReferenceNumber(trackingNumber);
		xref.setUploadedDateTime(new Timestamp(System.currentTimeMillis()));
		xref.setOriginalFileName(fileName);
		xref.setMimeType(attachments.getAttachment().get(0).getFileType());
		xref.setSubmitCrmWorklist(submitCrmWorklist);
		xref.setUploadedUserType(userType);

		// We need attributes from the first attachments, to extract
		// enrolment number, case id, vbnumber if available
		List<CmAttribute> cmAttributes = attachments.getAttachment().get(0).getCmAttributes().getCmAttribute();
		for (CmAttribute attr : cmAttributes) {
			if (attr.getName().equalsIgnoreCase("EnrolmentNumber")) {
				xref.setEnrolmentNumber(Long.valueOf(attr.getValue()));
			}
			if (attr.getName().equalsIgnoreCase("CaseId")) {
				xref.setCaseId(attr.getValue());
			}
			if (attr.getName().equalsIgnoreCase("vbNumber")) {
				xref.setVbNumber(attr.getValue());
			}
		}
		
		if (status == AttachmentUploadStatus.FAILURE) {
			String attachmentsXml = AttachmentBuilder.toXmlString(attachments);
			xref.setAttachmentXml(attachmentsXml);
			xref.setErrorMsg(errMsg.length() > 1024 ? errMsg.substring(0, BSA_UPLOAD_XREF_ERR_MSG_LENGTH) : errMsg.toString());
		}
		
		return xref;
	}

	private FormItemAttachmentEntity saveFormItemAttachmentTable(FormItemAttachmentEntity entity) {
		entity = formItemAttachmentRepository.save(entity);
		return entity;
	}

	// @Override
	// public BsaUploadXref findBsaUploadXref(Long theId) {
	// return bsaEm.find(BsaUploadXref.class, theId);
	// }
	//
	// @Override
	// public void deleteBsaUploadXref(Long theId) {
	// BsaUploadXref toBeDeleted = findBsaUploadXref(theId);
	// bsaEm.remove(toBeDeleted);
	//
	// }

	public List<FormItemAttachmentEntity> findFailedUploads() {
		List<FormItemAttachmentEntity> formItemEntityList = formItemAttachmentRepository.findAllFormItemAttachmentForAttachmentUploadStatus(AttachmentUploadStatus.FAILURE);
		return formItemEntityList;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public SubmissionResponse resubmitFailedUploads(Long formItemAttachmentId, String userName) throws Exception {
		FormItemAttachmentEntity formItemAttachmentEntity = formItemAttachmentRepository.findFormItemAttachmentForId(formItemAttachmentId);
		String xml = formItemAttachmentEntity.getAttachmentXml();
		Attachments attachments = AttachmentBuilder.fromXmlString(xml);
		String enrNum = String.valueOf(formItemAttachmentEntity.getFormItem().getEnrolmentNumber());
		SubmissionResponse ret;
		if (formItemAttachmentEntity.isSubmitCrmWorklist()) {
			ret = uploadFilesToCm(enrNum, attachments, userName);
		} else {
			ret = uploadFilesToCmWithoutCrmWorklistCreation(enrNum, attachments, userName);
		}
		return ret;
	}

	public UpdateResultDto uploadAttachment(String filename, byte[] data, String mimeType, UserDto userDto, FormItemEntity formItem) {

		AttachmentDto dto = new AttachmentDto();
		dto.getItemAttributes().put(CEPATSConstants.CM_ATTR_CASE_ID, "");
		dto.getItemAttributes().put(CEPATSConstants.CM_ATTR_ENROLMENT_NUMBER, CEPATSUtil.removeHFromEnrolment(userDto.getEnrolment().getEnrolmentNumber()));
		dto.getItemAttributes().put(CEPATSConstants.CM_ATTR_VB_NUMBER, CEPATSUtil.removeBFromVB(userDto.getEnrolment().getVbNumber()));
		dto.getItemAttributes().put(CEPATSConstants.CM_TEMPLATE_TYPE, CEPATSConstants.CM_CEPATS_TEMPLATE_TYPE);
		dto.getItemAttributes().put(CEPATSConstants.CM_ATTR_SCAN_DATE, CEPATSUtil.todayFormattedForCm());
		dto.getItemAttributes().put(CEPATSConstants.CM_ATTR_RECEIVED_DATE, CEPATSUtil.todayFormattedForCm());
		if (userDto.getLoggedinUserType() == UserTypeEnum.CCA || userDto.getLoggedinUserType() == UserTypeEnum.TARION) {
			dto.getItemAttributes().put(CEPATSConstants.CM_ATTR_SCANNER_ID, CEPATSConstants.CM_CEPATS_SCANNER_ID_PREFIX_HOP + userDto.getUsername());
		} else {
			dto.getItemAttributes().put(CEPATSConstants.CM_ATTR_SCANNER_ID, CEPATSConstants.CM_CEPATS_SCANNER_ID_PREFIX_BSA + userDto.getUsername());
		}
		dto.getItemAttributes().put(CEPATSConstants.CM_ATTR_TEMPLATE_TYPE, CEPATSConstants.CM_CEPATS_TEMPLATE_TYPE);
		dto.getItemAttributes().put(CEPATSConstants.CM_ATTR_STORAGE_ID, "");
		dto.getItemAttributes().put(CEPATSConstants.CM_ATTR_DOC_NAME, filename);
		dto.setDocData(data);
		dto.setItemType(CEPATSConstants.CM_CEPATS_ITEM_TYPE);
		
		dto.setMimeFileType(mimeType);
		
		String cedefCaseId = crmService.getCedefCaseId(formItem.getEnrolmentNumber());
		dto.getItemAttributes().put(CEPATSConstants.CM_ATTR_CASE_ID, cedefCaseId);


		String trackingNumber = String.valueOf(System.currentTimeMillis());
		List<AttachmentDto> dtoList = new ArrayList<AttachmentDto>();
		dtoList.add(dto);

		
		
		FormItemAttachmentEntity emptyEntity = insertEmptyFormItemAttachmentTable();
		String cmFileName = CEPATSConstants.CM_ATTR_CM_FILE_NAME_PREFIX + emptyEntity.getId();
		dto.getItemAttributes().put(CEPATSConstants.CM_ATTR_CM_FILE_NAME, cmFileName);

		
		Attachments attachments = new AttachmentBuilder(trackingNumber, dtoList).buildAndWrapGenericAttachment();

		SubmissionResponse submissionResponse = null;
		UpdateResultDto result = new UpdateResultDto();
		try {
			String userName = userDto.getUsername();
			// NOTE Due to issues with calling web service and local database
			// update in the same EJB method, I have split the call into two
			// steps:
			// (1) Upload files to CM
			// this is warranty service document upload - this one should be
			// WITH
			// CRM Worklist creation
			submissionResponse = uploadFilesToCmWithoutCrmWorklistCreation(trackingNumber, attachments, userName);
			// (2) update local BSA database for historical purposes
			updateFormItemAttachmentTable(emptyEntity, trackingNumber, filename, attachments, submissionResponse, userName, userDto.getLoggedinUserType(), formItem, false);
			result.setError(false);
			result.setMessage("Attachment uploaded successfully");
		} catch (Exception e) {
			LoggerUtil.logError(AttachmentUploadService.class, e.getMessage());
			result.setError(true);
			result.setMessage("Unable to upload attachment, try again later");
		}
		
		return result;

	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private SubmissionResponse uploadFilesToCm(String clientTrackingNumber, Attachments attachments, String userName) throws Exception {
		SubmissionResponse ret = uploadFiles(clientTrackingNumber, attachments, userName, true);
		return ret;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private SubmissionResponse uploadFilesToCmWithoutCrmWorklistCreation(String clientTrackingNumber, Attachments attachments, String userName) throws Exception {
		SubmissionResponse ret = uploadFiles(clientTrackingNumber, attachments, userName, false);
		return ret;
	}

	private SubmissionResponse uploadFiles(String clientTrackingNumber, Attachments attachments, String userName, boolean createCrmWorklist) throws Exception {
		SubmissionResponse ret = null;
		try {
			AttachmentsService service = wsClientRetrieval.getAttachmentsService();
			if (createCrmWorklist) {
				ret = service.uploadWithUsername(CEPATS_TB_UPLOAD_SOURCE_ID, clientTrackingNumber, attachments, userName);
			} else {
				ret = service.uploadWithUsernameWithoutCrmWorklistCreation(CEPATS_TB_UPLOAD_SOURCE_ID, clientTrackingNumber, attachments, userName);
			}
		} catch (Throwable th) {
			throw new Exception("Upload to CM failed for tracking num  " + clientTrackingNumber + ". " + th.getMessage(), th);
		}
		return ret;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ItemTypes getCMItemTypes() {
		AttachmentsService service = wsClientRetrieval.getAttachmentsService();
		ItemTypes ret = service.getAllItemTypes();
		return ret;
	}
	
	private String truncateFileName(String fileName){
		int maxLength = 100;
		if(fileName.length() <= maxLength){
			return fileName;
		}
		
		int indextOfDot = fileName.lastIndexOf(".");
		if(indextOfDot == -1){
			return fileName.substring(0, maxLength);
		}
		String extention = fileName.substring(indextOfDot);
		String truncated = fileName.substring(0, maxLength - extention.length());
		return truncated.trim() + extention;			
	}

	// @Override
	// @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	// public void addApplicationIDtoTC(String vbNumber, String applicationId)
	// throws BsaException {
	// AttachmentsService service = getAttachmentsService();
	// CmAttributes searchAttr = new CmAttributes();
	//
	// CmAttribute cmAttribute = new CmAttribute();
	// cmAttribute.setName("VBNumber");
	// cmAttribute.setValue(vbNumber);
	// searchAttr.getCmAttribute().add(cmAttribute);
	//
	// CmAttributes updateAttr = new CmAttributes();
	// CmAttribute updateCmAttribute = new CmAttribute();
	// updateCmAttribute.setName("ApplicationID");
	// updateCmAttribute.setValue(applicationId);
	// updateAttr.getCmAttribute().add(updateCmAttribute);
	//
	// List<String> docName = new ArrayList<String>();
	// docName.add("LUInitialRenewalDocuments");
	// UpdateResponse response = service.updateAttributesByQuery(2L, docName,
	// searchAttr, updateAttr);
	// // if(response.getUpdateReferenceInfoList() == null ||
	// response.getUpdateReferenceInfoList().getUpdateReferenceInfo() == null ||
	// response.getUpdateReferenceInfoList().getUpdateReferenceInfo().size() >
	// 0){
	// // throw new BsaException("Couldn't update CM: vbNumber=" + vbNumber +
	// "  / applicationId="+ applicationId);
	// // }
	// }

	// @Override
	// @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	// public void addAcceptedToCMDocument(String vbNumber, String
	// applicationId, boolean accepted) throws BsaException {
	// AttachmentsService service = getAttachmentsService();
	// CmAttributes searchAttr = new CmAttributes();
	//
	// CmAttribute cmAttribute = new CmAttribute();
	// cmAttribute.setName("VBNumber");
	// cmAttribute.setValue(vbNumber);
	// searchAttr.getCmAttribute().add(cmAttribute);
	//
	// CmAttribute cmAttribute2 = new CmAttribute();
	// cmAttribute2.setName("ApplicationID");
	// cmAttribute2.setValue(applicationId);
	// searchAttr.getCmAttribute().add(cmAttribute2);
	//
	// CmAttributes updateAttr = new CmAttributes();
	// CmAttribute updateCmAttribute = new CmAttribute();
	// updateCmAttribute.setName("Accepted");
	// updateCmAttribute.setValue(accepted?"Y":"N");
	// updateAttr.getCmAttribute().add(updateCmAttribute);
	//
	// CmAttribute updateCmAttributeDate = new CmAttribute();
	// updateCmAttributeDate.setName("ReceivedDate");
	// SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	// updateCmAttributeDate.setValue(df.format(new Date()));
	// updateAttr.getCmAttribute().add(updateCmAttributeDate);
	//
	// List<String> docName = new ArrayList<String>();
	// docName.add("LUInitialRenewalDocuments");
	// UpdateResponse response = service.updateAttributesByQuery(2L, docName,
	// searchAttr, updateAttr);
	// // if(response.getUpdateReferenceInfoList() == null ||
	// response.getUpdateReferenceInfoList().getUpdateReferenceInfo() == null ||
	// response.getUpdateReferenceInfoList().getUpdateReferenceInfo().size() >
	// 0){
	// // throw new BsaException("Couldn't update CM: vbNumber=" + vbNumber +
	// "  / applicationId="+ applicationId + " / accepted: " + accepted);
	// // }
	// }
	//
	// public String getAcceptedValue(String vbNumber, String applicationId){
	// // AttachmentsService service = getAttachmentsService();
	// // service.
	// return null;
	// }

	// @Override
	// @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	// public void saveAudit(String submittedBy, String auditMessage,
	// String vbNumber) {
	// Integer groupId =
	// userProfileBean.getUserProfile(submittedBy).getGroup().getGroupId();
	// String groupName =
	// userProfileBean.getUserProfile(submittedBy).getGroup().getGroupName();
	// auditLogBean.saveAudit(auditMessage, null,
	// TransactionType.ATTACHMENTS_UPLOAD, ObjectType.CM_DOC_LOG, submittedBy,
	// groupId, groupName, vbNumber);
	// }
	//
	// @Override
	// public BsaUploadXref findBsaUploadXrefByTrackingNum(String trackingNum) {
	// Query query =
	// bsaEm.createQuery("select e from BsaUploadXref e  where e.referenceNumber = :trackingNum");
	// query.setParameter("trackingNum", trackingNum);
	//
	// return (BsaUploadXref) query.getSingleResult();
	// }

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void markResubmitted(Long formItemAttachmentId) {
		FormItemAttachmentEntity formItemAttachmentEntity = formItemAttachmentRepository.findFormItemAttachmentForId(formItemAttachmentId);
		formItemAttachmentEntity.setAttachmentUploadStatus(AttachmentUploadStatus.RESUBMITTED);
		formItemAttachmentRepository.save(formItemAttachmentEntity);
	}

}