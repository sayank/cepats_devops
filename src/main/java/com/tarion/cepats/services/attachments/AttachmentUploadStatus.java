package com.tarion.cepats.services.attachments;

public enum AttachmentUploadStatus {
	
	SUCCESS, FAILURE, UNKNOWN, RESUBMITTED;
	
	/**
	 * Returns UploadValue enum from given String.
	 * Returns UNKNOWN if string is null or not recognized.
	 * Use this method instad of valueOf when converting values of status received from TBI system. 
	 * 
	 * @param status
	 * @return
	 */
	public static AttachmentUploadStatus fromString(String status) {
		try{
			return AttachmentUploadStatus.valueOf(status);
		}
		catch (Exception e){
			return UNKNOWN;
		}
	}
	
}
