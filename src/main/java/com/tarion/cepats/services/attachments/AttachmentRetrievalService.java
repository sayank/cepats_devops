package com.tarion.cepats.services.attachments;

import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.domains.cepats.FormItemAttachmentRepository;
import com.tarion.cepats.domains.cepats.entities.FormItemAttachmentEntity;
import com.tarion.cepats.services.FormService;
import com.tarion.cepats.services.WebServicesClientRetrieval;
import com.tarion.cepats.services.exception.CepatsServiceException;
import com.tarion.cepats.web.dtos.FormItemAttachmentDto;
import com.tarion.tbi.bsa.cm.DocumentRetrievalWSBean;
import com.tarion.tbi.bsa.cm.DocumentWSDto;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.LinkedList;
import java.util.List;

@Service
public class AttachmentRetrievalService {

	@Autowired
	private WebServicesClientRetrieval wsClientRetrieval;
	@Autowired
	private FormItemAttachmentRepository formItemAttachmentRepository;
	
	/** {@Inheritdoc} */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public DocumentWSDto getDocumentById(String itemId) throws CepatsServiceException {
		DocumentWSDto ret = null;
		try {
			DocumentRetrievalWSBean service = wsClientRetrieval.getDocumentRetrievalService();
			ret = service.getDocumentById(itemId);
		} catch (Throwable th) {
			throw new CepatsServiceException("Retrieval from CM failed for Item Id: " + itemId + ". "
					+ th.getMessage(), th);
		}
		return ret;
	}
	
	public FormItemAttachmentEntity getAttachmentEntityByItemId(String itemId) {
		FormItemAttachmentEntity result = formItemAttachmentRepository.findFormItemAttachmentForItemId(itemId);
		return result;
	}
	
	public List<FormItemAttachmentDto> getAttachmentsByFormItemId(Long formItemId) {
		
		Long startTime = LoggerUtil.logEnter(AttachmentRetrievalService.class, "info", "getAttachmentsByFormItemId",
                formItemId);
		
		List<FormItemAttachmentEntity> entities = formItemAttachmentRepository.findFormItemAttachmentForFormItemId(formItemId);
		
		List<FormItemAttachmentDto> dtos = new LinkedList<FormItemAttachmentDto>();
		try {
			for (FormItemAttachmentEntity entity : entities) {
				FormItemAttachmentDto dto = new FormItemAttachmentDto();
				BeanUtils.copyProperties(dto, entity);
				dtos.add(dto);
			} 
		}
		catch (Exception e) {
			LoggerUtil.logError(FormService.class, e.getMessage(), e);
		}
		
		LoggerUtil.logExit(AttachmentRetrievalService.class, "info", "getAttachmentsByFormItemId", startTime);
		return dtos;
	}
}
