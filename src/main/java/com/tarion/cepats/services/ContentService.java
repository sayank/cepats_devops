package com.tarion.cepats.services;

import java.util.List;

import com.tarion.cepats.web.dtos.ContentDto;

public interface ContentService {
	
	 public List<ContentDto> findByContentType(String contentType);
	

}
