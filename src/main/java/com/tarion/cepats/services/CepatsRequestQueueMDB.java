/* $Id$
 *
 * Copyright (c) 2014 Tarion Warranty Corporation.
 * All rights reserved. 
 */
package com.tarion.cepats.services;

import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.interceptor.Interceptors;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;

import com.tarion.cepats.common.CEPATSConstants;
import com.tarion.cepats.common.CEPATSUtil;
import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.services.dtos.CepatsMessageTypeEnum;
import com.tarion.cepats.services.exception.CepatsServiceException;
import com.tarion.cepats.services.jaxbs.CepatsInLineItemsRequest;

/**
 * Listens for JMS message from Cepats Request Queue
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @date Oct 09, 2014
 * @version 1.0
 */
@Resource(name="jms/CrmCeFrnCF", type=javax.jms.ConnectionFactory.class)
@javax.ejb.MessageDriven(mappedName = "jms/CepatsCrmReqQueue")
@Interceptors(SpringBeanAutowiringInterceptor.class)
public class CepatsRequestQueueMDB implements MessageListener {

	@Autowired
	private FormService formService;
	
	@Autowired
	private JmsMessageSender jmsMessageSender;

//	@Override
	public void onMessage(Message inMessage) {
		long startTime = LoggerUtil.logEnter(CepatsRequestQueueMDB.class, "onMessage", inMessage);
		String inCaseMsgXML = null;
		try {
			if (inMessage instanceof TextMessage) {
				// skip any ping messages
				try {
					String messageType = inMessage.getStringProperty(CEPATSConstants.MESSAGE_TYPE);
					if (CEPATSConstants.PING.equalsIgnoreCase(messageType)) {
						String destNodes = inMessage.getStringProperty(CEPATSConstants.DESTINATION_NODES_META);
						LoggerUtil.logTrace(CepatsRequestQueueMDB.class, "Ping received from CRM for Destination Node[" + destNodes + "]");
						return;
					}
				} catch (JMSException e) {
					// nothing to report
				}

				try {
					TextMessage txtMsg = (TextMessage) inMessage;
					inCaseMsgXML = txtMsg.getText();
				} catch (JMSException e) {
					throw new CepatsServiceException(e.getMessage());
				}

				if (inCaseMsgXML == null) {
					throw new CepatsServiceException("Missing Crm Request Object");
				}
				try {
					String messageName = inMessage.getStringProperty(CEPATSConstants.MESSAGE_NAME_META);
					if (CEPATSConstants.MESSAGE_NAME_CRM_CEPATS_REQUEST_IN_LINE_ITEMS.equalsIgnoreCase(messageName)) {
						processReceivedCrmToCepatsInLineItemsRequest(inCaseMsgXML);
					} else {
						// nothing
					}
				} catch (JMSException e) {
					LoggerUtil.logError(CepatsRequestQueueMDB.class, "JMSExceptions", e);
				}
			} else {
				String warnMsg = "Message of wrong type: " + inMessage.getClass().getName();
				LoggerUtil.logTrace(CepatsRequestQueueMDB.class, warnMsg);
			}
		} catch (CepatsServiceException pie) {
			StringBuffer buff = new StringBuffer("CSSCaseResultMDB error for: [");
			buff.append(inCaseMsgXML).append(']');
			LoggerUtil.logError(CepatsRequestQueueMDB.class, buff.toString(), pie);
			throw new EJBException(pie);

		}
		LoggerUtil.logExit(JmsMessageSender.class, "onMessage", startTime);
	}
	
	/**
	 * Processes received CRM's inLineItems requests
	 * @param xmlRequest
	 * @throws CepatsServiceException
	 */
	private void processReceivedCrmToCepatsInLineItemsRequest(String xmlRequest) throws CepatsServiceException {
		CepatsInLineItemsRequest request = CEPATSUtil.convertXmlToJaxb(xmlRequest, CepatsInLineItemsRequest.class);
		String xmlResponse = formService.getFormItems(request.getEnrolmentNumber());
		jmsMessageSender.postToCepatsSubmitRequestQueue(xmlResponse, CepatsMessageTypeEnum.CEPATS_MESSAGE_IN_LINE_ITEMS_RESPONSE);
	}
}
