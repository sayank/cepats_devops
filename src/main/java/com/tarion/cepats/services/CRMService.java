package com.tarion.cepats.services;

import java.sql.Timestamp;
import java.util.List;

import com.tarion.cepats.domains.crm.entities.AgreementLineEntity;
import com.tarion.cepats.domains.crm.entities.EnrolmentEntity;
import com.tarion.cepats.domains.crm.entities.PersonEntity;
import com.tarion.cepats.domains.crm.entities.WSCAssignedEntity;
import com.tarion.cepats.domains.crm.entities.WSRAssignedEntity;

public interface CRMService {
	
    public EnrolmentEntity getEnrolment(String enrolmentNumber);
	public AgreementLineEntity getAgreementLineEndDate(String enrolmentNumber);
	public Timestamp getAgreementLineEndDateFirstYear(String enrolmentNumber);
	public Timestamp getAgreementLineEndDateSecondYear(String enrolmentNumber);
	List<PersonEntity> getTarionUsers(String enrolmentNumber);
	List<PersonEntity> getCCAUsers(String enrolmentNumber);
	
	public String getCedefCaseId(String enrolmentNumber);
	public WSCAssignedEntity getWSCAssignedToCEPATS(String enrolmentNumber);
	public WSRAssignedEntity getWSRAssignedToCEPATS(String enrolmentNumber);

}
