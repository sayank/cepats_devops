package com.tarion.cepats.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tarion.cepats.common.CEPATSConstants;
import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.services.exception.CepatsServiceException;

 /**
 * 
 * @author oagady
 * @date Sep 27, 2014
 * @version 1.0
 *
 */
@Service
public class MailSenderImpl implements MailSender{
	
	@Autowired
	PropertyService propertyService;

    @Autowired
    WebServicesClientRetrieval webServicesClientRetrieval;
	

    /**
     * Send an email message with
     *
     * @param toAddresses List of semicolon separated e-mail addresses
     */
	@Override
    public void sendEmailUsingEsp(String subject, String body, String fromAddress, String toAddresses) throws CepatsServiceException {
		long start = LoggerUtil.logEnter(MailSenderImpl.class, "sendEmailUsingEsp {} {} {} {} ", subject, body, fromAddress, toAddresses);
        if(fromAddress == null) {
        	fromAddress = propertyService.getValue(CEPATSConstants.EMAIL_FROM_ADDRESS);
        } 
        webServicesClientRetrieval.sendEmailUsingEsp(subject, body, fromAddress, toAddresses);
        LoggerUtil.logExit(MailSenderImpl.class, "sendEmailUsingEsp", start);
    }
    
}
