/* 
 * 
 * EnrolmentsBean.java
 *
 * Copyright (c) 2012 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.cepats.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tarion.tbi.services.CEPATSService;
import com.tarion.tbi.services.TimelinesResponse;


@Service
public class TimelinesService {


	
	@Autowired
	private WebServicesClientRetrieval wsClientRetrieval;


	public TimelinesResponse getWarrantyTimelines(String enrolmentId) throws Exception {
		TimelinesResponse ret = null;
		try {
			CEPATSService service = wsClientRetrieval.getCEPATSService();
			ret = service.getWarrantyTimelines(enrolmentId);
		} catch (Throwable th) {
			throw new Exception("Failed to get warranty timelines for enrolment  " + enrolmentId);
		}
		return ret;
	}


}