package com.tarion.cepats.services;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.tarion.bsa.cepats.UserProfileDto;
import com.tarion.cepats.common.CEPATSUtil;
import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.common.TextUtil;
import com.tarion.cepats.domains.cepats.FormItemHistoryRepository;
import com.tarion.cepats.domains.cepats.FormItemRepository;
import com.tarion.cepats.domains.cepats.InitialUploadRepository;
import com.tarion.cepats.domains.crm.PersonRepository;
import com.tarion.cepats.domains.crm.entities.PersonEntity;
import com.tarion.cepats.domains.cepats.entities.FormItemEntity;
import com.tarion.cepats.domains.cepats.entities.FormItemHistoryEntity;
import com.tarion.cepats.domains.cepats.entities.FormTypeEnum;
import com.tarion.cepats.domains.cepats.entities.UserTypeEnum;
import com.tarion.cepats.web.dtos.ItemHistoryDto;
import com.tarion.cepats.web.dtos.UserDto;

 /**
 * 
 * @author oagady
 * @date Nov 7, 2014
 * @version 1.0
 *
 */
@Service(value = "emailNotificationService")
public class EmailNotificationServiceImpl implements EmailNotificationService{

	private static final String CONDO_CORP_REPRESENTATIVE = "Condo Corp Representative";

	private static final String COMMON_ELEMENTS_TEAM = "Common Elements Team";

	@Autowired
	PersonRepository personRepository;

	@Autowired
	FormItemHistoryRepository formItemHistoryRepository;
	
	@Autowired
	FormItemRepository formItemRepository;
	
	@Autowired
	InitialUploadRepository initialUploadRepository;

	@Autowired
	MailService mailService;

	@Autowired
	CRMService crmService;

	@Autowired
	VersionHistoryService historyService;
	
	@Autowired
	FormService formService;

	@Autowired
	PropertyService propertyService;
	
	@Autowired
	WebServicesClientRetrieval builderLinkWebServiceClient;
	

	/**
	 * @param enrolmentNumber
	 * @param formType
	 * @throws Exception
	 */
	public void sendInitialUploads(String enrolmentNumber, FormTypeEnum formType) throws Exception {
		
		sendInitialUploadToTarionUsers(formType, enrolmentNumber);
		sendInitialUploadToCCAUsers(formType, enrolmentNumber);
		sendInitialUploadToVBUsers(formType, enrolmentNumber);
		
	}
	


	/**
	 * @param enrolmentNumber
	 * @param formType
	 * @param uploadSummary
	 * @param emailAddress
	 * @param user
	 */
	public void sendUploadError(String enrolmentNumber, FormTypeEnum formType, String uploadSummary, String emailAddress, UserDto user) {
		try {
			mailService.sendUploadError(enrolmentNumber, formType, uploadSummary, emailAddress, user);
		} catch (Exception e) {
			LoggerUtil.logError(EmailNotificationServiceImpl.class, e.getMessage(), e);
		}
	}

	/**
	 * @param formType
	 * @param enrolmentNumber
	 */
	private void sendInitialUploadToTarionUsers(FormTypeEnum formType, String enrolmentNumber) {
		long startTime = LoggerUtil.logEnter(EmailNotificationServiceImpl.class, "ENTER - sendInitialUploadToTarionUsers({}, {})",
				formType, enrolmentNumber);
		
		List<PersonEntity> tarionUsers = null;
		try {
			tarionUsers = crmService.getTarionUsers(enrolmentNumber);
		} catch (Exception e) {
			LoggerUtil.logError(EmailNotificationServiceImpl.class, e.getMessage(), e);
		}
		// if there are no tarion users assigned to this enrolment, send email
		// to Common Elements Team
		if (tarionUsers == null || tarionUsers.size() == 0) {
			sendInitialUploadMail(enrolmentNumber, formType, propertyService.getDefaultEmail(),
					COMMON_ELEMENTS_TEAM, false);
		} else {
			LoggerUtil.logInfo(EmailNotificationServiceImpl.class, "Number of Tarion users: {}", tarionUsers.size());
			for (PersonEntity user : tarionUsers) {
				LoggerUtil.logInfo(EmailNotificationServiceImpl.class, "Sending initial upload email to Tarion user: {}", user.getEmailAddress());
				sendInitialUploadMail(enrolmentNumber, formType, user.getEmailAddress(), 
						CEPATSUtil.getFirstNameLastName(user.getFirstName(), user.getLastName()), false);
			}
		}
		LoggerUtil.logExit(EmailNotificationServiceImpl.class, "sendInitialUploadToTarionUsers", startTime);  
	}
	
	/**
	 * @param formType
	 * @param enrolmentNumber
	 */
	private void sendInitialUploadToCCAUsers(FormTypeEnum formType, String enrolmentNumber) throws Exception{
		long startTime = LoggerUtil.logEnter(EmailNotificationServiceImpl.class, "ENTER - sendInitialUploadToCCAUsers({}, {})",
				formType, enrolmentNumber);
		
		List<PersonEntity> ccaUsers = null;
		try {
			ccaUsers = crmService.getCCAUsers(enrolmentNumber);
		} catch (Exception e1) {
			LoggerUtil.logError(EmailNotificationServiceImpl.class, e1.getMessage(), e1);
			return;
		}		
		LoggerUtil.logInfo(EmailNotificationServiceImpl.class, "Number of CCA users: {}", ccaUsers.size());
		for (PersonEntity user : ccaUsers) {
			LoggerUtil.logInfo(EmailNotificationServiceImpl.class, "Sending initial upload email to CCA user: {}", user.getEmailAddress());
			sendInitialUploadMail(enrolmentNumber, formType, user.getEmailAddress(), CONDO_CORP_REPRESENTATIVE, false);
		}
		
		LoggerUtil.logExit(EmailNotificationServiceImpl.class, "sendInitialUploadToCCAUsers", startTime);  
	}
	
	/**
	 * @param formType
	 * @param enrolmentNumber
	 */
	private void sendInitialUploadToVBUsers(FormTypeEnum formType, String enrolmentNumber) throws Exception{
		long startTime = LoggerUtil.logEnter(EmailNotificationServiceImpl.class, "ENTER - sendInitialUploadToVBUsers({}, {})",
				formType, enrolmentNumber);
		
		List<UserProfileDto> vbUsers = null;
		try {
			vbUsers = builderLinkWebServiceClient.getCepatsService().getListOfWarrantyServicesUsersForVbForEnrolmentNumber(enrolmentNumber);
		} catch (Exception e) {
			LoggerUtil.logError(EmailNotificationServiceImpl.class, e.getMessage(), e);
			return;
		}
		LoggerUtil.logInfo(EmailNotificationServiceImpl.class, "Number of VB users: {}", vbUsers.size());
		for (UserProfileDto user : vbUsers) {
			LoggerUtil.logInfo(EmailNotificationServiceImpl.class, "Sending initial upload email to VB user: {}", user.getEmail());
			sendInitialUploadMail(enrolmentNumber, formType, user.getEmail(), user.getUserDesc(), true);
		}
		LoggerUtil.logExit(EmailNotificationServiceImpl.class, "sendInitialUploadToVBUsers", startTime);  
	}
	

	/**
	 * @param enrolmentNumber
	 * @param formType
	 * @param formItems
	 */
	/**
	 * @param formType
	 * @param timePeriod
	 */
	public void sendUpdates(FormTypeEnum formType, Timestamp timePeriod) {
		LoggerUtil.logEnter(EmailNotificationServiceImpl.class, "ENTER - sendUpdates({}, {})",
				formType, timePeriod);
		List<String> enrolmentNumbers = formItemRepository.findEnrolmentNumbersByDateAndFormType(timePeriod, formType);
		LoggerUtil.logInfo(EmailNotificationServiceImpl.class, "Found enrolments with updates: {}", enrolmentNumbers);

		for (String enrolmentNumber : enrolmentNumbers) {
			
			String history = null;
			try {
				history = buildFormUpdateHistory(enrolmentNumber, formType, timePeriod);
				if(history == null){
					LoggerUtil.logInfo(EmailNotificationServiceImpl.class, "History of  {} is null", enrolmentNumber);
					continue;
				}
			} catch (Exception e) {
				LoggerUtil.logError(EmailNotificationServiceImpl.class, e.getMessage(), e);
				continue;
			}
			
			sendUpdatesToTarionUsers(formType, enrolmentNumber, history);
			sendUpdatesToCCAUsers(formType, enrolmentNumber, history);
			sendUpdatesToVBUsers(formType, enrolmentNumber, history);

		}
	}
	

	/**
	 * @param formType
	 * @param enrolmentNumber
	 * @param history
	 */
	private void sendUpdatesToTarionUsers(FormTypeEnum formType, String enrolmentNumber, String history) {
		long startTime = LoggerUtil.logEnter(EmailNotificationServiceImpl.class, "ENTER - sendUpdatesToTarionUsers({}, {})",
				formType, enrolmentNumber);
		
		List<PersonEntity> tarionUsers = null;
		try {
			tarionUsers = crmService.getTarionUsers(enrolmentNumber);
		} catch (Exception e) {
			LoggerUtil.logError(EmailNotificationServiceImpl.class, e.getMessage(), e);
		}
		LoggerUtil.logInfo(EmailNotificationServiceImpl.class, "Number of tarion users: {}", tarionUsers.size());
		// if there are no tarion users assigned to this enrolment, send email
		// to Common Elements Team
		if (tarionUsers == null || tarionUsers.size() == 0) {
			sendUpdateMail(enrolmentNumber, formType, history, propertyService.getDefaultEmail(),
					COMMON_ELEMENTS_TEAM);
		} else {
			for (PersonEntity user : tarionUsers) {
				LoggerUtil.logInfo(EmailNotificationServiceImpl.class, "Sending update notification to Tarion user: {}", user.getEmailAddress());
				sendUpdateMail(enrolmentNumber, formType, history, user.getEmailAddress(),
						CEPATSUtil.getFirstNameLastName(user.getFirstName(), user.getLastName()));
			}
		}
		LoggerUtil.logExit(EmailNotificationServiceImpl.class, "sendUpdatesToTarionUsers", startTime);  
	}

	/**
	 * @param formType
	 * @param enrolmentNumber
	 * @param history
	 */
	private void sendUpdatesToCCAUsers(FormTypeEnum formType, String enrolmentNumber, String history) {
		long startTime = LoggerUtil.logEnter(EmailNotificationServiceImpl.class, "ENTER - sendUpdatesToCCAUsers({}, {})",
				formType, enrolmentNumber);
		
		List<PersonEntity> ccaUsers = null;
		try {
			ccaUsers = crmService.getCCAUsers(enrolmentNumber);
		} catch (Exception e) {
			LoggerUtil.logError(EmailNotificationServiceImpl.class, e.getMessage(), e);
			return;
		}
		LoggerUtil.logInfo(EmailNotificationServiceImpl.class, "Number of CCA users: {}", ccaUsers.size());
		
		for (PersonEntity user : ccaUsers) {
			LoggerUtil.logInfo(EmailNotificationServiceImpl.class, "Sending update notification to CCA user: {}", user.getEmailAddress());
			//Info for CCA users comes from CRM without first name and last name, because CCA users info is stored in CRM without Business Object ID,
			//and joining tables is not possible.
			//To display greeting in email message, use default greeting for CCA users
			sendUpdateMail(enrolmentNumber, formType, history, user.getEmailAddress(), CONDO_CORP_REPRESENTATIVE);
		}
		LoggerUtil.logExit(EmailNotificationServiceImpl.class, "sendUpdatesToCCAUsers", startTime);  
	}
	
	/**
	 * @param formType
	 * @param enrolmentNumber
	 * @param history
	 */
	private void sendUpdatesToVBUsers(FormTypeEnum formType, String enrolmentNumber, String history) {
		long startTime = LoggerUtil.logEnter(EmailNotificationServiceImpl.class, "ENTER - sendUpdatesToVBUsers({}, {})",
				formType, enrolmentNumber);
		
		List<String> emailAddressesForUsersWhoUpdatedForm = getEmailAddressesForUsersWhoUpdatedTheForm(enrolmentNumber, formType, UserTypeEnum.VB);
		LoggerUtil.logInfo(EmailNotificationServiceImpl.class, "Number of VB users: {}", emailAddressesForUsersWhoUpdatedForm.size());
		for(String emailAddress : emailAddressesForUsersWhoUpdatedForm) {
			String userFirstNameLastName = getUserFirstNameLastName(emailAddress, formType, UserTypeEnum.VB);
			LoggerUtil.logInfo(EmailNotificationServiceImpl.class, "Sending update notification to VB user: {}", emailAddress);
			sendUpdateMail(enrolmentNumber, formType, history,  emailAddress, userFirstNameLastName);
		}
		LoggerUtil.logExit(EmailNotificationServiceImpl.class, "sendUpdatesToVBUsers", startTime);  
	}
	
	/**
	 * @param enrolmentNumber
	 * @param formType
	 * @param emailAddress
	 * @param firstName
	 * @param lastName
	 */
	private void sendInitialUploadMail(String enrolmentNumber, FormTypeEnum formType, String emailAddress, String name, boolean isVendorBuilder) {
		if (emailAddress == null) {
			return;
		}
		long startTime = LoggerUtil.logEnter(EmailNotificationServiceImpl.class, "Sending initial upload email to CCA user: {}", emailAddress);

		try {
			if (formType.equals(FormTypeEnum.FIRSTYEAR)) {
				mailService.sendMailInitialUploadFirstYear(emailAddress, name, enrolmentNumber, isVendorBuilder);
			}
			if (formType.equals(FormTypeEnum.SECONDYEAR)) {
				mailService.sendMailInitialUploadSecondYear(emailAddress, name, enrolmentNumber, isVendorBuilder);
			}
		} catch (Exception e) {
			LoggerUtil.logError(EmailNotificationServiceImpl.class, e.getMessage(), e);
		}

		LoggerUtil.logExit(EmailNotificationServiceImpl.class, "sendInitialUploadMail", startTime);  
	}

	/**
	 * @param enrolmentNumber
	 * @param formType
	 * @param history
	 * @param emailAddress
	 * @param firstName
	 * @param lastName
	 */
	private void sendUpdateMail(String enrolmentNumber, FormTypeEnum formType, String history, String emailAddress, String name) {
		if (emailAddress == null) {
			return;
		}
		try {
			if (formType.equals(FormTypeEnum.FIRSTYEAR)) {
				mailService.sendFormUpdateFirstYear(history, emailAddress, name, enrolmentNumber);
			}
			if (formType.equals(FormTypeEnum.SECONDYEAR)) {
				mailService.sendFormUpdateSecondYear(history, emailAddress, name, enrolmentNumber);
			}
		} catch (Exception e) {
			LoggerUtil.logError(EmailNotificationServiceImpl.class, e.getMessage(), e);
		}
	}
	

	
	/**
	 * @param enrolmentNumber
	 * @param formType
	 * @return
	 */
	private List<String> getEmailAddressesForUsersWhoUpdatedTheForm(String enrolmentNumber, FormTypeEnum formType, UserTypeEnum userType) {
		List<String> emailAddressesForUsersWhoUpdatedForm 
				= formItemHistoryRepository.findEmailAddressesforEnrolmentAndFormTypeAndUserType(enrolmentNumber, formType.toString(), userType.toString());
		//Get the email address from the form items table as well. this is because there could be a possibility that a different vb email updates for the first time
		//in that scenario, the new email will be present only in the form item table. that email address should also be weekly notified of the changes.
		List<String> emailAddressesForUsersWhoUpdatedFormItem = formItemRepository.findEmailAddressesforEnrolmentAndFormTypeAndUserType(enrolmentNumber, formType.toString(), userType.toString());
		emailAddressesForUsersWhoUpdatedForm.removeAll(emailAddressesForUsersWhoUpdatedFormItem);
		emailAddressesForUsersWhoUpdatedForm.addAll(emailAddressesForUsersWhoUpdatedFormItem);
		return emailAddressesForUsersWhoUpdatedForm;
	}
	
	/**
	 * @param emailAddress
	 * @param formType
	 * @return
	 */
	private String getUserFirstNameLastName(String emailAddress, FormTypeEnum formType, UserTypeEnum userType) {
		//find only first form item with given email address
		List<FormItemHistoryEntity> formItemsHistory 
			= formItemHistoryRepository.findByUpdatedUserEmailAddressAndFormTypeAndUserType(emailAddress, formType, userType, new PageRequest(0,1));
		//Added the below logic to check the first name and last name of the VB user from form item table if it doesnt exist in the form item history table.
		if(formItemsHistory==null || formItemsHistory.size()==0) {
			List<FormItemEntity> formItems = formItemRepository.findByUpdatedUserEmailAddressAndFormTypeAndUserType(emailAddress, formType, userType, new PageRequest(0,1));
			return CEPATSUtil.getFirstNameLastName(formItems.get(0).getUpdatedUserFirstName(), formItems.get(0).getUpdatedUserLastName());
		}
		return CEPATSUtil.getFirstNameLastName(formItemsHistory.get(0).getUpdatedUserFirstName(), formItemsHistory.get(0).getUpdatedUserLastName());
	}



	/**
	 * @param enrolmentNumber
	 * @param formType
	 * @param timePeriod 
	 * @return
	 * @throws Exception
	 */
	private String buildFormUpdateHistory(String enrolmentNumber, FormTypeEnum formType, Timestamp startTime) throws Exception {
		List<ItemHistoryDto> itemUpdateDtoList 
			= historyService.getVersionHistory(enrolmentNumber, formType, startTime, new Timestamp(System.currentTimeMillis()), true);
		if(itemUpdateDtoList == null){
			return null;
		}
		return TextUtil.buildHistoryHTML(itemUpdateDtoList);
	}


	public void forceNotifications(int numberOfDays) {

		long startTime = LoggerUtil.logEnter(EmailNotificationServiceImpl.class, "forceNotifications");
		Timestamp lastWeek = CEPATSUtil.getPastTimestamp(numberOfDays);

		sendUpdates(FormTypeEnum.FIRSTYEAR, lastWeek);
		sendUpdates(FormTypeEnum.SECONDYEAR, lastWeek);
		
		LoggerUtil.logExit(EmailNotificationServiceImpl.class, "forceNotifications", startTime);  
	}  

}
