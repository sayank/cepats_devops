package com.tarion.cepats.services;

import com.tarion.cepats.domains.cepats.entities.SsoLoginEntity;

public interface SSOService {
	
	public void saveUser(String token, String userId, String firstName, String lastName, 
			String sourceSystem, String enrolmentNumber, String email, String userType);
	
	public SsoLoginEntity loadUserByToken(String token);
	
	public void deleteToken(String token);
	
	public boolean isUserLoggedIn(String token) throws Exception;
	
	public void logUser(SsoLoginEntity user);
	
	public void deleteStaleTokens();
	

}
