package com.tarion.cepats.config;

import org.apache.tiles.Attribute;
import org.apache.tiles.Definition;
import org.apache.tiles.definition.DefinitionsFactory;
import org.apache.tiles.request.Request;

import java.util.HashMap;
import java.util.Map;

public class TilesPageConfiguration implements DefinitionsFactory {

    private static final Map<String, Definition> tilesDefinitions = new HashMap<String,Definition>();
    private static final Attribute BASE_TEMPLATE = new Attribute("/WEB-INF/layouts/default.jsp");

    public static void addDefinitions(){

        addDefaultLayoutDef("patsForm", "/WEB-INF/views/patsForm.jsp");
        addDefaultLayoutDef("login", "/WEB-INF/views/login.jsp");
        addDefaultLayoutDef("loginFailed", "/WEB-INF/views/loginFailed.jsp");
        addDefaultLayoutDef("faq", "/WEB-INF/views/faq.jsp");
        addDefaultLayoutDef("admin/logbackAdmin", "/WEB-INF/views/admin/logbackAdmin.jsp");
    }

    @Override
    public Definition getDefinition(String name, Request tilesContext) {
        return tilesDefinitions.get(name);
    }

    private static void addDefaultLayoutDef(String name, String body) {
        Map<String, Attribute> attributes = new HashMap<String,Attribute>();
        attributes.put("title", new Attribute("CEPATS"));
        attributes.put("body", new Attribute(body));
        tilesDefinitions.put(name, new Definition(name, BASE_TEMPLATE, attributes));
    }
}