package com.tarion.cepats.config.datasource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.LocalEntityManagerFactoryBean;


@Configuration
@EnableJpaRepositories(
        entityManagerFactoryRef = "cepatsEntityManagerFactory",
        basePackages = { "com.tarion.cepats.domains.cepats" }
)
public class CEPATSDatasourceConfiguration {

    @Primary
    @Bean(name = "cepatsEntityManagerFactory")
    public LocalEntityManagerFactoryBean entityManagerFactory(){
        LocalEntityManagerFactoryBean factoryBean = new LocalEntityManagerFactoryBean();
        factoryBean.setPersistenceUnitName("cepats");
        return factoryBean;
    }

}
