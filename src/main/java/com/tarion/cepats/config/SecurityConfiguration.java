package com.tarion.cepats.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.filter.RequestContextFilter;

@Configuration
@EnableWebSecurity
@Order(1)
public class SecurityConfiguration  extends WebSecurityConfigurerAdapter {

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth){
        auth.eraseCredentials(true);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/login/**").permitAll()
            .and().authorizeRequests().antMatchers("/authToken/*").permitAll()
            .and().authorizeRequests().antMatchers("/patsList/*").permitAll()
            .and().authorizeRequests().antMatchers("/faq/*").permitAll()
            .and().authorizeRequests().antMatchers("/css/*").permitAll()
            .and().authorizeRequests().antMatchers("/js/*").permitAll()
            .and().authorizeRequests().antMatchers("/img/*").permitAll()
            .and().authorizeRequests().antMatchers("/favicon.ico").permitAll()
            .and().authorizeRequests().antMatchers("/monitor").permitAll()
            .and().authorizeRequests().antMatchers("/**").authenticated()
            .and().headers().frameOptions().sameOrigin()
            .and().formLogin().loginProcessingUrl("/login").loginPage("/login/")
            .and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/resources/j_spring_security_logout"))
            .and().csrf().disable();

        CsrfHeaderFilter csrfTokenFilter = new CsrfHeaderFilter();
        http.addFilterAfter(csrfTokenFilter, CsrfFilter.class);
        http.addFilterAfter(new RequestContextFilter(), CsrfHeaderFilter.class);
    }
}
