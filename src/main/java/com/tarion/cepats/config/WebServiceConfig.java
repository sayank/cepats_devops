package com.tarion.cepats.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {

    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(servlet, "/authToken/*", "patsList/*");
    }

    @Bean(name = "authToken")
    public DefaultWsdl11Definition authServiceDefinition(XsdSchema authTokenSchema) {
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("authTokenPort");
        definition.setTargetNamespace("http://cepats/auth.token.webservice");
        definition.setLocationUri("/authToken/");
        definition.setSchema(authTokenSchema);
        return definition;
    }
    @Bean
    public XsdSchema authTokenSchema() {
        return new SimpleXsdSchema(new ClassPathResource("AuthTokenService.xsd"));
    }

    @Bean(name = "patsList")
    public DefaultWsdl11Definition cepatsServiceDefinition(XsdSchema cepatsSchema) {
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("patsListPort");
        definition.setTargetNamespace("http://cepats/cepats.webservice");
        definition.setLocationUri("/patsList/");
        definition.setSchema(cepatsSchema);
        return definition;
    }
    @Bean
    public XsdSchema cepatsSchema() {
        return new SimpleXsdSchema(new ClassPathResource("CEPATSService.xsd"));
    }

}