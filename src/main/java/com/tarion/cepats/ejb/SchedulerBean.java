/*
 * SchedulerBeanRemote.java
 *
 * Copyright (c) 2012 Tarion Warranty Corporation.
 * All rights reserved.
 *
 */
package com.tarion.cepats.ejb;

import com.tarion.cepats.common.CEPATSUtil;
import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.domains.cepats.entities.FormItemAttachmentEntity;
import com.tarion.cepats.domains.cepats.entities.FormTypeEnum;
import com.tarion.cepats.services.EmailNotificationService;
import com.tarion.cepats.services.SSOService;
import com.tarion.cepats.services.attachments.AttachmentBuilder;
import com.tarion.cepats.services.attachments.AttachmentUploadService;
import com.tarion.tbi.cm.attachments.Attachments;
import com.tarion.tbi.cm.attachments.ReferenceInfo;
import com.tarion.tbi.cm.attachments.SubmissionResponse;
import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;
import org.springframework.stereotype.Component;

import javax.ejb.*;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * The methods of this class are periodically invoked by EJB timer service. It
 * is done indireclty from the SchedulerBeanWrapper, so that we avoid coupling
 * EJB and Spring injection mechanism in the same class.
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since July 10, 2013
 * @version 1.0
 */
@Stateless(mappedName="ejb/SchedulerBean")
@LocalBean
@Interceptors(SpringBeanAutowiringInterceptor.class)
@Component
public class SchedulerBean {

	@Inject
	AttachmentUploadService attachmentUploadService;

	@Inject
	EmailNotificationService emailNotificationService;

	@Inject
	SSOService ssoService;

	/* (non-Javadoc)
	 * @see com.tarion.cepats.ejb.SchedulerBeanRemote#resendUploadAttachmentsToCm()
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Schedule(hour = "0-23", minute = "*/15")
	public void resendUploadAttachmentsToCm() {
		long startTime = LoggerUtil.logEnter(SchedulerBean.class, "resendUploadAttachmentsToCm");
		List<FormItemAttachmentEntity> failedGenericUploads = attachmentUploadService.findFailedUploads();
		for (FormItemAttachmentEntity xref : failedGenericUploads) {
			try {
				ressubmitOneRecord(xref);
			} catch (Exception e) {
				LoggerUtil.logError(SchedulerBean.class, "Problem while resubmitting record: " + xref, e);
			}
		}
		LoggerUtil.logExit(SchedulerBean.class, "resendUploadAttachmentsToCm", startTime);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Schedule(dayOfWeek="*", hour="0")
	public void deleteStaleTokens(){

		long startTime = LoggerUtil.logEnter(SchedulerBean.class, "deleteStaleTokens Executing 'delete stale tokens' task");
		ssoService.deleteStaleTokens();
		LoggerUtil.logExit(SchedulerBean.class, "deleteStaleTokens", startTime);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Schedule(dayOfWeek="Mon", hour="1")
	public void notifyFormUpdatesWeekly(){
		long startTime = LoggerUtil.logEnter(SchedulerBean.class, "notifyFormUpdatesWeekly");
		Timestamp lastWeek = CEPATSUtil.getPastTimestamp(7);

		emailNotificationService.sendUpdates(FormTypeEnum.FIRSTYEAR, lastWeek);
		emailNotificationService.sendUpdates(FormTypeEnum.SECONDYEAR, lastWeek);

		LoggerUtil.logExit(SchedulerBean.class, "notifyFormUpdatesWeekly", startTime);
	}

	private void ressubmitOneRecord(FormItemAttachmentEntity xref) throws Exception {
		Long recordId = xref.getId();
		String trackingNumber = xref.getReferenceNumber();
		SubmissionResponse resp = attachmentUploadService.resubmitFailedUploads(recordId, "SCHEDULER");
		String xml = xref.getAttachmentXml();
		Attachments att = AttachmentBuilder.fromXmlString(xml);

		List<Integer> succeededIndexes = findIndexesOfSucceededUploads(resp);

		if (succeededIndexes.size() > 0) {

			if (succeededIndexes.size() < att.getAttachment().size()){
				// DO NOT remove last attahcment - it is needed below, to fill in
				// enrolment number, vb number etc in the uploadAttachmentsBean.updateBsaUploadXrefTable(...) call
				removeSucceededAttachments(att, succeededIndexes);
			}
			// Insert record into BsaUploadXref ...
			attachmentUploadService.insertFormItemAttachmentTable(trackingNumber, xref.getOriginalFileName(), att, resp, xref.getUploadedUser(), xref.getUploadedUserType(), xref.getFormItem(), xref.isSubmitCrmWorklist());
			// ... and update original record to RESUBMITTED status
			attachmentUploadService.markResubmitted(recordId);
		} else {
			// Nothing to update since nothing succeeded
		}
	}

	/**
	 * Loop through response for each individual file and adds to list if successfull
	 */
	private List<Integer> findIndexesOfSucceededUploads(SubmissionResponse resp) {
		List<Integer> ret = new ArrayList<Integer>();
		for (int i = 0; i < resp.getReferenceInfoList().getReferenceInfo().size(); i++){
			ReferenceInfo ri = resp.getReferenceInfoList().getReferenceInfo().get(i);
			if (ri.getStatus().equalsIgnoreCase("SUCCESS")) {
				ret.add(i);
			}
		}
		return ret;
	}

	private void removeSucceededAttachments(Attachments atts, List<Integer> succeededIndexes) {

		Collections.sort(succeededIndexes, Collections.reverseOrder());

		for (int i: succeededIndexes){
			atts.getAttachment().remove(i);
		}
	}

}
