package com.tarion.cepats;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.tarion.cepats.services",
        "com.tarion.cepats.config.datasource",
        "com.tarion.common",
        "com.tarion.cepats.domains",
        "com.tarion.cepats.config.beans",
        "com.tarion.cepats.builders"})
public class CepatsEjbApplication {



}
