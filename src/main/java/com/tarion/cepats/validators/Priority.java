package com.tarion.cepats.validators;

import com.tarion.cepats.validators.impl.PriorityValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Aug 28, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 *
 * Declaration of @Priority custom annotation
 *
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
@Constraint(validatedBy = PriorityValidator.class)
public @interface Priority {

    String message() default "Please use only values provided in the Priority dropdown list";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
