package com.tarion.cepats.validators;

import com.tarion.cepats.domains.cepats.entities.FormItemEntity;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Aug 28, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 */
public interface FormItemValidator {

    public boolean validate(FormItemEntity oldFormItem, FormItemEntity newFormItem) throws Exception;
}
