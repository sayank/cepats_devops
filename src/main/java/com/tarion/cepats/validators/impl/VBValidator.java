package com.tarion.cepats.validators.impl;

import com.tarion.cepats.common.CEPATSUtil;
import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.domains.cepats.entities.FormItemEntity;
import com.tarion.cepats.validators.FormItemValidator;

import java.util.Arrays;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Sep 02, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 */

/**
 * Apply vendor builder specific business rules to
 * determine the validity of an item when adding/updating
 * Rule # 1: Vendor cannot add a new item
 * Rule # 2: When updating an existing item, Vendor can update only two fields: Vendor position & response
 */
public class VBValidator implements FormItemValidator {

    /**
     * Define the list of item properties to ignore during copying values from an existing item to new item
     * Note: the property name should match exactly as FormItem bean variable name
     *      in order for reflection based object comparison to work
     */
    private static final String[] UPDATEABLE_FIELDS = {"vendorsPosition", "vendorsResponse"};

    @Override
    public boolean validate(FormItemEntity oldFormItem, FormItemEntity newFormItem) throws Exception {

        Long startTime = LoggerUtil.logEnter(VBValidator.class, "info", "validate", newFormItem.getAuditRefNum());
        boolean doUpdate = true;

        if (oldFormItem != null) {
            // compare existing and new items to determine whether we need to update it or not
            if (CEPATSUtil.isEqual(oldFormItem, newFormItem, Arrays.asList(UPDATEABLE_FIELDS))) {
                doUpdate = false;
            } else {
                // copy values of non-updateable fields to new formItem
                newFormItem.setAuditRefNum(oldFormItem.getAuditRefNum());
                newFormItem.setDeficiencyDescription(oldFormItem.getDeficiencyDescription());
                newFormItem.setDeficiencyLocation(oldFormItem.getDeficiencyLocation());
                newFormItem.setPriority(oldFormItem.getPriority());
                newFormItem.setCondoCorpPosition(oldFormItem.getCondoCorpPosition());
                newFormItem.setCondoCorpResponse(oldFormItem.getCondoCorpResponse());
              	//if this item is updated then it needs to be shown as green on the web page.
                //newFormItem.setItemAdded(true);
                newFormItem.setFundingAndDescription(oldFormItem.getFundingAndDescription());
            }
        }

        LoggerUtil.logExit(VBValidator.class, "info", "validate", startTime);
        return doUpdate;
    }
}
