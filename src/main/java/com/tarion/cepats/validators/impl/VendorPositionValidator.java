package com.tarion.cepats.validators.impl;

import com.tarion.cepats.common.CEPATSConstants;
import com.tarion.cepats.common.CEPATSUtil;
import com.tarion.cepats.validators.VendorPosition;
import org.apache.commons.lang3.text.WordUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Aug 28, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 *
 * Implementation of validating vendor position field in the form item
 * to be used with @VendorPosition custom annotation
 *
 */

public class VendorPositionValidator implements ConstraintValidator<VendorPosition, String> {

    @Override
    public void initialize(VendorPosition priority) {

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {

        if(CEPATSUtil.isEmpty(s) || Arrays.asList(CEPATSConstants.vendorPositionValues).contains(WordUtils.capitalizeFully(s.trim()))) return true;
        return false;
    }
}
