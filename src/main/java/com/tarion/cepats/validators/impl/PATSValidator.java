package com.tarion.cepats.validators.impl;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component("patsValidator")
public class PATSValidator  implements Validator{

	@Override
    public boolean supports(Class<?> paramClass) {
        return XSSFSheet.class.equals(paramClass);
    }
 
    @Override
    public void validate(Object obj, Errors errors) {
    	XSSFSheet patsForm = (XSSFSheet)obj;
    	if(patsForm.getNumMergedRegions() > 0){
    		errors.reject("Merging cells is not allowed");
    	}
    }

}
