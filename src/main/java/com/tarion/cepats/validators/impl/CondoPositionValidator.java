package com.tarion.cepats.validators.impl;

import com.tarion.cepats.common.CEPATSConstants;
import com.tarion.cepats.common.CEPATSUtil;
import com.tarion.cepats.validators.CondoPosition;
import org.apache.commons.lang3.text.WordUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Aug 28, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 *
 * Implementation of validating condo position field in the form item
 * to be used with @CondoPosition custom annotation
 *
 */

public class CondoPositionValidator implements ConstraintValidator<CondoPosition, String> {

    @Override
    public void initialize(CondoPosition priority) {

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {

        if(CEPATSUtil.isEmpty(s) || Arrays.asList(CEPATSConstants.condoPositionValues).contains(WordUtils.capitalizeFully(s.trim()))) return true;
        return false;
    }
}
