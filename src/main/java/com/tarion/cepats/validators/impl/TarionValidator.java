package com.tarion.cepats.validators.impl;

import com.tarion.cepats.common.CEPATSUtil;
import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.domains.cepats.entities.FormItemEntity;
import com.tarion.cepats.validators.FormItemValidator;

import java.util.Arrays;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Sep 02, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 */

/**
 * Tarion user can update all fields
 * In case of an existing item, check whether it has been updated
 */

public class TarionValidator implements FormItemValidator {

    /**
     * Define the list of item properties to ignore during copying values from an existing item to new item
     * Note: the property name should match exactly as FormItem bean variable name
     *      in order for reflection based object comparison to work
     */
    private static final String[] UPDATEABLE_FIELDS = {"auditRefNum", "deficiencyDescription", "deficiencyLocation",
                                                       "priority", "condoCorpPosition", "condoCorpResponse",
                                                       "vendorsPosition", "vendorsResponse", "fundingAndDescription"};

    @Override
    public boolean validate(FormItemEntity oldFormItem, FormItemEntity newFormItem) throws Exception {

        Long startTime = LoggerUtil.logEnter(TarionValidator.class, "info", "validate", newFormItem.getAuditRefNum());
        boolean doUpdate = true;

        if (oldFormItem != null) {
            if (CEPATSUtil.isEqual(oldFormItem, newFormItem, Arrays.asList(UPDATEABLE_FIELDS))) {
                doUpdate = false;
            }
            else {
            	//if this item is updated then it needs to be shown as green on the web page.
                //newFormItem.setItemAdded(true);
            }
        }
        LoggerUtil.logExit(TarionValidator.class, "info", "validate", startTime);
        return doUpdate;
    }
}
