package com.tarion.cepats.validators;

import com.tarion.cepats.validators.impl.VendorPositionValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Aug 28, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 *
 * Declaration of @VendorPosition custom annotation
 *
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
@Constraint(validatedBy = VendorPositionValidator.class)
public @interface VendorPosition {

    String message() default "Please use only values provided in the Vendor's Position dropdown list";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
