package com.tarion.cepats.common;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Logger Utility class contains utility methods
 * for easy logging
 * 
 * @author bojan
 *
 */
public class LoggerUtil {

	/**
	 * Method to be used for logging exceptions in Service Methods
	 * 
	 * @param methodName
	 * @param ex
	 * @param objects
	 */
	public static <T> void logServiceError(Class<T> clazz, String methodName, Throwable ex, Object...objects) {
	    Logger logger = LoggerFactory.getLogger(clazz);
		logger.error("Exception in Service Layer: {}({}) - Original Exception was {}", methodName, objects, ex);
	}

	public static <T> void logError(Class<T> clazz, String message) {
	    Logger logger = LoggerFactory.getLogger(clazz);
		logger.error(message);
	}

    public static <T> void logError(Class<T> clazz, String message, Object... params) {
	    Logger logger = LoggerFactory.getLogger(clazz);
		logger.error(message, params);
	}

    public static <T> void logError(Class<T> clazz, String message, Throwable ex) {
	    Logger logger = LoggerFactory.getLogger(clazz);
		logger.error(message, ex);
	}

	public static <T> void logTrace(Class<T> clazz, String message) {
	    Logger logger = LoggerFactory.getLogger(clazz);
		logger.trace("TRACE: {}", message);
	}

    public static <T> void logTrace(Class<T> clazz, String message, Object... params) {
	    Logger logger = LoggerFactory.getLogger(clazz);
		logger.trace("TRACE: {}({})", message, params);
	}

	public static <T> void logDebug(Class<T> clazz, String message) {
	    Logger logger = LoggerFactory.getLogger(clazz);
		logger.debug("DEBUG: {}", message);
	}

    public static <T> void logDebug(Class<T> clazz, String message, Object... params) {
	    Logger logger = LoggerFactory.getLogger(clazz);
		logger.debug("DEBUG: {}({})", message, params);
	}

	public static <T> void logInfo(Class<T> clazz, String message) {
	    Logger logger = LoggerFactory.getLogger(clazz);
		logger.info("INFO: {}", message);
	}

    public static <T> void logInfo(Class<T> clazz, String message, Object... params) {
	    Logger logger = LoggerFactory.getLogger(clazz);
		logger.info("INFO: {}({})", message, params);
	}

	public static <T> void logWarn(Class<T> clazz, String message) {
	    Logger logger = LoggerFactory.getLogger(clazz);
		logger.warn("WARN: {}", message);
	}

    public static <T> void logWarn(Class<T> clazz, String message, Object... params) {
	    Logger logger = LoggerFactory.getLogger(clazz);
		logger.warn("WARN: {}({})", message, params);
	}

	/**
	 * Method to be used for logging start of Service Methods without input parameters
	 * 
	 * @param methodName
	 */
	public static <T> long logEnter(Class<T> clazz, String methodName) {
	    Logger logger = LoggerFactory.getLogger(clazz);

		logger.trace("ENTER: {}",  methodName);
		return System.currentTimeMillis();
	}
	
	public static <T> long logEnter(Class<T> clazz, String methodName, Object... params) {
	    Logger logger = LoggerFactory.getLogger(clazz);
	    logger.trace("ENTER: {}({})", methodName, params);
		return System.currentTimeMillis();
	}


	public static <T> void logExit(Class<T> clazz, String methodName, long startTime) {
	    Logger logger = LoggerFactory.getLogger(clazz);
		long runTime = System.currentTimeMillis() - startTime;
		logger.trace("EXIT: {} in {}ms", methodName, runTime);
	}

	public static <T> void logExit(Class<T> clazz, String methodName, Object returnValue, long startTime) {
	    Logger logger = LoggerFactory.getLogger(clazz);
		long runTime = System.currentTimeMillis() - startTime;
		logger.trace("EXIT: {} in {}ms > {}", methodName, runTime, returnValue);
	}
	
	public static <T> void logClientIp(Class<T> clazz, String message, HttpServletRequest request) {
	    Logger logger = LoggerFactory.getLogger(clazz);
	    String clientIp = request.getHeader("X-FORWARDED-FOR");   // proxy
	    if(clientIp == null) {
	    	clientIp = request.getRemoteAddr();
	    }
		logger.trace("ClientIP: {} in method {}", clientIp, message);
	}
}
