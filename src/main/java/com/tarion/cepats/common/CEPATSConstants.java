package com.tarion.cepats.common;

public interface CEPATSConstants {
	
	public static final String SESSION_ATTRIBUTE_NAME_USER = "sessionDto";
	
	public static final String ROLE_USER = "ROLE_USER";
	public static final String WRITE_ACCESS = "WRITE";
	public static final String USER = "user";
	public static final String DEFAULT_DATE_FORMAT = "MM/dd/yyyy";
	public static final String DEFAULT_TIME_FORMAT = "hh:mm a";
    public static final String CRM_DATE_FORMAT = "yyyy-MM-dd";
    public static final String ATTACHMENT_UPLOADED_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";
    
    public static final String MYHOME_CCA_READ_ONLY = "READONLY";
	
    public static final String TRUE = "true";
    public static final String Y_YES = "Y";
    public static final String N_NO = "N";
    
    /** WSDL Urls */
	public static final String CEPATS_WS_CLIENT_TBI_CM_ATTACHMENT_UPLOAD_WSDL_URL = "cepats.ws.client.tbi_cm_attachment_upload.wsdl.url";
	public static final String CEPATS_WS_CLIENT_TBI_CM_DOCUMENT_RETRIEVAL_WSDL_URL = "cepats.ws.client.tbi_cm_document_retrieval.wsdl.url";
	public static final String CEPATS_WS_CLIENT_TBI_EMAIL_SENDER_WSDL_URL = "cepats.ws.client.tbi_email_sender.wsdl.url";
	public static final String CEPATS_WS_CLIENT_TBI_PDF_CONVERTER_WSDL_URL = "cepats.ws.client.tbi_pdf_converter.wsdl.url";
	public static final String CEPATS_WS_CLIENT_BSA_CEPATS_WSDL_URL = "cepats.ws.client.bsa_cepats.wsdl.url";
	public static final String CEPATS_WS_CLIENT_TBI_CEPATS_SERVICE_WSDL_URL = "cepats.ws.client.tbi_cepats_service.wsdl.url";

    /** Upload Attachments CM ATTRIBUTE NAMES */
	public static final String CM_CEPATS_ITEM_TYPE = "BB49Document";
	public static final String CM_ATTR_CM_FILE_NAME_PREFIX = "CP-";
	public static final String CM_CEPATS_SCANNER_ID_PREFIX_HOP = "CC_";
	public static final String CM_CEPATS_SCANNER_ID_PREFIX_BSA = "VB_";
	public static final String CM_CEPATS_TEMPLATE_TYPE = "PATS Attachment";
	public static final String CM_ATTR_CASE_ID = "CaseId";
	public static final String CM_ATTR_ENROLMENT_NUMBER = "EnrolmentNumber";
	public static final String CM_ATTR_VB_NUMBER = "VBNumber";
	public static final String CM_TEMPLATE_TYPE = "TemplateType";
	public static final String CM_ATTR_SCAN_DATE = "ScanDate";
	public static final String CM_ATTR_RECEIVED_DATE = "ReceivedDate";
	public static final String CM_ATTR_SCANNER_ID = "ScannerID";
	public static final String CM_ATTR_TEMPLATE_TYPE = "TemplateType";
	public static final String CM_ATTR_STORAGE_ID = "StorageId";
	public static final String CM_ATTR_DOC_NAME = "DocName";
	public static final String CM_ATTR_CM_FILE_NAME = "CMFileName";
	
	/** JMS Constants */
	public static final String CEPATS_CRM_DATA_SOURCE = "CEPATS";
	
	/** Email Constants */
    public static final String EMAIL_FROM_ADDRESS = "email.from.address";	

    public static final String CEPATS_CRM_RESPONSE_QUEUE_NAME = "jms/CepatsCrmRspQueue";
    public static final String CEPATS_CRM_CONNECTION_FACTORY_NAME = "jms/CrmCeCF";

    /** Constant for the ping message type */
    public static final String PING = "ping";
    /** Constant for the message property for the message type */
    public static final String MESSAGE_TYPE = "MessageType";
    public static final String MESSAGE_NAME_META = "MessageName";
    public static final String MESSAGE_NAME_CRM_CEPATS_REQUEST_IN_LINE_ITEMS = "TWC_SOL_IMPORT_REQ.V1";
    		
	public static final String CEPATS_CRM_JMS_DESTINATION = "cepats.jms_destination_node";

    /** Constant for the message property for the destination nodes */
    public static final String DESTINATION_NODES_META = "DestinationNodes";


    public static final String[] priorityValues = {"", "1", "2", "3", "4", "5"};
    public static final String[] vendorPositionValues = {"", "Not Warranted", "Under Investigation",
                                                          "Repair Proposed", "Repair In Progress", "Repair Complete"};
    public static final String[] condoPositionValues = {"", "Warranty Disputed", "Repair Method Rejected",
                                                      "Under Investigation", "Repair Method Accepted", "Deficiency Resolved"};
    
    public static final int MAX_TYPICAL_NUMBER_OF_EXCEL_ROWS = 700;
    
    public static final String FORM_COLUMN_PEF = "PEF (Tarion Comment Only)";
}
