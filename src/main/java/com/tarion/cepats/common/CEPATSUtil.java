package com.tarion.cepats.common;

import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang3.ObjectUtils;

import com.tarion.cepats.domains.cepats.entities.FormTypeEnum;
import com.tarion.cepats.domains.cepats.entities.UserTypeEnum;
import com.tarion.cepats.services.exception.CepatsServiceException;
import com.tarion.cepats.web.dtos.UserDto;

public class CEPATSUtil {

    private static final String REDIRECT_FIRST_YEAR = "redirect:/firstyear/";
    private static final String REDIRECT_SECOND_YEAR = "redirect:/secondyear/";
    public static final String DATE_PATTERN_DATE_ONLY = "yyyy-MM-dd";

	public static String todayFormattedForCm() {
		DateFormat format = new SimpleDateFormat(CEPATSConstants.CRM_DATE_FORMAT);
		return format.format(new java.util.Date());
	}

	/**
	 * Convert Date to String
	 * 
	 * @param date
	 * @param format
	 * @return
	 */
	public static String convertDateToString(Date date, String format) {

		if(format == null) {
			format = CEPATSConstants.DEFAULT_DATE_FORMAT;
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date);		
	}
	
	public static String convertDateToTimeString(Date date, String format){
		if(format == null){
			format = CEPATSConstants.DEFAULT_TIME_FORMAT;
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date);		
	}

    public static Timestamp getPastTimestamp(int numberOfDays) {
		long startTime = LoggerUtil.logEnter(CEPATSUtil.class, "getPastTimestamp", numberOfDays);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, -numberOfDays);
        Date date = calendar.getTime();

        Timestamp pastDate = new Timestamp(date.getTime());

		LoggerUtil.logExit(CEPATSUtil.class, "getPastTimestamp", pastDate, startTime);
        return pastDate;
    }

    public static boolean isEmpty(String s) {
        return s == null || s.isEmpty() ? true : false;
    }

    /**
     * Use reflection to compare given fields of two objects of the same class
     *
     * @param firstObject
     * @param secondObject
     * @param includeFields - exclude these fields in value comparison
     * @return
     */
    public static boolean isEqual(Object firstObject, Object secondObject, List<String> includeFields) throws Exception {

        Long startTime = LoggerUtil.logEnter(CEPATSUtil.class, "debug", "isEqual");

        Class firstClass = firstObject.getClass();
        Class secondClass = secondObject.getClass();
        boolean equal = true;

        for (String fieldName : includeFields) {
            Field firstField = firstClass.getDeclaredField(fieldName);
            Field secondField = secondClass.getDeclaredField(fieldName);
            firstField.setAccessible(true);
            secondField.setAccessible(true);
            if (!ObjectUtils.equals(firstField.get(firstObject), secondField.get(secondObject))) {
                equal = false;
                break;
            }
        }

        LoggerUtil.logExit(CEPATSUtil.class, "debug", "isEqual", startTime);
        return equal;
    }

    /**
     * Determine whether logged in user is a Tarion user
     *
     * @param userType
     * @return
     */
    public static boolean isTarionUser(UserTypeEnum userType) {
        return userType.equals(UserTypeEnum.TARION) ? true : false;
    }
    
    /**
     * Determine whether logged in user is a VB user
     *
     * @param userType
     * @return
     */
    public static boolean isVBUser(UserTypeEnum userType) {
        return userType.equals(UserTypeEnum.VB) ? true : false;
    }
    
    /**
     * @param formType
     * @return
     */
    public static boolean isFirstYearForm(FormTypeEnum formType) {
        return formType.equals(FormTypeEnum.FIRSTYEAR) ? true : false;
    }
    
    /**
     * @param formType
     * @return
     */
    public static boolean isSecondYearForm(FormTypeEnum formType) {
        return formType.equals(FormTypeEnum.SECONDYEAR) ? true : false;
    }
    
    /**
     * Determine whether logged in user is a CCA user
     *
     * @param userType
     * @return
     */
    public static boolean isCCAUser(UserTypeEnum userType) {
        return userType.equals(UserTypeEnum.CCA) ? true : false;
    }


    public static <T> T convertXmlToJaxb(String xmlResponse, Class<T> clazz) throws CepatsServiceException {
        T response = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            StringReader stringReader = new StringReader(xmlResponse);
            response = (T) unmarshaller.unmarshal(stringReader);
        } catch (JAXBException e) {
            throw new CepatsServiceException("Error converting xml to JAXB of type: " + clazz, e);
        }

        return response;
    }

    public static <T> String convertJaxbToXml(T jaxb, Class<T> clazz) {
        StringWriter stringWriter = new StringWriter();
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
            Marshaller marshaller = jaxbContext.createMarshaller();

            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(jaxb, stringWriter);
        } catch (JAXBException e) {
            throw new CepatsServiceException("Error converting JAXB to xml ", e);
        }

        return stringWriter.toString();
    }
    
	
	/**
	 * @return
	 */
	public static boolean isUnderWarranty(FormTypeEnum formType, UserDto user) {
		if(isUnderFirstYearWarranty(formType, user)){
			return true;
		}
		if(isUnderSecondYearWarranty(formType, user)) {
			return true;
		}
		return false;
	}

	private static boolean isUnderSecondYearWarranty(FormTypeEnum formType, UserDto user) {
		return formType.equals(FormTypeEnum.SECONDYEAR) && user.getEndOfSecondYearWarranty().after(new Date(System.currentTimeMillis()));
	}

	private static boolean isUnderFirstYearWarranty(FormTypeEnum formType, UserDto user) {
		return formType.equals(FormTypeEnum.FIRSTYEAR) && user.getEndOfFirstYearWarranty().after(new Date(System.currentTimeMillis()));
	}


    public static String findMapping(UserDto user, String enrolmentNumber) {

        if(user.isFirstYearFormSubmitted()) {
            return REDIRECT_FIRST_YEAR  + enrolmentNumber+ "/";
        }

        if (user.isSecondYearFormSubmitted()) {
            return REDIRECT_SECOND_YEAR  + enrolmentNumber+ "/";
        }

        return REDIRECT_FIRST_YEAR + enrolmentNumber+ "/";
    }
    
    public static String getFirstNameLastName(String firstName, String lastName) {
    	if(isEmpty(firstName) && isEmpty(lastName)){
    		return "";
    	}
        return firstName + " " + lastName;
    }
    
    public static String getFirstNameLastName(UserDto userDto) {
        return userDto.getFirstName() + " " + userDto.getLastName();
    }
    
    public static String removeHFromEnrolment(String enrolNum) {
		if (enrolNum != null && enrolNum.length() > 1 && ("h".equalsIgnoreCase(enrolNum.substring(0, 1)))) {
			return enrolNum.toUpperCase().substring(1);
		} else if (enrolNum == null) {
			return enrolNum;
		}
		return enrolNum;
	}
    
    public static String removeBFromVB(String vbNumbder) {
		if (vbNumbder != null && vbNumbder.length() > 1 && ("b".equalsIgnoreCase(vbNumbder.substring(0, 1)))) {
			return vbNumbder.toUpperCase().substring(1);
		} else if (vbNumbder == null) {
			return vbNumbder;
		}
		return vbNumbder;
	}
    
    public static String getUserSessionAttributeName(String enrolmentNumber){
    	return CEPATSConstants.SESSION_ATTRIBUTE_NAME_USER + enrolmentNumber;
    }
    
	
	public static Timestamp convertStringToTimestamp(String date, String dateFormat) throws Exception{
		SimpleDateFormat format = new java.text.SimpleDateFormat(dateFormat);
		Date startDate = format.parse(date);
		long ms= startDate.getTime();
		return new Timestamp(ms);
	}
	
	public static String convertTimestampToString(Timestamp dateTime, String dateFormat) throws Exception{
		SimpleDateFormat format = new java.text.SimpleDateFormat(dateFormat);
		Date date = new Date(dateTime.getTime());
		return format.format(date);
	}

}
