package com.tarion.cepats.domains.crm;

import com.tarion.cepats.domains.crm.entities.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author oagady
 * Oct 1, 2014
 */
@Repository
public interface PersonRepository extends JpaRepository<PersonEntity, Long> {
	
	@Query(value="SELECT * FROM CEPATS_CE_ACCTTM_VW WHERE SITE_ID = ?1 AND USER_TYPE = ?2", nativeQuery = true)
	public List<PersonEntity> findByUserTypeAndEnrolmentNumber( String enrolmentNumber, String userType);
 
}
