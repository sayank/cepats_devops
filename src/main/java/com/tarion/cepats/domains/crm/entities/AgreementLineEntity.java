package com.tarion.cepats.domains.crm.entities;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author oagady
 * @date Oct 27, 2014
 * @version 1.0
 *
 */
@Entity
@Table(name = "PS_RF_AGREEMENT_LN")
public class AgreementLineEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "AGREEMENT_CODE")
	private String enrolmentNumber;
	
	@Column(name = "MESSAGE_NBR")
	private Integer messageNBR;
	
	@Column(name = "MESSAGE_SET_NBR")
	private Integer messageSetNBR;
	
	@Column(name = "END_DT")
	private Timestamp agreementLineEndDate;

	public String getEnrolmentNumber() {
		return enrolmentNumber;
	}

	public void setEnrolmentNumber(String enrolmentNumber) {
		this.enrolmentNumber = enrolmentNumber;
	}


	public Timestamp getAgreementLineEndDate() {
		return agreementLineEndDate;
	}

	public void setAgreementLineEndDate(Timestamp agreementLineEndDate) {
		this.agreementLineEndDate = agreementLineEndDate;
	}

	public Integer getMessageNBR() {
		return messageNBR;
	}

	public void setMessageNBR(Integer messageNBR) {
		this.messageNBR = messageNBR;
	}

	public Integer getMessageSetNBR() {
		return messageSetNBR;
	}

	public void setMessageSetNBR(Integer messageSetNBR) {
		this.messageSetNBR = messageSetNBR;
	}


}
