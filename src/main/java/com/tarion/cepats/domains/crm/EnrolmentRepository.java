package com.tarion.cepats.domains.crm;

import java.math.BigDecimal;

import com.tarion.cepats.domains.crm.entities.EnrolmentEntity;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Repository
public class EnrolmentRepository {

    public static final String ENROLMENT  = "SELECT B.SITE_ID, F.COMPANYID, START_DT, D.ADDRESS1, CITY, STATE, POSTAL "
    		+ "FROM PS_RD_SITE B, PS_BO_CM C, PS_CM D, PS_RF_AGREEMENT E, PS_RD_COMPANY F "
    		+ "WHERE B.SITE_ID = E.SITE_ID AND B.BO_ID = C.BO_ID AND C.CM_ID = D.CM_ID AND B.BO_ID_PARENT = F.BO_ID "
    		+ "AND C.BO_CM_START_DT <= GETDATE() AND C.BO_CM_END_DT > GETDATE() "
    		+ "AND C.CM_PURPOSE_TYPE_ID = 35 AND C.CM_TYPE_ID = 1 "
    		+ "AND exists (select 1 from PS_RC_CASE A where A.BO_ID_SITE = B.BO_ID AND A.CASE_TYPE = 'CEDEF') "
    		+ "AND B.SITE_ID = ?1";
    
    public static final String CEDEF_CASE_ID = 
    		" SELECT A.CASE_ID " +
    		" from PS_RC_CASE A, PS_RD_SITE B " +
    		" WHERE A.BO_ID_SITE = B.BO_ID AND B.SITE_ID = ?1 " +
    		" AND A.CASE_TYPE = 'CEDEF' AND A.RC_STATUS IN ('OPEN','INACT') ";
    
    public static final String WARRANTY_TIMELINES = "";

    public static final String ENROLMENT_RESULTSET_MAPPING = "EnrolmentResult";

    public EnrolmentRepository(@Qualifier("crmEntityManagerFactory") EntityManagerFactory crmEntityManagerFactory){
        this.crmEm = crmEntityManagerFactory.createEntityManager();
    }

    private EntityManager crmEm;

    public EnrolmentEntity findByEnrolmentNumber(String enrolmentNumber) {
        Query query = crmEm.createNativeQuery(ENROLMENT, ENROLMENT_RESULTSET_MAPPING);
        query.setParameter(1, enrolmentNumber);
        return (EnrolmentEntity)query.getSingleResult();
    }

    public String findCedefCaseIdByEnrolmentNumber(String enrolmentNumber) {
        Query query = crmEm.createNativeQuery(CEDEF_CASE_ID);
        query.setParameter(1, enrolmentNumber);
        BigDecimal queryResult = (BigDecimal)query.getSingleResult();
        String returnString = null;
        try {
			returnString = queryResult.toPlainString();
		} catch (Exception e) {
			returnString = null;
		}
        return returnString;
    }
}
