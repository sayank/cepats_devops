package com.tarion.cepats.domains.crm;

import com.tarion.cepats.domains.crm.entities.CondoConversionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface CondoConversionRepository extends JpaRepository<CondoConversionEntity, Long> {
	
    @Query("SELECT entity.condoConversionFlag FROM CondoConversionEntity entity WHERE entity.enrolmentNumber = ?1")
    public String findCondoConversionFlag(String enrolmentNumber);
 
}
