package com.tarion.cepats.domains.crm.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * 
 * @author oagady
 * @date Oct 23, 2014
 * @version 1.0
 *
 */
@Entity
public class PersonEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	

	@Column(name = "USER_ID")
	private String userId;

	@Id
	@Column(name = "EMAIL_ADDR")
	private String emailAddress;
	
	@Column(name = "FIRST_NAME")
	private String firstName;
	
	@Column(name = "LAST_NAME")
	private String lastName;
	
	@Column(name = "USER_TYPE")
	private String userType;

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}


	
}
