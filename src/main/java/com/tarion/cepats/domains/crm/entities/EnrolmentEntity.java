package com.tarion.cepats.domains.crm.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @author oagady
 * @date Oct 7, 2014
 * @version 1.0
 *
 */

@SqlResultSetMapping(
        name="EnrolmentResult",
        entities={
                @EntityResult(
                        entityClass=EnrolmentEntity.class,
                        fields={@FieldResult(name="enrolmentNumber", column="SITE_ID"),
                                @FieldResult(name="vbNumber", column="COMPANYID"),
                                @FieldResult(name="warrantyStartDate", column="START_DT"),
                                @FieldResult(name="streetAddress", column="ADDRESS1"),
                                @FieldResult(name="city", column="CITY"),
                                @FieldResult(name="province", column="STATE"),
                                @FieldResult(name="postalCode", column="POSTAL"),
                })
        })

@Entity
public class EnrolmentEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String enrolmentNumber;
	private String vbNumber;
	private Timestamp warrantyStartDate;
    private String streetAddress;
    private String city;
    private String province;
    private String postalCode;

	public String getEnrolmentNumber() {
		return enrolmentNumber;
	}

	public void setEnrolmentNumber(String enrolmentNumber) {
		this.enrolmentNumber = enrolmentNumber;
	}

    public String getVbNumber() {
        return vbNumber;
    }

    public void setVbNumber(String vbNumber) {
        this.vbNumber = vbNumber;
    }

    public Timestamp getWarrantyStartDate() {
		return warrantyStartDate;
	}

	public void setWarrantyStartDate(Timestamp warrantyStartDate) {
		this.warrantyStartDate = warrantyStartDate;
	}

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
}
