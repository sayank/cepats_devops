/* 
 * 
 * dashboardPropertiesEntity.java
 *
 * Copyright (c) 2012 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.cepats.domains.cepats.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

 /**
 * 
 * @author oagady
 * @date Sept 25, 2014
 * @version 1.0
 *
 */
@Entity
@Table(name = "CEPATS_CONFIG")
public class ConfigEntity implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    private String name;
    private String description;
    private String value;

    /**
     * Instantiates a new CEPATS properties entity.
     */
    public ConfigEntity() {
        super();
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(String value) {
        this.value = value;
    }

}
