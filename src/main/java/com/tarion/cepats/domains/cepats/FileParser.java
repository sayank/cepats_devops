package com.tarion.cepats.domains.cepats;

import com.tarion.cepats.common.CEPATSConstants;
import com.tarion.cepats.common.CEPATSUtil;
import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.domains.cepats.entities.FormItemEntity;
import com.tarion.cepats.domains.cepats.entities.UserTypeEnum;
import com.tarion.cepats.web.dtos.UserDto;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.*;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Sep 18, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 *
 * Parse the data file and validate each item
 *
 */

@Component
public class FileParser {

	private static final String HEADER_ROW_IS_MISSING = "Header row is missing";

	private static final String EXPECTED_HEADER_IS_MISSING = "Expected header is missing: ";

	private static final String ERROR_MESSAGE_INCORRECT_HEADER = "You have altered the Header details on your PATS.  Please use the standard CE PATS template format in order to upload.";

    private List<FormItemEntity> parsedItems;

    @Autowired
    private FormItemRepository formItemRepository;


    public String parseCsv(InputStream inputStream, UserDto user, boolean isCondoConversion) throws Exception {

        Long startTime = LoggerUtil.logEnter(FileParser.class, "info", "parseCsv",
                                             user.getFormType(), user.getEnrolment().getEnrolmentNumber());

        this.parsedItems = new ArrayList<>();

        try (ICsvBeanReader csvBeanReader = new CsvBeanReader(new InputStreamReader(inputStream),
                                                              CsvPreference.STANDARD_PREFERENCE)) {
            /**
             * Read header from & validate it
             * then configure beanFields & fieldType for automatically populating
             */
            final String[] header = csvBeanReader.getHeader(true);
            if (header == null || header.length == 0) {
                LoggerUtil.logError(FileParser.class, HEADER_ROW_IS_MISSING);
                return HEADER_ROW_IS_MISSING;
            }
            Map<String, String>headerRowMap = this.getHeaderColumnsMapping(isCondoConversion);
            CellProcessor[] cellProcessors = new CellProcessor[getHeaderColumnsMapping(isCondoConversion).size()];
            String[] beanFields = new String[headerRowMap.size()];
            for (int i = 0; i < header.length; i++) {
//                if (!headerRowMap.containsKey(WordUtils.capitalizeFully(header[i]))) {
                if (CEPATSUtil.isEmpty(header[i]) || !headerRowMap.containsKey(header[i])) {
                    LoggerUtil.logError(FileParser.class, "Unacceptable header column={}", header[i]);
                    return ERROR_MESSAGE_INCORRECT_HEADER;
                }
                beanFields[i] = headerRowMap.get(header[i]);
                if (beanFields[i].equals("itemId")) {
                    cellProcessors[i] = new Optional(new ParseInt());
                } else {
                    cellProcessors[i] = new Optional();
                }
            }

            // read and validate the retrieve item
            FormItemEntity newFormItem;
            while ((newFormItem = csvBeanReader.read(FormItemEntity.class, beanFields, cellProcessors)) != null) {
                this.parsedItems.add(newFormItem);
            }
        }  catch (Exception ex) {
        	LoggerUtil.logError(FileParser.class, "Failed reading csv file", ex);
            return new String("Failed reading csv file: " + ex.getMessage());
        }

        LoggerUtil.logExit(FileParser.class, "info", "parseCsv", startTime);
        return null;
    }

    public String parseExcel(InputStream inputStream, UserDto user, boolean initialUpload, boolean isCondoConversion) throws Exception {

        Long startTime = LoggerUtil.logEnter(FileParser.class, "info", "parseExcel",
                                             user.getFormType(), user.getEnrolment().getEnrolmentNumber());

        XSSFWorkbook workbook = null;
        try {
			this.parsedItems = new ArrayList<>();
			workbook = new XSSFWorkbook(inputStream);
        }catch(Exception e){
        	LoggerUtil.logError(FileParser.class, "Error creating excel workbook from uploaded file:  ", e);
        	return "Error creating excel workbook from uploaded file:  " + e.getMessage();
        }
        
        try{
			XSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);
        }catch(Exception e){
        	LoggerUtil.logError(FileParser.class, "Error evaluating formula cells  ", e);
        	return "Error evaluating formula cells:  " + e.getMessage();
        }
        
        try{
			XSSFSheet patsForm = workbook.getSheetAt(0);
			Map<String, String>headerRowMap = this.getHeaderColumnsMapping(isCondoConversion);
			DataFormatter formatter = new DataFormatter();

			int firstRow = patsForm.getFirstRowNum();
			int lastRow = patsForm.getLastRowNum();
			XSSFRow row = null;

			// read header row and validate it
			String[] beanFields = new String[headerRowMap.size()];
			row = patsForm.getRow(firstRow);
			if(row == null){
				LoggerUtil.logError(FileParser.class, HEADER_ROW_IS_MISSING);
				return HEADER_ROW_IS_MISSING;
			}
			
			Iterator<String> iter = headerRowMap.keySet().iterator();
			int count = 0;
			while (iter.hasNext()) {
				String key = iter.next();

			    Cell cell = row.getCell(count);
			    if (cell == null) {
			    	//PEF column can be missing if the template is the original template without PEF column
		        	if(key.equals(CEPATSConstants.FORM_COLUMN_PEF)){
		        		continue;
		        	}
		            LoggerUtil.logError(FileParser.class, EXPECTED_HEADER_IS_MISSING + key);
		            return EXPECTED_HEADER_IS_MISSING + key;
			    }

			    // set cell type to string to avoid boilerplate code to determine cell type and conversion
			    cell.setCellType(CellType.STRING);
			    String headerValue = row.getCell(count).getStringCellValue();
			    //The header column map contains PEF column as "PEF (Tarion Comment Only)"
			    //while the excel spreadsheet has it as "PEF/n(Tarion Comment Only)" because the header is printed in two lines
			    if(key.equals(CEPATSConstants.FORM_COLUMN_PEF) && headerValue.indexOf("PEF") != -1){
			    	//only Tarion users can modify PEF column
			    	if(user.getLoggedinUserType() == UserTypeEnum.TARION){
			    		beanFields[count] = headerRowMap.get(CEPATSConstants.FORM_COLUMN_PEF);			    		
			    	}else{
			    		continue;
			    	}
			    }else{
			       if (CEPATSUtil.isEmpty(headerValue) || !headerRowMap.containsKey(headerValue)) {
			            LoggerUtil.logError(FileParser.class, "Unknown header: {}", headerValue);
			            return ERROR_MESSAGE_INCORRECT_HEADER;
			       }			            	
			          beanFields[count] = headerRowMap.get(headerValue);
			    }
			   count++;
			}

			//Skip the header and parse rest of the file
			for(int i = firstRow + 1; i <= lastRow; i++) {

			    row = patsForm.getRow(i);
			    if (row != null) {
			        // use reflection to populate the fields in the FormItem bean
			        FormItemEntity newFormItem = null;
			        for (int j = 0; j < beanFields.length; j++) {
			        	//When for a condo conversion form user uploads original PATS template without PEF column,
			        	//the field for PEF column is null
			        	if(beanFields[j] == null){
			        		continue;
			        	}
			        	if(initialUpload){
			        		//skip validating item # since it will be generated automatically
			        		if(beanFields[j].equals("itemId")){
			        			continue;
			        		}
			        	}
			            Cell cell = row.getCell(j);
			            if (cell != null) {
			            	if(cell.getCellType() == CellType.FORMULA){
			            		LoggerUtil.logError(FileParser.class, "PATS with Excel formulas are not permitted.");
			            		return "PATS with Excel formulas are not permitted. Please use text instead.";
			            	}

			            	String cellValue = null;
			            	if(cell.getCellType() == CellType.NUMERIC){
			            		cellValue =  formatter.formatCellValue(cell);
			            	}else{
			            		cell.setCellType(CellType.STRING);
				                cellValue = row.getCell(j).getStringCellValue();
			            	}

			                if (!CEPATSUtil.isEmpty(cellValue)) {
			                    if (newFormItem == null) {
			                        newFormItem = new FormItemEntity();
			                    }
			                    Field field = FormItemEntity.class.getDeclaredField(beanFields[j]);
			                    field.setAccessible(true);
			                    String fieldType = field.getType().getName();
			                    if (fieldType.equalsIgnoreCase("java.lang.Integer")) {
			                        try {
										field.set(newFormItem, Integer.parseInt(cellValue));
									} catch (Exception e) {
										// This can happen if the user added new row with alpha-numerical item #
										// This item # should be ignored
										LoggerUtil.logError(FileParser.class, "Only integers are allowed in '" + getColumnName(beanFields[j], isCondoConversion) + "' column", e);
										return "Only integers are allowed in '" + getColumnName(beanFields[j], isCondoConversion) + "' column";
									}
			                    } else {
			                        field.set(newFormItem, cellValue.trim());
			                    }
			                }
			            }
			        }

			        // ignore empty row and validate the retrieved item
			        if (newFormItem != null) {
			            this.parsedItems.add(newFormItem);
			        }
			    }
			}
		} catch (Exception e) {
			LoggerUtil.logError(FileParser.class, "Error parsing Excel file: ", e);
			 return "Failed reading Excel file: " + e.getMessage();
		}

        LoggerUtil.logExit(FileParser.class, "info", "parseExcel", startTime);
        return null;
    }

	/**
     * Create a map to validate and parse CSV header row
     * key: list of acceptable values in header row
     * value: FormItem instance variable name so csv reader could populate the FormItem bean automatically
     * Note: values in map should match exactly as variable names in FormItem bean
     */
    private final Map<String, String> getHeaderColumnsMapping(boolean isCondoConversion) {

        Map<String, String> headerColumnMap = new LinkedHashMap<>();

        headerColumnMap.put("Item #", "itemId");
        headerColumnMap.put("PA Ref. #", "auditRefNum");
        headerColumnMap.put("Deficiency Description", "deficiencyDescription");
        headerColumnMap.put("Deficiency Location", "deficiencyLocation");
        headerColumnMap.put("Priority", "priority");
        headerColumnMap.put("Vendor's Position", "vendorsPosition");
        headerColumnMap.put("Vendor's Response", "vendorsResponse");
        headerColumnMap.put("Condo Corp Position", "condoCorpPosition");
        headerColumnMap.put("Condo Corp Response", "condoCorpResponse");
        if(isCondoConversion){
        	headerColumnMap.put(CEPATSConstants.FORM_COLUMN_PEF, "fundingAndDescription");        	
        }

        return headerColumnMap;
    }

    private String getColumnName(String fieldName, boolean isCondoConversion){
    	Map<String, String> headerColumnMap = getHeaderColumnsMapping(isCondoConversion);
    	Iterator<String> iter = headerColumnMap.keySet().iterator();

    	while(iter.hasNext()){
    		String key = iter.next();
    		if(headerColumnMap.get(key).equals(fieldName)){
    			return key;
    		}
    	}

    	return null;
    }


    public void setParsedItems(List<FormItemEntity> parsedItems) {
    	this.parsedItems = parsedItems;
    }

    public List<FormItemEntity> getParsedItems() {
        return parsedItems;
    }
}
