package com.tarion.cepats.domains.cepats.entities;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Sep 05, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 */
public enum UpdateTypeEnum {
    CSV("csv"),
    EXCEL("excel"),
    INLINE ("inline");

    private String label;

    UpdateTypeEnum(String label) {  this.label = label; }
    public String getLabel() { return this.label;  }
}
