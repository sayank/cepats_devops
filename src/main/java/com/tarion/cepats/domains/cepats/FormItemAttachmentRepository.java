package com.tarion.cepats.domains.cepats;

import com.tarion.cepats.domains.cepats.entities.FormItemAttachmentEntity;
import com.tarion.cepats.services.attachments.AttachmentUploadStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Sep 17, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 */
@Repository
public interface FormItemAttachmentRepository extends JpaRepository<FormItemAttachmentEntity, Long> {

    @Query("select e from FormItemAttachmentEntity e  where e.attachmentUploadStatus = ?1")
    public List<FormItemAttachmentEntity> findAllFormItemAttachmentForAttachmentUploadStatus(AttachmentUploadStatus attachmentUploadStatus);

    @Query("select e from FormItemAttachmentEntity e  where e.id = ?1")
    public FormItemAttachmentEntity findFormItemAttachmentForId(Long id);
    
    @Query("select e from FormItemAttachmentEntity e  where e.cmItemId = ?1")
    public FormItemAttachmentEntity findFormItemAttachmentForItemId(String id);
    
    @Query("select e from FormItemAttachmentEntity e  where e.formItem.id = ?1")
    public List<FormItemAttachmentEntity> findFormItemAttachmentForFormItemId(Long formItemId);

    @Query("select count(e.id) from FormItemAttachmentEntity e  where e.formItem.id = ?1")
    public int attachmentsCount(Long formItemId);
}
