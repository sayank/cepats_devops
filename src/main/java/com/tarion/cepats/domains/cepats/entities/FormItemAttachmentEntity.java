package com.tarion.cepats.domains.cepats.entities;

import javax.persistence.*;

import com.tarion.cepats.services.attachments.AttachmentUploadStatus;

import java.io.Serializable;
import java.sql.Timestamp;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Sep 08, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 */
@Entity
@Table(name = "FORM_ITEMS_ATTACHMENTS")
public class FormItemAttachmentEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(optional = false, cascade = {PERSIST, MERGE})
    @JoinColumn(name = "formItemId")
    private FormItemEntity formItem;

    @Column(name = "originalFileName")
    private String originalFileName;

    @Column(name = "mimeType")
    private String mimeType;

    @Column(name = "cmItemId")
    private String cmItemId;
    
	@Column(name = "referenceNumber")
	private String referenceNumber;
	
	@Column(name = "caseId")
	private String caseId;
	
	@Column(name = "enrolmentNumber")
	private Long enrolmentNumber;
	
	@Column(name = "vbNumber")
	private String vbNumber;
	
	@Column(name = "submitCrmWorklist")
	private boolean submitCrmWorklist;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "uploadStatus")
    private AttachmentUploadStatus attachmentUploadStatus;

    @Column(name = "attachmentsXML")
    private String attachmentXml;

    @Column(name = "errorMsg")
    private String errorMsg;

    @Column(name = "uploadedDateTime")
    private Timestamp uploadedDateTime;

    @Column(name = "uploadedUser")
    private String uploadedUser;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "uploadedUserType")
    private UserTypeEnum uploadedUserType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public FormItemEntity getFormItem() {
        return formItem;
    }

    public void setFormItem(FormItemEntity formItem) {
        this.formItem = formItem;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getCmItemId() {
        return cmItemId;
    }

    public void setCmItemId(String cmItemId) {
        this.cmItemId = cmItemId;
    }

    public AttachmentUploadStatus getAttachmentUploadStatus() {
        return attachmentUploadStatus;
    }

    public void setAttachmentUploadStatus(AttachmentUploadStatus attachmentUploadStatus) {
        this.attachmentUploadStatus = attachmentUploadStatus;
    }

    public String getAttachmentXml() {
        return attachmentXml;
    }

    public void setAttachmentXml(String attachmentXml) {
        this.attachmentXml = attachmentXml;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Timestamp getUploadedDateTime() {
        return uploadedDateTime;
    }

    public void setUploadedDateTime(Timestamp uploadedDateTime) {
        this.uploadedDateTime = uploadedDateTime;
    }

    public String getUploadedUser() {
        return uploadedUser;
    }

    public void setUploadedUser(String uploadedUser) {
        this.uploadedUser = uploadedUser;
    }

    public UserTypeEnum getUploadedUserType() {
        return uploadedUserType;
    }

    public void setUploadedUserType(UserTypeEnum uploadedUserType) {
        this.uploadedUserType = uploadedUserType;
    }

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public Long getEnrolmentNumber() {
		return enrolmentNumber;
	}

	public void setEnrolmentNumber(Long enrolmentNumber) {
		this.enrolmentNumber = enrolmentNumber;
	}

	public String getVbNumber() {
		return vbNumber;
	}

	public void setVbNumber(String vbNumber) {
		this.vbNumber = vbNumber;
	}

	public boolean isSubmitCrmWorklist() {
		return submitCrmWorklist;
	}

	public void setSubmitCrmWorklist(boolean submitCrmWorklist) {
		this.submitCrmWorklist = submitCrmWorklist;
	}
    
	
    
}
