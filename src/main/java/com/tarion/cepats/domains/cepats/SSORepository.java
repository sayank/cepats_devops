package com.tarion.cepats.domains.cepats;

import com.tarion.cepats.domains.cepats.entities.SsoLoginEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author oagady
 * Oct 1, 2014
 */
@Repository
public interface SSORepository extends JpaRepository<SsoLoginEntity, Long> {
	
    @Query("SELECT item FROM SsoLoginEntity item WHERE item.token = ?1")
    public SsoLoginEntity findUser(String token);

    @Modifying
    @Transactional
    @Query("DELETE FROM SsoLoginEntity item WHERE item.token = ?1")
    public void deleteToken(String token);   

    @Modifying
    @Transactional
    @Query(value="delete FROM SSO_LOGIN WHERE createdDateTime <  DATEADD(minute, -5, getdate())", nativeQuery = true)
    public void deleteStaleTokens();

}
