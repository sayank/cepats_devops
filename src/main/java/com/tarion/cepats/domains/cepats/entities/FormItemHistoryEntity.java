package com.tarion.cepats.domains.cepats.entities;

import com.tarion.cepats.validators.CondoPosition;
import com.tarion.cepats.validators.Priority;
import com.tarion.cepats.validators.VendorPosition;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Sep 05, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 */

@Entity
@Table(name = "FORM_ITEMS_HISTORY")
public class FormItemHistoryEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "version")
    private Integer version;

    @Column(name = "itemId")
    private Integer itemId;

    @Column(name = "enrolmentNumber")
    private String enrolmentNumber;

    @Column(name = "formType")
    @Enumerated(EnumType.STRING)
    private FormTypeEnum formType;

    @Column(name = "refId")
    @NotNull
    @Size(min = 1, max = 95)
    private String auditRefNum;

    @Column(name = "deficiencyDescription")
    @NotNull
    @Size(min = 1, max = 2000)
    private String deficiencyDescription;

    @Column(name = "deficiencyLocation")
    @NotNull
    @Size(min = 1, max = 1400)
    private String deficiencyLocation;

    @Column(name = "priority")
    @Priority
    private String priority;

    @Column(name = "vendorsPosition")
    @VendorPosition
    private String vendorsPosition;

    @Column(name = "vendorsResponse")
    private String vendorsResponse;

    @Column(name = "condoCorpPosition")
    @CondoPosition
    private String condoCorpPosition;

    @Column(name = "condoCorpResponse")
    private String condoCorpResponse;
    
    @Column(name = "fundingAndDescription")
    private String fundingAndDescription;

    @Column(name = "updateType")
    @Enumerated(EnumType.STRING)
    private UpdateTypeEnum updateType;

    @Column(name = "updatedUser")
    private String user;
    
    @Column(name="updatedUserFirstName")
    private String updatedUserFirstName;
    
    @Column(name="updatedUserLastName")
    private String updatedUserLastName;
    
    @Column(name = "updatedUserEmailAddress")
    private String updatedUserEmailAddress;

    @Column(name = "updatedUserType")
    @Enumerated(EnumType.STRING)
    private UserTypeEnum userType;

    @Column(name = "updatedDateTime")
    private Timestamp updatedDateTime;

    @Column(name = "uploadedToCRM")
    private boolean uploadedToCRM;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getEnrolmentNumber() {
        return enrolmentNumber;
    }

    public void setEnrolmentNumber(String enrolmentNumber) {
        this.enrolmentNumber = enrolmentNumber;
    }

    public FormTypeEnum getFormType() {
        return formType;
    }

    public void setFormType(FormTypeEnum formType) {
        this.formType = formType;
    }

    public String getFundingAndDescription() {
		return fundingAndDescription;
	}

	public void setFundingAndDescription(String fundingAndDescription) {
		this.fundingAndDescription = fundingAndDescription;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getAuditRefNum() {
        return auditRefNum;
    }

    public void setAuditRefNum(String refId) {
        this.auditRefNum = refId;
    }

    public String getDeficiencyDescription() {
        return deficiencyDescription;
    }

    public void setDeficiencyDescription(String deficiencyDescription) {
        this.deficiencyDescription = deficiencyDescription;
    }

    public String getDeficiencyLocation() {
        return deficiencyLocation;
    }

    public void setDeficiencyLocation(String deficiencyLocation) {
        this.deficiencyLocation = deficiencyLocation;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getVendorsPosition() {
        return vendorsPosition;
    }

    public void setVendorsPosition(String vendorsPosition) {
        this.vendorsPosition = vendorsPosition;
    }

    public String getVendorsResponse() {
        return vendorsResponse;
    }

    public void setVendorsResponse(String vendorsResponse) {
        this.vendorsResponse = vendorsResponse;
    }

    public String getCondoCorpPosition() {
        return condoCorpPosition;
    }

    public void setCondoCorpPosition(String condoCorpPosition) {
        this.condoCorpPosition = condoCorpPosition;
    }

    public String getCondoCorpResponse() {
        return condoCorpResponse;
    }

    public void setCondoCorpResponse(String condoCorpResponse) {
        this.condoCorpResponse = condoCorpResponse;
    }

    public UpdateTypeEnum getUpdateType() {
        return updateType;
    }

    public void setUpdateType(UpdateTypeEnum updateType) {
        this.updateType = updateType;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public UserTypeEnum getUserType() {
        return userType;
    }

    public void setUserType(UserTypeEnum userType) {
        this.userType = userType;
    }

    public Timestamp getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(Timestamp updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    public boolean isUploadedToCRM() {
        return uploadedToCRM;
    }

    public void setUploadedToCRM(boolean uploadedToCRM) {
        this.uploadedToCRM = uploadedToCRM;
    }

	public String getUpdatedUserFirstName() {
		return updatedUserFirstName;
	}

	public void setUpdatedUserFirstName(String updatedUserFirstName) {
		this.updatedUserFirstName = updatedUserFirstName;
	}

	public String getUpdatedUserLastName() {
		return updatedUserLastName;
	}

	public void setUpdatedUserLastName(String updatedUserLastName) {
		this.updatedUserLastName = updatedUserLastName;
	}

	public String getUpdatedUserEmailAddress() {
		return updatedUserEmailAddress;
	}

	public void setUpdatedUserEmailAddress(String updatedUserEmailAddress) {
		this.updatedUserEmailAddress = updatedUserEmailAddress;
	}
    
	
}
