/* 
 * 
 * dashboardPropertiesEntity.java
 *
 * Copyright (c) 2012 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.cepats.domains.cepats.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

 /**
 * 
 * @author oagady
 * @date Nov 5, 2014
 * @version 1.0
 *
 */
@Entity
@Table(name = "FORM_ITEMS")
public class InitialUploadEntity implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    private String enrolmentNumber;
    private int version;
	public String getEnrolmentNumber() {
		return enrolmentNumber;
	}
	public void setEnrolmentNumber(String enrolmentNumber) {
		this.enrolmentNumber = enrolmentNumber;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}

}
