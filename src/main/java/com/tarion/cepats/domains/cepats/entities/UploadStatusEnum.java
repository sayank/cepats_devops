package com.tarion.cepats.domains.cepats.entities;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Sep 08, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 */
public enum UploadStatusEnum {
    SUBMITTED("submitted"),
    INPROGRESS("inprogress");

    private String label;

    UploadStatusEnum(String label) {    this.label = label; }
    public String getLabel() {  return this.label;  }
}
