package com.tarion.cepats.domains.cepats;

import com.tarion.cepats.domains.cepats.entities.ContentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContentRepository extends JpaRepository<ContentEntity, String> {

	@Query("SELECT item FROM ContentEntity item WHERE item.contentType = ?1 ORDER BY item.contentNumber ASC")
    public List<ContentEntity> findByContentType(String contentType);
}
