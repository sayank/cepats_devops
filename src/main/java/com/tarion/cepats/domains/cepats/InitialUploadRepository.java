package com.tarion.cepats.domains.cepats;

import com.tarion.cepats.domains.cepats.entities.InitialUploadEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;


@Repository
public interface InitialUploadRepository extends JpaRepository<InitialUploadEntity, Long> {
    
    @Query(value="SELECT enrolmentNumber, MAX(version) FROM  FORM_ITEMS WHERE updatedDateTime > ?1 AND formType = ?2 GROUP BY enrolmentNumber HAVING MAX(version) = 1", nativeQuery = true)
    public List<InitialUploadEntity> findInitialUploads(Timestamp timestamp, String formType);
}
