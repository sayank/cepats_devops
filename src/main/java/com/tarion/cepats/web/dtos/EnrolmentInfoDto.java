package com.tarion.cepats.web.dtos;

import java.util.ArrayList;
import java.util.List;

import com.tarion.tbi.services.ActivityTimelineDatesDto;

public class EnrolmentInfoDto {
	
	private List<ActivityTimelineDatesDto> timelines = new ArrayList<>();
	private String wscAssigned;
	private String wsrAssigned;
	private String wscAssignedEmail;
	private String wsrAssignedEmail;
	private String errorMessage;
	
	
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public List<ActivityTimelineDatesDto> getTimelines() {
		return timelines;
	}
	public void setTimelines(List<ActivityTimelineDatesDto> timelines) {
		this.timelines = timelines;
	}
	public String getWscAssigned() {
		return wscAssigned;
	}
	public void setWscAssigned(String wscAssigned) {
		this.wscAssigned = wscAssigned;
	}
	public String getWsrAssigned() {
		return wsrAssigned;
	}
	public void setWsrAssigned(String wsrAssigned) {
		this.wsrAssigned = wsrAssigned;
	}
	public String getWscAssignedEmail() {
		return wscAssignedEmail;
	}
	public void setWscAssignedEmail(String wscAssignedEmail) {
		this.wscAssignedEmail = wscAssignedEmail;
	}
	public String getWsrAssignedEmail() {
		return wsrAssignedEmail;
	}
	public void setWsrAssignedEmail(String wsrAssignedEmail) {
		this.wsrAssignedEmail = wsrAssignedEmail;
	}



}
