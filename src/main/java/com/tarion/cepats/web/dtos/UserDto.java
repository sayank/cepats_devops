package com.tarion.cepats.web.dtos;

import java.io.Serializable;
import java.sql.Timestamp;

import org.springframework.stereotype.Component;

import com.tarion.cepats.domains.crm.entities.EnrolmentEntity;
import com.tarion.cepats.domains.cepats.entities.FormTypeEnum;
import com.tarion.cepats.domains.cepats.entities.UserTypeEnum;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Sep 17, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 */
@Component
public class UserDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private EnrolmentEntity enrolment;
    private String username;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private UserTypeEnum loggedinUserType;
    private UserTypeEnum updateAsUserType;
    private FormTypeEnum formType;
    private boolean firstYearFormSubmitted;
    private boolean secondYearFormSubmitted;
    
    private Timestamp endOfFirstYearWarranty;   //end of first year warranty (this value cannot be calculated by adding one year  
    										    //to warranty start date, because it can be manually extended by CRM users)
    
    private Timestamp endOfSecondYearWarranty;  //end of second year warranty (this value cannot be calculated by adding two years
    										    //to warranty start date, because it can be manually extended by CRM users)

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserTypeEnum getLoggedinUserType() {
        return loggedinUserType;
    }

    public void setLoggedinUserType(UserTypeEnum loggedinUserType) {
        this.loggedinUserType = loggedinUserType;
    }

    public UserTypeEnum getUpdateAsUserType() {
        return updateAsUserType;
    }

    public void setUpdateAsUserType(UserTypeEnum updateAsUserType) {
        this.updateAsUserType = updateAsUserType;
    }

    public FormTypeEnum getFormType() {
        return formType;
    }

    public void setFormType(FormTypeEnum formType) {
        this.formType = formType;
    }

	public boolean isFirstYearFormSubmitted() {
		return firstYearFormSubmitted;
	}

	public void setFirstYearFormSubmitted(boolean firstYearFormSubmitted) {
		this.firstYearFormSubmitted = firstYearFormSubmitted;
	}

	public boolean isSecondYearFormSubmitted() {
		return secondYearFormSubmitted;
	}

	public void setSecondYearFormSubmitted(boolean secondYearFormSubmitted) {
		this.secondYearFormSubmitted = secondYearFormSubmitted;
	}

    public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

    public EnrolmentEntity getEnrolment() {
        return enrolment;
    }

    public void setEnrolment(EnrolmentEntity enrolment) {
        this.enrolment = enrolment;
    }

	public Timestamp getEndOfFirstYearWarranty() {
		return endOfFirstYearWarranty;
	}

	public void setEndOfFirstYearWarranty(Timestamp endOfFirstYearWarranty) {
		this.endOfFirstYearWarranty = endOfFirstYearWarranty;
	}

	public Timestamp getEndOfSecondYearWarranty() {
		return endOfSecondYearWarranty;
	}

	public void setEndOfSecondYearWarranty(Timestamp endOfSecondYearWarranty) {
		this.endOfSecondYearWarranty = endOfSecondYearWarranty;
	}


}
