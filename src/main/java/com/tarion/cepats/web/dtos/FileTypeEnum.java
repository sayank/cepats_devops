package com.tarion.cepats.web.dtos;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Sep 19, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 */
public enum FileTypeEnum {
    CSV("csv"),
    EXCEL("xls"),
    EXCELX("xlsx");

    private String label;

    private FileTypeEnum(String label) {    this.label = label; }
    public String getLabel() {  return this.label;  }

}
