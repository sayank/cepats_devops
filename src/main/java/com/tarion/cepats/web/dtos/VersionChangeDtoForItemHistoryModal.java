package com.tarion.cepats.web.dtos;

import java.util.ArrayList;
import java.util.List;

public class VersionChangeDtoForItemHistoryModal {

	private String auditRefNum;
	private String deficiencyDescription;
	private String deficiencyLocation;
	private List<VersionChangeDto> changes = new ArrayList<VersionChangeDto>();
	
	public String getAuditRefNum() {
		return auditRefNum;
	}
	public void setAuditRefNum(String auditRefNum) {
		this.auditRefNum = auditRefNum;
	}
	public String getDeficiencyDescription() {
		return deficiencyDescription;
	}
	public void setDeficiencyDescription(String deficiencyDescription) {
		this.deficiencyDescription = deficiencyDescription;
	}
	public String getDeficiencyLocation() {
		return deficiencyLocation;
	}
	public void setDeficiencyLocation(String deficiencyLocation) {
		this.deficiencyLocation = deficiencyLocation;
	}
	public List<VersionChangeDto> getChanges() {
		return changes;
	}
	public void setChanges(List<VersionChangeDto> changes) {
		this.changes = changes;
	}
	
	public void addVersionChange(VersionChangeDto dto){
		changes.add(dto);
	}
	
}
