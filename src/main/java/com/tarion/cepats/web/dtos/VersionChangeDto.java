package com.tarion.cepats.web.dtos;

import java.util.LinkedList;
import java.util.List;

/**
 * @author <A href="mailto:joni.paananen@tarion.com">Joni Paananen</A>
 *
 * @since Nov 19, 2014
 */
public class VersionChangeDto {

	private int version;
	private String versionCreatedDate;
	private String changedBy;
	private List<ChangeDto> changes = new LinkedList<ChangeDto>();
	
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public String getVersionCreatedDate() {
		return versionCreatedDate;
	}
	public void setVersionCreatedDate(String versionCreatedDate) {
		this.versionCreatedDate = versionCreatedDate;
	}
	public String getChangedBy() {
		return changedBy;
	}
	public void setChangedBy(String changedBy) {
		this.changedBy = changedBy;
	}
	public List<ChangeDto> getChanges() {
		return changes;
	}
	public void setChanges(List<ChangeDto> changes) {
		this.changes = changes;
	}	
}
