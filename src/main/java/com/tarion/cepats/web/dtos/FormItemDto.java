package com.tarion.cepats.web.dtos;

import java.sql.Timestamp;

import com.tarion.cepats.domains.cepats.entities.FormTypeEnum;

public class FormItemDto {

	private String enrolmentNumber;
	private FormTypeEnum formType;
    private Long id;
    private Integer itemId;
    private String auditRefNum;
    private String deficiencyDescription;
    private String deficiencyLocation;
    private String priority;
    private String vendorsPosition;
    private String vendorsResponse;
    private String condoCorpPosition;
    private String condoCorpResponse;
    private String fundingAndDescription;
    private boolean attachments;
    private boolean isCondoConversionForm;
    private String userType;    
    private boolean itemAdded;
    private String lastModifiedDate;
    private String lastModifiedBy;
	public String getEnrolmentNumber() {
		return enrolmentNumber;
	}
	public void setEnrolmentNumber(String enrolmentNumber) {
		this.enrolmentNumber = enrolmentNumber;
	}
	public FormTypeEnum getFormType() {
		return formType;
	}
	public void setFormType(FormTypeEnum formType) {
		this.formType = formType;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	public String getAuditRefNum() {
		return auditRefNum;
	}
	public void setAuditRefNum(String auditRefNum) {
		this.auditRefNum = auditRefNum;
	}
	public String getDeficiencyDescription() {
		return deficiencyDescription;
	}
	public void setDeficiencyDescription(String deficiencyDescription) {
		this.deficiencyDescription = deficiencyDescription;
	}
	public String getDeficiencyLocation() {
		return deficiencyLocation;
	}
	public void setDeficiencyLocation(String deficiencyLocation) {
		this.deficiencyLocation = deficiencyLocation;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getVendorsPosition() {
		return vendorsPosition;
	}
	public void setVendorsPosition(String vendorsPosition) {
		this.vendorsPosition = vendorsPosition;
	}
	public String getVendorsResponse() {
		return vendorsResponse;
	}
	public void setVendorsResponse(String vendorsResponse) {
		this.vendorsResponse = vendorsResponse;
	}
	public String getCondoCorpPosition() {
		return condoCorpPosition;
	}
	public void setCondoCorpPosition(String condoCorpPosition) {
		this.condoCorpPosition = condoCorpPosition;
	}
	public String getCondoCorpResponse() {
		return condoCorpResponse;
	}
	public void setCondoCorpResponse(String condoCorpResponse) {
		this.condoCorpResponse = condoCorpResponse;
	}
	public String getFundingAndDescription() {
		return fundingAndDescription;
	}
	public void setFundingAndDescription(String fundingAndDescription) {
		this.fundingAndDescription = fundingAndDescription;
	}
	public boolean isAttachments() {
		return attachments;
	}
	public void setAttachments(boolean attachments) {
		this.attachments = attachments;
	}
	public boolean getIsCondoConversionForm() {
		return isCondoConversionForm;
	}
	public void setIsCondoConversionForm(boolean isCondoConversionForm) {
		this.isCondoConversionForm = isCondoConversionForm;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public boolean isItemAdded() {
		return itemAdded;
	}
	public void setItemAdded(boolean itemAdded) {
		this.itemAdded = itemAdded;
	}
	public String getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(String lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}    

}
