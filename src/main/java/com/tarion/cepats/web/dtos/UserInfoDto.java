package com.tarion.cepats.web.dtos;

import com.tarion.cepats.domains.crm.entities.EnrolmentEntity;
import com.tarion.cepats.domains.cepats.entities.UserTypeEnum;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Oct 07, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 */
public class UserInfoDto {

    private EnrolmentEntity enrolment;
    private UserTypeEnum userType;
    private String[] priority;
    private String[] condoPosition;
    private String[] vendorPosition;
    private boolean withinFirstYearPeriod;
    private boolean withinSecondYearPeriod;
    private boolean firstYearFormSubmitted;
    private boolean secondYearFormSubmitted;

    public UserTypeEnum getUserType() {
        return userType;
    }

    public void setUserType(UserTypeEnum userType) {
        this.userType = userType;
    }

    public String[] getPriority() {
        return priority;
    }

    public void setPriority(String[] priority) {
        this.priority = priority;
    }

    public String[] getCondoPosition() {
        return condoPosition;
    }

    public void setCondoPosition(String[] condoPosition) {
        this.condoPosition = condoPosition;
    }

    public String[] getVendorPosition() {
        return vendorPosition;
    }

    public void setVendorPosition(String[] vendorPosition) {
        this.vendorPosition = vendorPosition;
    }

    @JsonProperty("withinFirstYearPeriod")
    public boolean isWithinFirstYearPeriod() {
        return withinFirstYearPeriod;
    }

    public void setWithinFirstYearPeriod(boolean withinFirstYearPeriod) {
        this.withinFirstYearPeriod = withinFirstYearPeriod;
    }

    @JsonProperty("withinSecondYearPeriod")
    public boolean isWithinSecondYearPeriod() {
        return withinSecondYearPeriod;
    }

    public void setWithinSecondYearPeriod(boolean withinSecondYearPeriod) {
        this.withinSecondYearPeriod = withinSecondYearPeriod;
    }

    public EnrolmentEntity getEnrolment() {
        return enrolment;
    }

    public void setEnrolment(EnrolmentEntity enrolment) {
        this.enrolment = enrolment;
    }

	public boolean isFirstYearFormSubmitted() {
		return firstYearFormSubmitted;
	}

	public void setFirstYearFormSubmitted(boolean firstYearFormSubmitted) {
		this.firstYearFormSubmitted = firstYearFormSubmitted;
	}

	public boolean isSecondYearFormSubmitted() {
		return secondYearFormSubmitted;
	}

	public void setSecondYearFormSubmitted(boolean secondYearFormSubmitted) {
		this.secondYearFormSubmitted = secondYearFormSubmitted;
	}
    
}
