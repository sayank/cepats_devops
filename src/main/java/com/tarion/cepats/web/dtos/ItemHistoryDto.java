package com.tarion.cepats.web.dtos;

import java.util.ArrayList;
import java.util.List;

public class ItemHistoryDto {
	
	private List<ChangeDto> changes = new ArrayList<>();
	private int itemId;

	public List<ChangeDto> getChanges() {
		return changes;
	}
	public void setChanges(List<ChangeDto> changes) {
		this.changes = changes;
	}
	
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public void addChangeDto(ChangeDto changeDto) {
		changes.add(changeDto);
	}

}
