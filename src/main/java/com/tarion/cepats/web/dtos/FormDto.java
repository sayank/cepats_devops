package com.tarion.cepats.web.dtos;

import java.util.List;

public class FormDto {
	
	List<FormItemDto> items;
	boolean isCondoConversionForm;
	String userType;
	
	
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public List<FormItemDto> getItems() {
		return items;
	}
	public void setItems(List<FormItemDto> items) {
		this.items = items;
	}
	public boolean getIsCondoConversionForm() {
		return isCondoConversionForm;
	}
	public void setIsCondoConversionForm(boolean isCondoConversionForm) {
		this.isCondoConversionForm = isCondoConversionForm;
	}
	
	
	

}
