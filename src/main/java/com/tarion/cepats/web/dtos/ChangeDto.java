package com.tarion.cepats.web.dtos;

public class ChangeDto {
	
	String changeName;
	String oldValue;
	String newValue;
	private String revisedOnDate;
	private String revisedOnTime;
	private String revisedBy;
	int version;
	
	public String getChangeName() {
		return changeName;
	}
	public void setChangeName(String changeName) {
		this.changeName = changeName;
	}
	public String getOldValue() {
		return oldValue;
	}
	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}
	public String getNewValue() {
		return newValue;
	}
	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public String getRevisedOnDate() {
		return revisedOnDate;
	}
	public void setRevisedOnDate(String revisedOnDate) {
		this.revisedOnDate = revisedOnDate;
	}
	public String getRevisedOnTime() {
		return revisedOnTime;
	}
	public void setRevisedOnTime(String revisedOnTime) {
		this.revisedOnTime = revisedOnTime;
	}
	public String getRevisedBy() {
		return revisedBy;
	}
	public void setRevisedBy(String revisedBy) {
		this.revisedBy = revisedBy;
	}


}
