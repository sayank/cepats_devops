package com.tarion.cepats.web.controllers;

import com.tarion.cepats.common.CEPATSUtil;
import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.domains.cepats.entities.FormItemAttachmentEntity;
import com.tarion.cepats.domains.cepats.entities.FormItemEntity;
import com.tarion.cepats.services.FormService;
import com.tarion.cepats.services.attachments.AttachmentRetrievalService;
import com.tarion.cepats.services.attachments.AttachmentUploadService;
import com.tarion.cepats.web.dtos.FormItemAttachmentDto;
import com.tarion.cepats.web.dtos.UpdateResultDto;
import com.tarion.cepats.web.dtos.UserDto;
import com.tarion.tbi.bsa.cm.DocumentWSDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

@Controller
public class AttachmentsController extends CEPATSController{
	
	@Autowired
	private AttachmentRetrievalService attachmentRetrievalService;
	@Autowired 
    private AttachmentUploadService attachmentUploadService;
	@Autowired
	private FormService formService;
	
	@RequestMapping(value = "/{formType}/{enrolmentNumber}/getAttachmentData/{itemId}", method = RequestMethod.GET)
    public void getAttachmentData(@PathVariable("formType")String formType,
    		@PathVariable("enrolmentNumber")String enrolmentNumber,
    		@PathVariable String itemId, 
    		HttpServletResponse response, HttpSession session, HttpServletRequest request) {
		long startTime = LoggerUtil.logEnter(AttachmentsController.class, "getAttachmentData", itemId);
		LoggerUtil.logClientIp(AttachmentsController.class, "getAttachmentData", request);

		FormItemAttachmentEntity entity = attachmentRetrievalService.getAttachmentEntityByItemId(itemId);

		if(entity == null) {
			LoggerUtil.logError(AttachmentsController.class, "Unable to find form_items_attachments with cmItemId " + itemId);
			return;
		}
		
		DocumentWSDto document = null;
		
		try {
			document = attachmentRetrievalService.getDocumentById(itemId);
		}
		catch(Exception e) {
			LoggerUtil.logError(AttachmentsController.class, e.getMessage(), e);
		}
		
		if(document == null) {
			LoggerUtil.logError(AttachmentsController.class, "Unable to download document from CM with cmItemId " + itemId);
			return;
		}
		
		response.setContentType(document.getMimeType());
		
		String headerKey = "Content-Disposition";
        String headerValue = String.format("inline; filename=\"%s\"",
                entity.getOriginalFileName());
        response.setHeader(headerKey, headerValue);
        
        try {
        	OutputStream outStream = response.getOutputStream();
			outStream.write(document.getDataList().getData().get(0));
			outStream.close();
		} catch (IOException e) {
			LoggerUtil.logError(AttachmentsController.class, e.getMessage(), e);			
		}
		LoggerUtil.logExit(AttachmentsController.class, "getAttachmentData", startTime);
	}
	
    @RequestMapping(value = "/getAttachments/{formItemId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<FormItemAttachmentDto> getAttachments(@PathVariable("formItemId")long formItemId, HttpServletRequest request, HttpServletResponse response) {
		long startTime = LoggerUtil.logEnter(AttachmentsController.class, "getAttachments", formItemId);
		LoggerUtil.logClientIp(AttachmentsController.class, "getAttachmentData", request);
        List<FormItemAttachmentDto> attachments = null;

        try {
        	attachments = attachmentRetrievalService.getAttachmentsByFormItemId(formItemId);
        } catch (Exception e) {
            LoggerUtil.logError(CepatsFormController.class, e.getMessage(), e);
        }
		LoggerUtil.logExit(AttachmentsController.class, "getAttachments", startTime);
		setNoCacheResponseHeaders(response);
        return attachments;
    }

    @RequestMapping(value = "/{formType}/{enrolmentNumber}/uploadAttachment/{formItemId}", method = RequestMethod.POST, produces = "application/json", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE} )
    @ResponseBody
    public UpdateResultDto uploadAttachment(
    		@PathVariable("enrolmentNumber")String enrolmentNumber,
    								@PathVariable("formType")String formType,
                                    @RequestPart("file") MultipartFile file,
                                    @PathVariable("formItemId") long formItemId, HttpSession session, HttpServletRequest request) {

        Long startTime = LoggerUtil.logEnter(AttachmentsController.class, "uploadAttachment", formItemId);
		LoggerUtil.logClientIp(AttachmentsController.class, "uploadAttachment", request);
		UserDto user = (UserDto)session.getAttribute(CEPATSUtil.getUserSessionAttributeName(enrolmentNumber));

        FormItemEntity formItem = formService.getFormItem(formItemId);
        
        UpdateResultDto updateResult = null;

        try {
        	updateResult = attachmentUploadService.uploadAttachment(file.getOriginalFilename(), file.getBytes(), file.getContentType(), user, formItem);            
        } catch (IOException e) {
        	//IOException from getBytes, very unlikely to happen 
            LoggerUtil.logError(AttachmentsController.class, e.getMessage(), e);
            updateResult = new UpdateResultDto();
            updateResult.setError(true);
            updateResult.setMessage("Unable to upload attachment, try again later");
        }
        
        updateResult.setFormType(user.getFormType());

        LoggerUtil.logExit(AttachmentsController.class, "uploadAttachment", updateResult, startTime);
        
        return updateResult;
    }


}
