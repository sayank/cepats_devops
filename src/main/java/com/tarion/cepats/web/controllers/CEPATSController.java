package com.tarion.cepats.web.controllers;

import javax.servlet.http.HttpServletResponse;

public class CEPATSController {
	

	protected void setNoCacheResponseHeaders(HttpServletResponse response) {
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); 
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
	}

}
