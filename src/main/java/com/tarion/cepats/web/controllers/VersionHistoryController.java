package com.tarion.cepats.web.controllers;

import java.sql.Timestamp;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tarion.cepats.common.CEPATSUtil;
import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.domains.cepats.entities.FormTypeEnum;
import com.tarion.cepats.services.VersionHistoryService;
import com.tarion.cepats.web.dtos.ItemHistoryDto;
import com.tarion.cepats.web.dtos.UserDto;
import com.tarion.cepats.web.dtos.VersionChangeDto;
import com.tarion.cepats.web.dtos.VersionChangeDtoForItemHistoryModal;

@Controller
public class VersionHistoryController  extends CEPATSController{

	@Autowired
	VersionHistoryService formHistoryService;
	
	@RequestMapping(value = "/getHistory/{enrolmentNumber}/{formType}", method = RequestMethod.GET, produces = "application/json", headers = "Accept=application/json")
	public @ResponseBody List<ItemHistoryDto> getVersionHistory(@PathVariable("enrolmentNumber") String enrolmentNumber,
			@PathVariable("formType") String formType, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		
		LoggerUtil.logEnter(VersionHistoryController.class, "info", "getVersionHistory", enrolmentNumber, formType);

		try {
			List<ItemHistoryDto> versionHistory 
				=  formHistoryService.getVersionHistory(enrolmentNumber, FormTypeEnum.valueOf(formType), null, null, false);
			LoggerUtil.logExit(VersionHistoryController.class, "info", "getVersionHistory ", versionHistory.size());
			setNoCacheResponseHeaders(response);
			return versionHistory;
			
		} catch (Exception e) {
			LoggerUtil.logError(VersionHistoryController.class, e.getMessage(), e);
			return null;
		}
	}
	
	@RequestMapping(value = "/getItemHistory/{enrolmentNumber}/{itemId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody VersionChangeDtoForItemHistoryModal getVersionHistory(@PathVariable("enrolmentNumber") String enrolmentNumber, @PathVariable("itemId") long itemId, HttpSession session, HttpServletRequest request) {
		UserDto user = (UserDto)session.getAttribute(CEPATSUtil.getUserSessionAttributeName(enrolmentNumber));
		return formHistoryService.getHistoryForItemModal(itemId, enrolmentNumber);
	}
	
	//startDate and endDate format: YYYY-MM-DD
	@RequestMapping(value = "/downloadHistory/{enrolmentNumber}/{formType}/{startDate}/{endDate}", method = RequestMethod.GET)
	public @ResponseBody  String getUpdatesHistoryForDatesRange(
			@PathVariable("enrolmentNumber") String enrolmentNumber, 
			@PathVariable("formType") String formType, 
			@PathVariable("startDate") String startDate, 
			@PathVariable("endDate") String endDate, 
			HttpServletRequest request,
			HttpServletResponse response) {
		
		LoggerUtil.logDebug(RemoteController.class, "downloadHistory", enrolmentNumber,formType, startDate, endDate);
		
		try {

			Timestamp start = CEPATSUtil.convertStringToTimestamp(startDate, CEPATSUtil.DATE_PATTERN_DATE_ONLY);
			Timestamp end =  CEPATSUtil.convertStringToTimestamp(endDate, CEPATSUtil.DATE_PATTERN_DATE_ONLY);
			String versionHistory = formHistoryService.buildFormUpdateHistory(enrolmentNumber, 
					FormTypeEnum.valueOf(formType.toUpperCase()), start, end);
			
			return versionHistory;
//        	StringBuffer fileName = new StringBuffer("Version-History-" + enrolmentNumber + ".doc");
//
//            response.setContentType("application/octet-stream");
//            response.setHeader("Content-Disposition", "attachment; filename=" + fileName.toString());
//            setNoCacheResponseHeaders(response); 
//            response.getOutputStream().write(versionHistory.getBytes());
//            response.flushBuffer();
        } catch (Exception e) {
            LoggerUtil.logError(CepatsFormController.class, e.getMessage(), e);
            return null;
        }

	}
}