package com.tarion.cepats.web.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tarion.cepats.common.CEPATSUtil;
import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.domains.crm.entities.WSCAssignedEntity;
import com.tarion.cepats.domains.crm.entities.WSRAssignedEntity;
import com.tarion.cepats.services.CRMService;
import com.tarion.cepats.services.TimelinesService;
import com.tarion.cepats.web.dtos.EnrolmentInfoDto;
import com.tarion.cepats.web.dtos.UserDto;
//import com.tarion.tbi.warrantytimelines.TimelinesResponse;
import com.tarion.tbi.services.TimelinesResponse;

@Controller
public class EnrolmentController extends CEPATSController{
	
	@Autowired
	private TimelinesService timelinesService;
	@Autowired 
    private CRMService crmService;
	
	
	@RequestMapping(value = "/{enrolmentNumber}/getEnrolmentInfo", method = RequestMethod.GET, produces = "application/json", headers = "Accept=application/json")
    public @ResponseBody EnrolmentInfoDto  getEnrolmentInfo( @PathVariable("enrolmentNumber")String enrolmentNumber, HttpServletResponse response, HttpSession session, HttpServletRequest request) {
		UserDto user = (UserDto)session.getAttribute(CEPATSUtil.getUserSessionAttributeName(enrolmentNumber));
		long startTime = LoggerUtil.logEnter(EnrolmentController.class, "getEnrolmentInfo", user.getUsername(), user.getLoggedinUserType(), user.getEnrolment().getEnrolmentNumber());
		LoggerUtil.logClientIp(EnrolmentController.class, "getEnrolmentInfo", request);
		
		EnrolmentInfoDto dto = new EnrolmentInfoDto();
		
		try {
			TimelinesResponse timelines = timelinesService.getWarrantyTimelines(user.getEnrolment().getEnrolmentNumber());
			WSCAssignedEntity wscAssigned = crmService.getWSCAssignedToCEPATS(user.getEnrolment().getEnrolmentNumber());
			WSRAssignedEntity wsrAssigned = crmService.getWSRAssignedToCEPATS(user.getEnrolment().getEnrolmentNumber());
			
			dto.setTimelines(timelines.getTimelinesList().getTimelines());
			dto.setWscAssigned(wscAssigned.getWscAssigned());
			dto.setWsrAssigned(wsrAssigned.getWsrAssigned());
			dto.setWscAssignedEmail(wscAssigned.getEmailAddress());
			dto.setWsrAssignedEmail(wsrAssigned.getEmailAddress());
		} catch (Exception e) {
			LoggerUtil.logError(EnrolmentController.class, "getEnrolmentInfo", startTime);
			dto.setErrorMessage(e.getMessage());
		}

		LoggerUtil.logExit(EnrolmentController.class, "getEnrolmentInfo", startTime);
		return dto;
		
	}

}
